package com.aroundpost;

import android.content.Context;
import android.content.SharedPreferences;

import com.aroundpost.response.ChannelResponse;
import com.aroundpost.response.LocationResponse;
import com.aroundpost.response.SearchResultNewsModel;
import com.aroundpost.response.SignUpResponse;
import com.aroundpost.utils.JsonUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manish on 9/22/2016.
 */
public class AppPreference {
    private static final String SIGNUP_RESPONSE = "signup_response";
    private static final String LOCATION_RESPONSE = "location_response";
    private static final String NEW_PREFERENCE = "news_preference";
    private static final String CHANNEL_RESPONSE = "channel_response";
    private static final String PREFERENCE_CATEGORY = "preference_category";
    private static final String CHANNEL_PREFERENCE = "channel_preference";
    private static final String USER_LOCATION = "user_location";
    private static final String IMAGE_URL = "image_url";
    private static final String API_TYPE = "which_api";
    private static final String DISTANCE_COVERED = "distance_covered";
    private static AppPreference appPreference;
    private static SharedPreferences mSharedPreferences;
    public final static String AROUND_POST_PREFERENCE = "around_post_preference";

    public final static String USER_TOKEN_KEY = "user_token_key";

    private final static String IS_APP_STARTED_AGAIN = "is_app_started_again";

   public static final String LANG = "lang";
   public static final String NOTIFICATION_ON_OFF = "notifcation_on_off";
   public static final String SOUND_ON_OFF = "sound_on_off";
   public static final String language_type = "language_type";
   public static final String setting_User_FirstName = "setting_User_FirstName";
   public static final String setting_User_LastName = "setting_User_LastName";
   public static final String setting_User_Email = "setting_User_Email";

    public static final String FEED_LIST = "feed_list";
    public static final String SKIP = "skip";
    public static final String PAGE = "page";
    public static final String LIMIT = "limit";
    public static final String TITLE = "title";
    public static final String TYPE_DISTANCE = "type";
    public static final String ONE_SIGNAL_ID = "one_signal_id";


    public static final String LATITUDE = "latidute";
    public static final String LONGITUDE = "longitude";

    private AppPreference() {
        if (appPreference != null) {
            appPreference = new AppPreference();
        }
    }

    public static AppPreference getAppPreference(Context context) {
        if (appPreference == null) {
            appPreference = new AppPreference();
        }
        mSharedPreferences = context.getSharedPreferences(AROUND_POST_PREFERENCE, Context.MODE_PRIVATE);
        return appPreference;
    }

    public void clearPreference(){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    public boolean isAppStartedFirstTime() {
        return mSharedPreferences.getBoolean(IS_APP_STARTED_AGAIN, true);
    }

    public void setIsAppStartedAgain(boolean isAppStarted) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(IS_APP_STARTED_AGAIN, isAppStarted);
        editor.commit();
    }

    public void setSignUpDetails(SignUpResponse signUpResponse) {
        String signupDetail = JsonUtils.toJson(signUpResponse);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(SIGNUP_RESPONSE, signupDetail);
        editor.commit();
    }

    public SignUpResponse getSignUpDetails() {
        String signUpDetail = mSharedPreferences.getString(SIGNUP_RESPONSE, null);
        if (signUpDetail == null) {
            return null;
        }
        return JsonUtils.fromJson(signUpDetail, SignUpResponse.class);
    }

    public void setLocationData(LocationResponse locationResponse) {
        String location = JsonUtils.toJson(locationResponse);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(LOCATION_RESPONSE, location);
        editor.commit();
    }

    public LocationResponse getLocationData() {
        String location = mSharedPreferences.getString(LOCATION_RESPONSE, null);
        if (location == null) {
            return null;
        }
        return JsonUtils.fromJson(location, LocationResponse.class);
    }

    public void setChannelData(ChannelResponse channelResponse){
        String channel = JsonUtils.toJson(channelResponse);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(CHANNEL_RESPONSE, channel);
        editor.commit();
    }

    public ChannelResponse getChannelData() {
        String location = mSharedPreferences.getString(CHANNEL_RESPONSE, null);
        if (location == null) {
            return null;
        }
        return JsonUtils.fromJson(location, ChannelResponse.class);
    }

    public void setLocationNewsPreference(String id) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(NEW_PREFERENCE, id);
        editor.commit();
    }

    public String getLocationNewsPreference(){
        return mSharedPreferences.getString(NEW_PREFERENCE, "");
    }

    public void setChannelNewsPreference(String id) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(CHANNEL_PREFERENCE, id);
        editor.commit();
    }

    public String getChannelNewsPreference(){
        return mSharedPreferences.getString(CHANNEL_PREFERENCE, "");
    }

    public void setPreferedCategory(String locationId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PREFERENCE_CATEGORY, locationId);
        editor.commit();
    }

    public String getPreferenceCategory(){
        return mSharedPreferences.getString(PREFERENCE_CATEGORY, "");
    }

    public void setUserLocation(String location) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_LOCATION, location);
        editor.commit();
    }

    public String getTitlePref()
    {
        return mSharedPreferences.getString(TITLE,"");
    }
    public void setTitlePref(String title)
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(TITLE, title);
        editor.commit();
    }

    public String getUserLocation(){
        return mSharedPreferences.getString(USER_LOCATION, "");
    }

    public void setImageURL(String imageUrl) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(IMAGE_URL, imageUrl);
        editor.commit();
    }

    public int getTypeDistance()
    {
        return mSharedPreferences.getInt(TYPE_DISTANCE,0);
    }
    public void setTypeDistance(int type)
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(TYPE_DISTANCE, type);
        editor.commit();
    }
    public String getImageURL(){
        return mSharedPreferences.getString(IMAGE_URL, "");
    }

    public void setWhichApi(int type) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(API_TYPE, type);
        editor.commit();
    }

    public int getWhichApi(){
        return mSharedPreferences.getInt(API_TYPE, 0);
    }
    public int getLimit(){
        return mSharedPreferences.getInt(LIMIT, 10);
    }
    public int getSkip(){
        return mSharedPreferences.getInt(SKIP, 0);
    }
    public int getPage(){
        return mSharedPreferences.getInt(PAGE, 1);
    }

    public void setDistanceCovered(int i) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(DISTANCE_COVERED, i);
        editor.commit();
    }

    public int getDistanceCovered(){
        return mSharedPreferences.getInt(DISTANCE_COVERED, 300);
    }

    public void setFeedNeews(ArrayList<SearchResultNewsModel> list, int skip, int limit, int page){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(FEED_LIST, json);
        editor.putInt(SKIP, skip);
        editor.putInt(LIMIT, limit);
        editor.putInt(PAGE, page);
        editor.commit();
    }

    public ArrayList<SearchResultNewsModel> getFeedList(){

        Gson gson = new Gson();
        String json = mSharedPreferences.getString(FEED_LIST, "");
        Type type = new TypeToken<List<SearchResultNewsModel>>(){}.getType();
        ArrayList<SearchResultNewsModel> list  = gson.fromJson(json, type);
        return list;
    }
    public void setOneSignalID(String oneSignalID) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ONE_SIGNAL_ID, oneSignalID);
        editor.commit();
    }

    public String getOneSignalID(){
        return mSharedPreferences.getString(ONE_SIGNAL_ID, "");
    }
}
