package com.aroundpost.response;

import com.aroundpost.response.StoryModel;

public class SearchResultNewsModel {
    public String story_id;
    public StoryModel story;
    public String distance = null;

    public String getDistance() { return distance; }
    public void setDistance(String distance) { this.distance = distance; }

    public String getStory_id() {
        return story_id;
    }

    public void setStory_id(String story_id) {
        this.story_id = story_id;
    }

    public StoryModel getStory() {
        return story;
    }

    public void setStory(StoryModel story) {
        this.story = story;
    }
}