package com.aroundpost.response;

/**
 * Created by Manish on 3/31/2017.
 */

public class FeedSearchModelResponse extends GenericResponse {
    private FeedSearchModel data = new FeedSearchModel();

    public FeedSearchModel getData() {
        return data;
    }

    public void setData(FeedSearchModel data) {
        this.data = data;
    }
}
