package com.aroundpost.response;

/**
 * Created by Manish on 9/23/2016.
 */
public class FeedNewsModel {
    private String _id;
    private FeedStory story;
    private FeedImageModel image;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public FeedStory getStory() {
        return story;
    }

    public void setStory(FeedStory story) {
        this.story = story;
    }

    public FeedImageModel getImage() {
        return image;
    }

    public void setImage(FeedImageModel image) {
        this.image = image;
    }
}
