package com.aroundpost.response;

import java.util.ArrayList;

/**
 * Created by Manish on 12/26/2016.
 */
public class UserInterestData {
    private ArrayList<UserInterestPlaces> places;
    private ArrayList<UserInterestCategory> categories;

    public ArrayList<UserInterestCategory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<UserInterestCategory> categories) {
        this.categories = categories;
    }

    public ArrayList<UserInterestPlaces> getPlaces() {
        return places;
    }

    public void setPlaces(ArrayList<UserInterestPlaces> places) {
        this.places = places;
    }
}
