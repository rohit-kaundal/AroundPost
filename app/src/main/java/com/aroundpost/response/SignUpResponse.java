package com.aroundpost.response;

/**
 * Created by Manish on 12/10/2016.
 */

public class SignUpResponse extends GenericResponse{
    private SignUpModel data = new SignUpModel();

    public SignUpModel getData() {
        return data;
    }

    public void setData(SignUpModel data) {
        this.data = data;
    }
}
