package com.aroundpost.response;

import java.util.ArrayList;

/**
 * Created by Manish on 12/26/2016.
 */
public class UserInterestPlaces {
    private String _id;
    private String name;
    private ArrayList<UserInterestLocation> location;
    private int __v;
    private String parentId;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<UserInterestLocation> getLocation() {
        return location;
    }

    public void setLocation(ArrayList<UserInterestLocation> location) {
        this.location = location;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
