package com.aroundpost.response;

/**
 * Created by Manish on 1/7/2017.
 */

public class UpdateUserInterestResponse extends GenericResponse {
    private UpdateUserInterestData data = new UpdateUserInterestData();

    public UpdateUserInterestData getData() {
        return data;
    }

    public void setData(UpdateUserInterestData data) {
        this.data = data;
    }
}
