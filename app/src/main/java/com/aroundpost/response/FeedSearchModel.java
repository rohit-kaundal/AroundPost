package com.aroundpost.response;

import java.util.ArrayList;

/**
 * Created by Manish on 3/31/2017.
 */

public class FeedSearchModel {
    private String imgUrl;
    private int recordCount;
    private ArrayList<StoryModel> searchResult;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    public ArrayList<StoryModel> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(ArrayList<StoryModel> searchResult) {
        this.searchResult = searchResult;
    }
}
