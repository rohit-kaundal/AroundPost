package com.aroundpost.response;

import java.util.ArrayList;

/**
 * Created by Manish on 3/23/2017.
 */

public class LocationResponse extends GenericResponse {
    public ArrayList<LocationData> data = new ArrayList<LocationData>();


    public ArrayList<LocationData> getData() {
        return data;
    }

    public void setData(ArrayList<LocationData> data) {
        this.data = data;
    }
}
