package com.aroundpost.response;

/**
 * Created by Manish on 12/26/2016.
 */

public class UserInterestResponse extends GenericResponse {
    private UserInterestData data = new UserInterestData();

    public UserInterestData getData() {
        return data;
    }

    public void setData(UserInterestData data) {
        this.data = data;
    }
}
