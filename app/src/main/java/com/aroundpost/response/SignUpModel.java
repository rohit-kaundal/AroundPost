package com.aroundpost.response;

/**
 * Created by Manish on 12/10/2016.
 */
public class SignUpModel {
    private String api_key;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }
}
