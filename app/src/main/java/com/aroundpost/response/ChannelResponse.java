package com.aroundpost.response;

import java.util.ArrayList;

/**
 * Created by Manish on 3/24/2017.
 */

public class ChannelResponse extends GenericResponse {
    public ArrayList<ChannelData> data = new ArrayList<ChannelData>();


    public ArrayList<ChannelData> getData() {
        return data;
    }

    public void setData(ArrayList<ChannelData> data) {
        this.data = data;
    }
}
