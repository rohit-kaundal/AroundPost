package com.aroundpost.response;

import java.util.ArrayList;

/**
 * Created by Manish on 1/10/2017.
 */

public class FeedNewsResponse extends GenericResponse {
    private FeedImageModel data = new FeedImageModel();

    public FeedImageModel getData() {
        return data;
    }

    public void setData(FeedImageModel data) {
        this.data = data;
    }
}
