package com.aroundpost.response;

import java.util.ArrayList;

/**
 * Created by Manish on 1/10/2017.
 */
public class FeedImageModel {
   private String imgUrl;
    private int recordCount;
    private ArrayList<SearchResultNewsModel> searchResult;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    public ArrayList<SearchResultNewsModel> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(ArrayList<SearchResultNewsModel> searchResult) {
        this.searchResult = searchResult;
    }
}
