package com.aroundpost.response;

/**
 * Created by Manish on 3/24/2017.
 */

public class LanguageModel  {
    public String language_name;

    public String getLanguage_name() {
        return language_name;
    }

    public void setLanguage_name(String language_name) {
        this.language_name = language_name;
    }
}
