package com.aroundpost.request;

/**
 * Created by Manish on 12/6/2016.
 */

public class SignUpRequest {
    private String fb_access_token;
    private String mobile;
    private String device_id;
    private String country;
    private String country_code;
    private String facebook_id;
    private String device_type;
    private String first_name ="";
    private String last_name ="";
    private String email = "";

    public String getOne_signal_id() {
        return one_signal_id;
    }

    public void setOne_signal_id(String one_signal_id) {
        this.one_signal_id = one_signal_id;
    }

    private String one_signal_id="";

    public String languages() {
        return languages;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public void setLang_type(String languages) {
        this.languages = languages;
    }

    private String languages;

    public String getFb_access_token() {
        return fb_access_token;
    }

    public void setFb_access_token(String fb_access_token) {
        this.fb_access_token = fb_access_token;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }
}
