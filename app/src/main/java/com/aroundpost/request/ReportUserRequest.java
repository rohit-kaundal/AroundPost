package com.aroundpost.request;

/**
 * Created by Manish on 3/30/2017.
 */

public class ReportUserRequest {
    //@Field("story_id") String storyId, @Field("report_category_id") String type
    private String story_id;
    private String report_category_id;

    public String getStory_id() {
        return story_id;
    }

    public void setStory_id(String story_id) {
        this.story_id = story_id;
    }

    public String getReport_category_id() {
        return report_category_id;
    }

    public void setReport_category_id(String report_category_id) {
        this.report_category_id = report_category_id;
    }
}
