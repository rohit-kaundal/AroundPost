package com.aroundpost.request;

/**
 * Created by Manish on 12/6/2016.
 */
public class LocationRequest {
    private String latitude;
    private String longitude;
    private String observedOn;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getObservedOn() {
        return observedOn;
    }

    public void setObservedOn(String observedOn) {
        this.observedOn = observedOn;
    }
}
