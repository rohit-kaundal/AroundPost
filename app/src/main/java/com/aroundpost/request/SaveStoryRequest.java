package com.aroundpost.request;

/**
 * Created by Manish on 3/31/2017.
 */

public class SaveStoryRequest {
    private String story_id;

    public String getStory_id() {
        return story_id;
    }

    public void setStory_id(String story_id) {
        this.story_id = story_id;
    }
}
