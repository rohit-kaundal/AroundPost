package com.aroundpost.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.aroundpost.R;
import com.aroundpost.activity.FeedChannelLocationSelectActivity;
import com.aroundpost.response.LocationData;
import com.aroundpost.utils.BasicUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Manish on 3/25/2017.
 */

public class LocationNewsAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private List<LocationData> listTitle;
    private HashMap<LocationData, List<LocationData>> listHashMap;
    private FeedChannelLocationSelectActivity feedMainNewsActivity;
    private ExpandableListView expandableListView;

    private HashMap<String, String> placesHashMap = new HashMap<>();
    private ArrayList<String> cityIdList;
    private ArrayList<String> cityChildIdList;

    public LocationNewsAdapter(Context context, List<LocationData> listTitle, HashMap<LocationData, List<LocationData>> listHashMap, ExpandableListView expandableListView, ArrayList<String> cityIdList, ArrayList<String> cityChildIdList, FeedChannelLocationSelectActivity feedMainNewsActivity) {
        this.mContext = context;
        this.listTitle = listTitle;
        this.listHashMap = listHashMap;
        this.feedMainNewsActivity = feedMainNewsActivity;
        this.expandableListView = expandableListView;
        this.cityIdList = cityIdList;
        this.cityChildIdList = cityChildIdList;
    }

    @Override
    public int getGroupCount() {
        return listTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (listHashMap.get(listTitle.get(groupPosition)) != null)
            return listHashMap.get(listTitle.get(groupPosition)).size();
        else return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(this.listTitle.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_feed_news_topic, null);
        }

        TextView tvTopic = (TextView) convertView.findViewById(R.id.tv_topic);
        final CheckBox cbBox = (CheckBox) convertView.findViewById(R.id.cb_header);
        final LocationData userInterestPlaces = listTitle.get(groupPosition);
        tvTopic.setText(userInterestPlaces.getLocation_name().trim());
        tvTopic.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(mContext));
        if (cityIdList != null) {
            if (cityIdList.contains(userInterestPlaces.getId())) {
                cbBox.setChecked(true);
                placesHashMap.put(groupPosition + "", userInterestPlaces.getId());
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, placesHashMap, 1, false);
            }
        }

        convertView.findViewById(R.id.rel_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityIdList.clear();
                if (cbBox.isChecked()) {
                    cbBox.setChecked(false);
                    if (placesHashMap.containsKey(groupPosition + "")) {
                        Log.d("Contains and ", " removed");
                        placesHashMap.remove(groupPosition + "");
                        Log.d("Place HashMap ", placesHashMap + "");
                    }
                    if (getChildrenCount(groupPosition) > 0) {
                        int sizeChild = getChildrenCount(groupPosition);
                        for (int i = 0; i < sizeChild; i++) {
                            placesHashMap.remove(groupPosition + "-" + i);
                        }
                    }
                } else {
                    cbBox.setChecked(true);
                    placesHashMap.put(groupPosition + "", userInterestPlaces.getId());
                    if (getChildrenCount(groupPosition) > 0) {
                        int sizeChild = getChildrenCount(groupPosition);
                        for (int i = 0; i < sizeChild; i++) {
                            LocationData child = (LocationData) getChild(groupPosition, i);
                            placesHashMap.put(groupPosition + "-" + i, child.getId());
                        }
                    }
                }
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, placesHashMap, 1, true);
            }
        });

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_nav_list, null);
        }
        TextView tvHeader = (TextView) convertView.findViewById(R.id.tv_header);
        final CheckBox cbBox = (CheckBox) convertView.findViewById(R.id.cb_header);
        final LocationData child = (LocationData) getChild(groupPosition, childPosition);
        tvHeader.setText(child.getLocation_name());
        tvHeader.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(mContext));

        if (cityChildIdList != null) {
            if (cityChildIdList.contains(child.getId())) {
                cbBox.setChecked(true);
                placesHashMap.put(groupPosition + "-" + childPosition + "", child.getId());
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, placesHashMap, 1, false);
            }
        }

        convertView.findViewById(R.id.rel_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityChildIdList.clear();
                if (cbBox.isChecked()) {
                    cbBox.setChecked(false);
                    if (placesHashMap.containsKey(groupPosition + "-" + childPosition + "")) {
                        placesHashMap.remove(groupPosition + "-" + childPosition + "");
                    }
                } else {
                    cbBox.setChecked(true);
                    placesHashMap.put(groupPosition + "-" + childPosition + "", child.getId());
                }
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, placesHashMap, 1, false);
            }
        });


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}