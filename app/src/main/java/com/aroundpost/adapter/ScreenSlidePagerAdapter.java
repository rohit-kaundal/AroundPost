package com.aroundpost.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.aroundpost.fragment.SlidingFragment;

/**
 * Created by Manish on 3/28/2017.
 */

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    private String imageUrl,sourceUrl;
    private int listCount, selectedPosition;


    public ScreenSlidePagerAdapter(FragmentManager fm, String imageUrl,String sourceUrl, int  listCount, int selectedPosition) {
        super(fm);
        this.imageUrl = imageUrl;
        this.sourceUrl = sourceUrl;
        this.listCount = listCount;
        this.selectedPosition = selectedPosition;
    }

    @Override
    public Fragment getItem(int position) {
        SlidingFragment slidingFragment = new SlidingFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putString("imageUrl", imageUrl);
        bundle.putString("source_url",sourceUrl);
        slidingFragment.setArguments(bundle);

        return slidingFragment;
    }

    @Override
    public int getCount() {
        return listCount;
    }
}