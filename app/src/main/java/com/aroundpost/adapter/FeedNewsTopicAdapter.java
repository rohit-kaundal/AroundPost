package com.aroundpost.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.aroundpost.R;
import com.aroundpost.response.UserInterestPlaces;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Manish on 11/28/2016.
 */

public class FeedNewsTopicAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<UserInterestPlaces> listTitle;
    private HashMap<UserInterestPlaces, List<String>> listHashMap;

    public FeedNewsTopicAdapter(Context context, List<UserInterestPlaces> listTitle, HashMap<UserInterestPlaces, List<String>> listHashMap){
        this.mContext = context;
        this.listTitle = listTitle;
        this.listHashMap = listHashMap;
    }

    @Override
    public int getGroupCount() {
        return listTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listHashMap.get(listTitle.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(listTitle.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_feed_news_topic, null);
        }

        TextView tvTopic = (TextView) convertView.findViewById(R.id.tv_topic);
        tvTopic.setText(listTitle.get(groupPosition).getName());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_feed_news_topic, null);
        }

        TextView tvTopic = (TextView) convertView.findViewById(R.id.tv_topic);
        tvTopic.setText(listHashMap.get(listTitle.get(groupPosition)).get(childPosition));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
