package com.aroundpost.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aroundpost.R;
import com.aroundpost.interfaces.LocationDataInterface;
import com.aroundpost.response.LocationData;
import com.aroundpost.utils.BasicUtils;

import java.util.ArrayList;

/**
 * Created by Manish on 3/23/2017.
 */

public class LocationSpinnerAdapter extends BaseAdapter {

    public ArrayList<LocationData> mLocationDataArray;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private LocationDataInterface mLocationDataInterface;

    public LocationSpinnerAdapter(@NonNull Context context, ArrayList<LocationData> locationDataArrayList, LocationDataInterface locationDataInterface) {
        this.mContext = context;
        this.mLocationDataArray = locationDataArrayList;
        this.mLocationDataInterface = locationDataInterface;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mLocationDataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mLocationDataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;
        if (view == null) {
            viewHolder = new ViewHolder();
            view = mLayoutInflater.inflate(R.layout.spinner_layout, parent, false);
            viewHolder.textView = (TextView) view.findViewById(R.id.text_location);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        Log.d("Location Name:  " + position + " = ", mLocationDataArray.get(position).getLocation_name().toString());
        viewHolder.textView.setText(mLocationDataArray.get(position).getLocation_name().toString().trim());
        viewHolder.textView.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(mContext));
        /*view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLocationDataInterface.locationDataListener(mLocationDataArray.get(position));
            }
        });*/

        return view;
    }

    public class ViewHolder {
        public TextView textView;
    }
}
