/*
package com.aroundpost.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.aroundpost.R;
import com.aroundpost.activity.FeedMainNewsActivity;
import com.aroundpost.interfaces.OpenGroupInterface;
import com.aroundpost.response.UserInterestPlaces;
import com.aroundpost.utils.BasicUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

*/
/**
 * Created by Manish on 12/28/2016.
 *//*

public class PlacesInterestAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private List<UserInterestPlaces> listTitle;
    private HashMap<UserInterestPlaces, List<UserInterestPlaces>> listHashMap;
    private FeedMainNewsActivity feedMainNewsActivity;
    private ExpandableListView expandableListView;

    private HashMap<String, String> placesHashMap = new HashMap<>();
    private ArrayList<String> cityIdList;
    private ArrayList<String> cityChildIdList;

    public PlacesInterestAdapter(Context context, List<UserInterestPlaces> listTitle, HashMap<UserInterestPlaces, List<UserInterestPlaces>> listHashMap, ExpandableListView expandableListView, ArrayList<String> cityIdList, ArrayList<String> cityChildIdList, FeedMainNewsActivity feedMainNewsActivity) {
        this.mContext = context;
        this.listTitle = listTitle;
        this.listHashMap = listHashMap;
        this.feedMainNewsActivity = feedMainNewsActivity;
        this.expandableListView = expandableListView;
        this.cityIdList = cityIdList;
        this.cityChildIdList = cityChildIdList;
    }

    @Override
    public int getGroupCount() {
        return listTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (listHashMap.get(listTitle.get(groupPosition)) != null)
            return listHashMap.get(listTitle.get(groupPosition)).size();
        else return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(this.listTitle.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_feed_news_topic, null);
        }

        TextView tvTopic = (TextView) convertView.findViewById(R.id.tv_topic);
        final CheckBox cbBox = (CheckBox) convertView.findViewById(R.id.cb_header);
        final UserInterestPlaces userInterestPlaces = listTitle.get(groupPosition);
        tvTopic.setText(userInterestPlaces.getName());
        tvTopic.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(mContext));
        if (cityIdList != null) {
            if (cityIdList.contains(userInterestPlaces.get_id())) {
                cbBox.setChecked(true);
                placesHashMap.put(groupPosition + "", userInterestPlaces.get_id());
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, placesHashMap, 1, false);
            }
        }

        convertView.findViewById(R.id.rel_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityIdList.clear();
                if (cbBox.isChecked()) {
                    cbBox.setChecked(false);
                    if (placesHashMap.containsKey(groupPosition + "")) {
                        Log.d("Contains and ", " removed");
                        placesHashMap.remove(groupPosition + "");
                        Log.d("Place HashMap ", placesHashMap + "");
                    }
                    if (getChildrenCount(groupPosition) > 0) {
                        int sizeChild = getChildrenCount(groupPosition);
                        for (int i = 0; i < sizeChild; i++) {
                            placesHashMap.remove(groupPosition + "-" + i);
                        }
                    }
                } else {
                    cbBox.setChecked(true);
                    placesHashMap.put(groupPosition + "", userInterestPlaces.get_id());
                    if (getChildrenCount(groupPosition) > 0) {
                        int sizeChild = getChildrenCount(groupPosition);
                        for (int i = 0; i < sizeChild; i++) {
                            UserInterestPlaces child = (UserInterestPlaces) getChild(groupPosition, i);
                            placesHashMap.put(groupPosition + "-" + i, child.get_id());
                        }
                    }
                }
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, placesHashMap, 1, true);
            }
        });

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_nav_list, null);
        }
        TextView tvHeader = (TextView) convertView.findViewById(R.id.tv_header);
        final CheckBox cbBox = (CheckBox) convertView.findViewById(R.id.cb_header);
        final UserInterestPlaces child = (UserInterestPlaces) getChild(groupPosition, childPosition);
        tvHeader.setText(child.getName());
        tvHeader.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(mContext));

        if (cityChildIdList != null) {
            if (cityChildIdList.contains(child.get_id())) {
                cbBox.setChecked(true);
                placesHashMap.put(groupPosition + "-" + childPosition + "", child.get_id());
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, placesHashMap, 1, false);
            }
        }

        convertView.findViewById(R.id.rel_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityChildIdList.clear();
                if (cbBox.isChecked()) {
                    cbBox.setChecked(false);
                    if (placesHashMap.containsKey(groupPosition + "-" + childPosition + "")) {
                        placesHashMap.remove(groupPosition + "-" + childPosition + "");
                    }
                } else {
                    cbBox.setChecked(true);
                    placesHashMap.put(groupPosition + "-" + childPosition + "", child.get_id());
                }
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, placesHashMap, 1, false);
            }
        });


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
*/
