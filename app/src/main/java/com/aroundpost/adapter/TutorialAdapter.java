package com.aroundpost.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.aroundpost.fragment.TutorialScreenFragment;
import com.aroundpost.fragment.TutorialScreenFragment1;
import com.aroundpost.utils.Constants;

/**
 * Created by Manish on 9/22/2016.
 */
public class TutorialAdapter extends FragmentStatePagerAdapter {
    private Context mContext;
    private FragmentManager mFragmentManager;

    public TutorialAdapter(FragmentManager fragmentManager, Context context){
        super(fragmentManager);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return TutorialScreenFragment.getTutorialFragment(position);
        }else if(position == 1){
            return TutorialScreenFragment1.getTutorialFragment1(position);
        }else if(position == 2){
            return TutorialScreenFragment1.getTutorialFragment1(position);
        }else{
            return null;
        }
    }

    @Override
    public int getCount() {
        return Constants.TUTORIAL_PAGES_SIZE;
    }
}
