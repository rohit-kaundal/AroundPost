package com.aroundpost.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.aroundpost.R;
import com.aroundpost.activity.FeedChannelLocationSelectActivity;
import com.aroundpost.response.ChannelData;
import com.aroundpost.utils.BasicUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Manish on 3/25/2017.
 */

public class ChannelNewsAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private List<ChannelData> listTitle;
    private HashMap<ChannelData, List<ChannelData>> listHashMap;
    private HashMap<String, String> categoryHashMap = new HashMap<>();
    private FeedChannelLocationSelectActivity feedMainNewsActivity;
    private ExpandableListView expandableListView;
    private ArrayList<String> categoryIdList;
    private ArrayList<String> childCategoryIdList;

    public ChannelNewsAdapter(Context context, List<ChannelData> listTitle, HashMap<ChannelData, List<ChannelData>> listHashMap, ExpandableListView expandableListView, ArrayList<String> categoryIdList, ArrayList<String> childCategoryIdList, FeedChannelLocationSelectActivity feedMainNewsActivity) {
        this.mContext = context;
        this.listTitle = listTitle;
        this.listHashMap = listHashMap;
        this.feedMainNewsActivity = feedMainNewsActivity;
        this.expandableListView = expandableListView;
        this.categoryIdList = categoryIdList;
        this.childCategoryIdList = childCategoryIdList;
    }

    @Override
    public int getGroupCount() {
        return listTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (listHashMap.get(listTitle.get(groupPosition)) != null)
            return listHashMap.get(listTitle.get(groupPosition)).size();
        else return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(this.listTitle.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_feed_news_topic, null);
        }

        TextView tvTopic = (TextView) convertView.findViewById(R.id.tv_topic);
        tvTopic.setText(listTitle.get(groupPosition).getChannel_name());
        tvTopic.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(mContext));

        final CheckBox cbBox = (CheckBox) convertView.findViewById(R.id.cb_header);
        final ChannelData ChannelData = listTitle.get(groupPosition);
        tvTopic.setText(ChannelData.getChannel_name());

        if (categoryIdList != null) {
            if (categoryIdList.contains(ChannelData.getId())) {
                cbBox.setChecked(true);
                categoryHashMap.put(groupPosition + "", ChannelData.getId());
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, categoryHashMap, 2, false);
            }
        }

        convertView.findViewById(R.id.rel_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryIdList.clear();
                if (cbBox.isChecked()) {
                    cbBox.setChecked(false);
                    if (categoryHashMap.containsKey(groupPosition+"")) {
                        categoryHashMap.remove(groupPosition+"");
                    }
                } else {
                    cbBox.setChecked(true);
                    categoryHashMap.put(groupPosition + "", ChannelData.getId());
                }
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, categoryHashMap, 2, true);
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_feed_news_topic, null);
        }


        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_nav_list, null);
        }
        TextView tvHeader = (TextView) convertView.findViewById(R.id.tv_header);
        final CheckBox cbBox = (CheckBox) convertView.findViewById(R.id.cb_header);
        final ChannelData child = (ChannelData) getChild(groupPosition, childPosition);
        tvHeader.setText(child.getChannel_name());
        tvHeader.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(mContext));

        if (childCategoryIdList != null) {
            if (childCategoryIdList.contains(child.getId())) {
                cbBox.setChecked(true);
                categoryHashMap.put(groupPosition + "-" + childPosition + "", child.getId());
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, categoryHashMap, 2, false);
            }
        }

        convertView.findViewById(R.id.rel_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                childCategoryIdList.clear();
                if (cbBox.isChecked()) {
                    cbBox.setChecked(false);
                    if (categoryHashMap.containsKey(groupPosition + "-" + childPosition + "")) {
                        categoryHashMap.remove(groupPosition + "-" + childPosition + "");
                    }
                } else {
                    cbBox.setChecked(true);
                    categoryHashMap.put(groupPosition + "-" + childPosition + "", child.getId());
                }
                feedMainNewsActivity.openInterfaceListener(groupPosition, expandableListView, categoryHashMap, 2, false);
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}