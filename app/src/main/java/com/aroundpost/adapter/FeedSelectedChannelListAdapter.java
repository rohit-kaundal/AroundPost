package com.aroundpost.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aroundpost.AppPreference;
import com.aroundpost.R;
import com.aroundpost.response.ChannelData;
import com.aroundpost.response.LocationData;
import com.aroundpost.utils.BasicUtils;

/**
 * Created by Manish on 3/25/2017.
 */

public class FeedSelectedChannelListAdapter extends ArrayAdapter<ChannelData> {
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private String id;
    private boolean isNewsMode;

    public FeedSelectedChannelListAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
        this.mContext = context;
        id = AppPreference.getAppPreference(mContext).getPreferenceCategory();
        if(id.length()>0 && id.startsWith("C")){
            isNewsMode = true;
            id = id.substring(1);
        }
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        FeedSelectedChannelListAdapter.ViewHolder viewHolder;
        View view = convertView;
        if(view == null){
            viewHolder = new FeedSelectedChannelListAdapter.ViewHolder();
            view = mLayoutInflater.inflate(R.layout.adapter_feed_selected_list, parent, false);
            viewHolder.textView = (TextView) view.findViewById(R.id.text_location);
            view.setTag(viewHolder);
        }else{
            viewHolder = (FeedSelectedChannelListAdapter.ViewHolder) view.getTag();
        }
        viewHolder.textView.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(mContext));
        /*if(isNewsMode && id.equals(getItem(position).getId())){
            view.setBackgroundColor(mContext.getColor(R.color.new_background));
        }*/
        viewHolder.textView.setText(getItem(position).getChannel_name());
        return view;
    }

    public class ViewHolder{
        public TextView textView;
    }
}