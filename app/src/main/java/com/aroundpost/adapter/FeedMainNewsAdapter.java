package com.aroundpost.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.aroundpost.R;
import com.aroundpost.response.UserInterestPlaces;

import java.util.ArrayList;

/**
 * Created by Manish on 12/26/2016.
 */

public class FeedMainNewsAdapter extends RecyclerView.Adapter<FeedMainNewsAdapter.MyViewHolder> {
    private ArrayList<UserInterestPlaces> mUserInterestPlacesArrayList;
    private Context mContext;

    public FeedMainNewsAdapter(Context context, ArrayList<UserInterestPlaces> userInterestPlacesArrayList){
        this.mUserInterestPlacesArrayList = userInterestPlacesArrayList;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_nav_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        UserInterestPlaces userInterestPlaces = mUserInterestPlacesArrayList.get(position);
    }

    @Override
    public int getItemCount() {
        if(mUserInterestPlacesArrayList != null){
            return mUserInterestPlacesArrayList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public CheckBox checkBox;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_header);
            checkBox = (CheckBox) view.findViewById(R.id.cb_header);
        }
    }

}
