package com.aroundpost.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aroundpost.AppPreference;
import com.aroundpost.R;
import com.aroundpost.interfaces.OnNewsFeedClickInterface;
import com.aroundpost.response.SearchResultNewsModel;
import com.aroundpost.utils.BasicUtils;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Manish on 9/23/2016.
 */
public class FeedNewsAdapter extends RecyclerView.Adapter<FeedNewsAdapter.FeedNewsItemHolder> {
    private final SimpleDateFormat format;
    private Context mContext;
    private ArrayList<SearchResultNewsModel> mFeedNewsModelList;
    private OnNewsFeedClickInterface mOnNewsFeedClickInterface;
    private Typeface mDevengariTypeface;
    private String imageUrl;
    private String source_link;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    public FeedNewsAdapter(Context context, ArrayList<SearchResultNewsModel> feedNewsModelList, OnNewsFeedClickInterface onNewsFeedClickInterface) {
        this.mContext = context;
        this.mFeedNewsModelList = feedNewsModelList;
        this.mOnNewsFeedClickInterface = onNewsFeedClickInterface;
        mDevengariTypeface = BasicUtils.getBasicUtilInstance().setAvirLTStdBook(context);
        format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        prefs = mContext.getSharedPreferences(AppPreference.AROUND_POST_PREFERENCE, mContext.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public FeedNewsItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_feed_news_item, parent, false);
        return new FeedNewsItemHolder(view);
    }

    @Override
    public void onBindViewHolder(FeedNewsItemHolder holder, final int position) {
        Log.d("position", position + "");
        final SearchResultNewsModel feedNewsModel = mFeedNewsModelList.get(position);
        Log.d("feedNmod ", feedNewsModel.getStory() + "");

        holder.tvNewsHeading.setText(feedNewsModel.getStory().getStory_title());
        /*try {
            Date date = format.parse(feedNewsModel.getStory().getCreated());
            Log.d("Time ", date.getTime()+"");
            int year = 1900 + date.getYear();
            holder.tvNewsTiming.setText(date.getDate()+"-"+ date.getMonth() + "-" + year +"");
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        holder.tvNewsTiming.setText(BasicUtils.getBasicUtilInstance().timeCalculation(feedNewsModel.getStory().getCreated()));
        //holder.tvNewsTiming.setText(feedNewsModel.getNewsTiming());
        // holder.tvNewsEditorName.setText(feedNewsModel.getStory().getUser().getFirst_name());
        Log.e("Image Url == ", imageUrl + feedNewsModel.getStory().getStory_image());
        Log.e("Image Url == ", "Latitude" + feedNewsModel.getStory().getLatitude());
        Log.e("Image Url == ", "Diatance" + feedNewsModel.getStory().getDistance());
        if (feedNewsModel.getDistance() != null && feedNewsModel.getDistance().length() > 0){
            Log.d("Image Url == ", "************Distance**********" + feedNewsModel.getStory().getDistance());
            double doubleDistance = Double.parseDouble(feedNewsModel.getDistance());
            double roundDistance = round(doubleDistance,2);// // returns 200.35
            String strDistance = " "+roundDistance+ " Kms away" ;
            holder.tvNewsEditorName.setText(strDistance);
        }else{
            holder.tvNewsEditorName.setText(feedNewsModel.getStory().getUser().getFirst_name() + " " + feedNewsModel.getStory().getUser().getLast_name());
        }


        /*(double lat1, double lon1, double lat2, double lon2)*/

       /* double doubleDistance = distance(Double.parseDouble(prefs.getString(AppPreference.LATITUDE,"")),Double.parseDouble(prefs.getString(AppPreference.LONGITUDE,"")),
                Double.parseDouble(feedNewsModel.getStory().getLatitude()),Double.parseDouble(feedNewsModel.getStory().getLongitude()));


        double roundDistance = round(doubleDistance,2);// // returns 200.35
        String strDistance = ""+roundDistance;

        Log.e("**********","****Distance****"+strDistance);
        if (strDistance.length() > 0){
            holder.tvNewsEditorName.setText(strDistance + " km" );
        }else{
            holder.tvNewsEditorName.setText("0 km");
        }*/
        /*
       * add String source  if string null*/
        try {
            if (imageUrl == null) {
                this.imageUrl = AppPreference.getAppPreference(mContext).getImageURL();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String fullUrl = imageUrl + feedNewsModel.getStory().getStory_image();
        if (!imageUrl.startsWith("http://")) {
            fullUrl = "http://" + fullUrl;
        }
        Picasso.with(mContext).load(fullUrl).into(holder.ivNewsImage);
        //Log.d("Heading position", feedNewsModel.getNewsHeading() + position);
        holder.tvNewsHeading.setTypeface(mDevengariTypeface);
        //source_link = feedNewsModel.getStory().getStory_source();


        holder.mRelFullView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnNewsFeedClickInterface != null) {

                    source_link = mFeedNewsModelList.get(position).getStory().getStory_source();
                    mOnNewsFeedClickInterface.onNewsFeedItemClick(source_link, imageUrl, mFeedNewsModelList, feedNewsModel, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFeedNewsModelList.size();
    }

    public class FeedNewsItemHolder extends RecyclerView.ViewHolder {
        protected ImageView ivNewsImage;
        protected TextView tvNewsHeading, tvNewsTiming, tvNewsEditorName;
        protected RelativeLayout mRelFullView;

        public FeedNewsItemHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            ivNewsImage = (ImageView) itemView.findViewById(R.id.imv_news);
            tvNewsHeading = (TextView) itemView.findViewById(R.id.tv_news_heading);
            tvNewsTiming = (TextView) itemView.findViewById(R.id.tv_news_time);
            tvNewsEditorName = (TextView) itemView.findViewById(R.id.tv_news_editor);
            mRelFullView = (RelativeLayout) itemView.findViewById(R.id.rel_full_view);
        }
    }

    public void UpdateArrayList(ArrayList<SearchResultNewsModel> arrayList) {
        this.mFeedNewsModelList = arrayList;
//        notifyDataSetChanged();
    }

    public double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
