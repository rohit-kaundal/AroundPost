package com.aroundpost.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aroundpost.R;
import com.aroundpost.activity.NewFeedNewsActivity;
import com.aroundpost.activity.SourceLinkOpen;
import com.aroundpost.response.SearchResultNewsModel;
import com.aroundpost.response.StoryModel;
import com.aroundpost.utils.BasicUtils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Manish on 3/28/2017.
 */

public class SlidingFragment extends Fragment {
    //    private static final int EXTERNAL_PERMISSION_REQUEST = 10;
    private static final int INFINITE_STORAGE_PERMISSION_REQUEST = 10;
    String currentFragment = "";
    LinearLayout rel_news;
    int width = 0;
    int height = 0;
    ViewGroup rootView;
    private ArrayList<SearchResultNewsModel> mFeedNewsModelList = new ArrayList<>();
    private SearchResultNewsModel mFeedNewsModel;
    private ImageView ivPreviousView, ivNextNews, ivNewsImage, screenshotLogo;
    private TextView tvNewsTiming, tvNewsPublisher, tvNewsHeader, tvNewsDescription;
    private String imageUrl;
    private int selectedPosition;
    private String source_url = "";
    private AppData mAppData;
    private RelativeLayout mainview;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mAppData = (AppData) context;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        imageUrl = bundle.getString("imageUrl");
        selectedPosition = bundle.getInt("position");
        source_url = bundle.getString("source_url");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_feed_news_detail, container, false);

        mFeedNewsModelList = ((NewFeedNewsActivity) getActivity()).getmFeedNewsModelList();

        ((NewFeedNewsActivity) getActivity()).mPosition = selectedPosition;
        ((NewFeedNewsActivity) getActivity()).hideShowLayoutSliding(View.GONE);
        // source_url = mFeedNewsModelList.get(selectedPosition).getStory().getStory_source();
        initView(rootView);

        final boolean[] value = {true};
        firebaseAnalytics();
        mainview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getActionMasked();
                switch (action) {
                    case 1:
                        Log.d("Up", "ineiebdbcdj");
                        int c = 0;
                        Log.d("dd", c + "");
                        if (value[0]) {
                            value[0] = false;
                            //this method to show animation
                            ((NewFeedNewsActivity) getActivity()).hideShowLayout(View.VISIBLE, false);
                            //this method is to click center textview to go back to main screen
                            NewFeedNewsActivity.textNews.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getActivity().onBackPressed();
                                }
                            });
                        } else {
                            value[0] = true;
                            ((NewFeedNewsActivity) getActivity()).hideShowLayout(View.GONE, true);
                        }
                        break;
                }
                return true;
            }
        });
        mFeedNewsModel = mAppData.getData(selectedPosition);
        setDataFromCurrentModel(mFeedNewsModel);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((NewFeedNewsActivity) getActivity()).currentFragment("slide");
        NewFeedNewsActivity.textNews.setEnabled(true);

    }

    public void initView(View view) {
        ivNewsImage = (ImageView) view.findViewById(R.id.imv_news);
        screenshotLogo = (ImageView) view.findViewById(R.id.screenshotLogo);
        tvNewsTiming = (TextView) view.findViewById(R.id.tv_timing);
        tvNewsPublisher = (TextView) view.findViewById(R.id.tv_editor);
        tvNewsHeader = (TextView) view.findViewById(R.id.tv_news_heading);
        tvNewsDescription = (TextView) view.findViewById(R.id.tv_news_description);

        rel_news = (LinearLayout) view.findViewById(R.id.linearnews);
        mainview = (RelativeLayout) view.findViewById(R.id.mainview);
        screenshotLogo.setVisibility(View.GONE);
        getShareNews();
        getSourceLink();
        tvNewsDescription.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdBook(getActivity()));

    }

    //showing link that is getting from api.
    private void getSourceLink() {
        NewFeedNewsActivity.ivSaveStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!source_url.equals("")) {
                    Intent intent = new Intent(getActivity(), SourceLinkOpen.class);
                    intent.putExtra("source_url", source_url);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "There is no source.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //taking screenshot  of screen on clicking of share button
    private void getShareNews() {
        NewFeedNewsActivity.ivShareNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkAndRequestPermissions()) {
                        takeScreenShotAndShare();
                    }
                } else {
                    // write your logic here
                    takeScreenShotAndShare();
                }

            }
        });
    }


    // method to take screenshot
    public void takeScreenShotAndShare() {
/*
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getActivity().getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            Uri bmpUri = Uri.fromFile(imageFile);
            final Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
            emailIntent1.setType("image/png");
            startActivity(emailIntent1);
//            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }*/

        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
        //screenshotLogo.setVisibility(View.VISIBLE);
        try {


            Bitmap bm = screenShot(getActivity().getWindow().getDecorView().findViewById(R.id.linearnews));
            Bitmap scrLogo = BitmapFactory.decodeResource(getContext().getResources(),R.drawable.ap);
           // Bitmap scaledImg = Bitmap.createScaledBitmap(scrLogo, ((int) Math.round(scrLogo.getWidth() * 0.9)), ((int) Math.round(scrLogo.getHeight() * 0.9)), false);

            //Merge bitmaps
            Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
            Canvas result = new Canvas(bm);
            result.drawBitmap(scrLogo, Math.round(bm.getWidth() *.25), Math.round(bm.getHeight() * .85 ), paint);


            File file = saveBitmap(bm, "aroundpost_screenshot.png");

            Log.i("chase", "filepath: "+file.getAbsolutePath());
            Uri uri = Uri.fromFile(new File(file.getAbsolutePath()));
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Via aroundpost.com/dl");
           // shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Shared Via AroundPost.");
            Spanned txtHtml = Html.fromHtml("Via <a href='http://www.aroundpost.com/dl'>aroundpost.com/dl</a>");
            shareIntent.putExtra(Intent.EXTRA_HTML_TEXT, txtHtml);

            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            shareIntent.setType("image/*");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(shareIntent, "share via"));

        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
        //screenshotLogo.setVisibility(View.GONE);
    }

    private Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private static File saveBitmap(Bitmap bm, String fileName){
        final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(path);
        if(!dir.exists())
            dir.mkdirs();
        File file = new File(dir, fileName);

        if( file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream fOut = new FileOutputStream(file, false);
            bm.compress(Bitmap.CompressFormat.PNG, 90, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }

    private void setDataFromCurrentModel(SearchResultNewsModel mFeedNewsModel) {
        Log.d("Image Url == ", imageUrl + mFeedNewsModel.getStory().getStory_image());
       // StoryModel tmpModel = mFeedNewsModel.getStory();

        String fullUrl = imageUrl + mFeedNewsModel.getStory().getStory_image();
        if (!imageUrl.startsWith("http://")) {
            fullUrl = "http://" + fullUrl;
        }
        Picasso.with(getActivity()).load(fullUrl).into(ivNewsImage);

        tvNewsTiming.setText(BasicUtils.getBasicUtilInstance().timeCalculation(mFeedNewsModel.getStory().getCreated()));

//        tvNewsPublisher.setText(mFeedNewsModel.getStory().getUser().getFirst_name());
        /*if (mFeedNewsModel.getDistance() != null && mFeedNewsModel.getDistance().length() > 0) {
            Log.d("Image Url == ", "************Distance**********" + mFeedNewsModel.getDistance());
            double doubleDistance = Double.parseDouble(mFeedNewsModel.getDistance());
            double roundDistance = round(doubleDistance, 2);// // returns 200.35
            String strDistance = "" + roundDistance;
            tvNewsPublisher.setText(strDistance);
        } else {
            tvNewsPublisher.setText(mFeedNewsModel.getStory().getUser().getFirst_name() + " " + mFeedNewsModel.getStory().getUser().getLast_name());
        }*/

        tvNewsPublisher.setText(mFeedNewsModel.getStory().getUser().getFirst_name() + " " + mFeedNewsModel.getStory().getUser().getLast_name());
        tvNewsHeader.setText(mFeedNewsModel.getStory().getStory_title());
        tvNewsDescription.setText(mFeedNewsModel.getStory().getStory_content());
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case INFINITE_STORAGE_PERMISSION_REQUEST: {
                if (requestCode == INFINITE_STORAGE_PERMISSION_REQUEST) {
                    boolean isallpermissioongranted = true;
                    for (int i = 0, len = permissions.length; i < len; i++) {
                        String permission = permissions[i];
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            boolean showRationale = shouldShowRequestPermissionRationale(permission);
                            if (!showRationale) {
                                isallpermissioongranted = false;
                                break;
                            }
                        }
                    }
                    if (!isallpermissioongranted) {
                        // popup
                        showPermissionsDialog();
                    } else {
                        if (checkAndRequestPermissions()) {
                            takeScreenShotAndShare();
                        }
                    }

                }
            }
        }
    }

    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(getActivity()).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, SlidingFragment.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SlidingFragment.class.getName());
        mFirebaseAnalytics.logEvent("SlidingFragment", bundle);
    }


    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public interface AppData {
        SearchResultNewsModel getData(int position);
    }


    public void showPermissionsDialog() {
        final Dialog mDialog = new Dialog(getActivity());
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_dialog);

        Button btnCancel = (Button) mDialog.findViewById(R.id.btnCancel);
        Button btnOk = (Button) mDialog.findViewById(R.id.btnOk);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, 100);
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (data != null) {
                if (checkAndRequestPermissions()) {
                    takeScreenShotAndShare();
                }
            }
        }
    }


    ArrayList<String> listpermissionsNeeded = new ArrayList<String>();

    @TargetApi(Build.VERSION_CODES.M)
    public boolean checkAndRequestPermissions() {

        listpermissionsNeeded = new ArrayList<String>();

        int permissionWRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if (permissionWRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listpermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!listpermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listpermissionsNeeded.toArray(new String[listpermissionsNeeded.size()]), INFINITE_STORAGE_PERMISSION_REQUEST);
            return false;
        }

        return true;
    }
}
