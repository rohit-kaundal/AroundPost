package com.aroundpost.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.AroundPostApp;
import com.aroundpost.R;
import com.aroundpost.activity.FacebookAccountKitActivity;
import com.aroundpost.activity.NewFeedNewsActivity;
import com.aroundpost.adapter.FeedNewsAdapter;
import com.aroundpost.interfaces.OnNewsFeedClickInterface;
import com.aroundpost.request.SaveStoryRequest;
import com.aroundpost.response.ChannelData;
import com.aroundpost.response.FeedNewsResponse;
import com.aroundpost.response.FeedSearchModelResponse;
import com.aroundpost.response.GenericResponse;
import com.aroundpost.response.LocationData;
import com.aroundpost.response.SearchResultNewsModel;
import com.aroundpost.utils.JsonUtils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.aroundpost.activity.FeedMainNewsActivity;

/**
 * Created by Manish on 9/22/2016.
 */
public class FeedNewsFragment extends Fragment implements OnNewsFeedClickInterface, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mRecyclerView;
    private FeedNewsAdapter mFeedNewsAdapter;
    private ArrayList<SearchResultNewsModel> mFeedNewsModelList;
    private boolean loading = true;
    int mPastVisibleItems, mVisibleItemCount, mTotalItemCount;
    private LinearLayoutManager mLayoutManager;
    private NewFeedNewsActivity mFeedNewsActivity;
    private int skip = 0;
    private int limit = 10;
    private ProgressBar pbLoadMore;
    private boolean loadMore;
    private TextView mTxtNoNews;
    private int whichApi = 0;
    private int page = 1;
    String newsTitle = "";
    private SwipeRefreshLayout mSwipeContainer;
    Call<FeedNewsResponse> feedNewsResponseCall = null;

    ArrayList<SearchResultNewsModel> arrayList;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onResume() {
        super.onResume();
        ((NewFeedNewsActivity) getActivity()).currentFragment("feed");
//        skip = 0;
//        limit = 10;

      /*  if(mFeedNewsModelList.size()==singleton.getmFeedNewsModelList().size()){
            mFeedNewsModelList.clear();
            mFeedNewsModelList = singleton.getmFeedNewsModelList();

        }else {
            mFeedNewsModelList.clear();
            mFeedNewsAdapter.notifyDataSetChanged();
            getUserStoriesApi(page);
        }*/
//        if (singleton.getmFeedNewsModelList().size() > 0) {
//            loading = false;
//        }
//        mFeedNewsModelList.clear();
//        ArrayList<SearchResultNewsModel> arrayList = new ArrayList<>();
//        arrayList = singleton.getmFeedNewsModelList();
//        mFeedNewsModelList.addAll(arrayList);
//        ((NewFeedNewsActivity) getActivity()).setmFeedNewsModelList(mFeedNewsModelList);
//        mFeedNewsAdapter.notifyDataSetChanged();
//        getUserStoriesApi(page);
        NewFeedNewsActivity.textNews.setEnabled(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_news, container, false);
        Log.d("Fragment === ", getActivity().getClass().getName());
        setHasOptionsMenu(true);
        mFeedNewsActivity = (NewFeedNewsActivity) getActivity();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.feed_recyclerview);
        pbLoadMore = (ProgressBar) view.findViewById(R.id.pb_loadmore);
        mTxtNoNews = (TextView) view.findViewById(R.id.no_news);
        mSwipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.mSwipeContainer);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //((FeedNewsActivity) getActivity()).setTitle("Himachal Pradesh");

        /*todo firebase analytics*/
        firebaseAnalytics();
        mFeedNewsActivity.setTopMargin(true);
        mFeedNewsActivity.feedHideLayout();

        mFeedNewsModelList = new ArrayList<>();

        mFeedNewsAdapter = new FeedNewsAdapter(getActivity(), mFeedNewsModelList, FeedNewsFragment.this);
        mRecyclerView.setAdapter(mFeedNewsAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    mVisibleItemCount = mLayoutManager.getChildCount();
                    mTotalItemCount = mLayoutManager.getItemCount();
                    mPastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((mVisibleItemCount + mPastVisibleItems) >= mTotalItemCount) {
                            loading = false;
                            if (loadMore) {
                                page++;
                                skip = mTotalItemCount; // skip show the total item comes from api after incremented of page
                                pbLoadMore.setVisibility(View.VISIBLE);
                                getUserStoriesApi(page);
                            }
                        }
                    }
                }
            }
        });

        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pbLoadMore.setVisibility(View.GONE);
                loading = true;
                page = 1;
                getUserStoriesApi(page);

            }
        });
        // this condition is for setting data in prefernce that showing screen currently if app is offline or
        // user clicks on list and goes to details screen after coming back  to main screen the data should retain.
        Bundle bundle = getArguments();
        if (bundle != null) {
            boolean hasdata = bundle.getBoolean("hasdata");
            if (hasdata) {
                ArrayList<SearchResultNewsModel> tem = AppPreference.getAppPreference(getActivity()).getFeedList();
                if (tem != null && tem.size() > 0) {
                    newsTitle = AppPreference.getAppPreference(getActivity()).getTitlePref();
                    ((NewFeedNewsActivity) getActivity()).setTitle(newsTitle);
                    skip = AppPreference.getAppPreference(getActivity()).getSkip();
                    limit = AppPreference.getAppPreference(getActivity()).getLimit();
                    page = AppPreference.getAppPreference(getActivity()).getPage();

                    mFeedNewsModelList.clear();
                    feedUserStories(AppPreference.getAppPreference(getActivity()).getImageURL(), tem);
                } else {
                    skip = 0;
                    limit = 10;
                    getApiResponse();
                }
            } else {
                skip = 0;
                limit = 10;
                getApiResponse();
            }
        }
        return view;
    }

    private void getApiResponse() {

        mFeedNewsModelList.clear();
        getUserStoriesApi(page);
    }

    // to refresh page
    @Override
    public void onRefresh() {

        getUserStoriesApi(page);

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private void getUserStoriesApi(int page) {

        if (!loading) {
            mSwipeContainer.setRefreshing(false);
        } else {
            mSwipeContainer.setRefreshing(true);
        }
        whichApi = AppPreference.getAppPreference(getActivity()).getWhichApi();
        boolean isChannelNews = false;
        String id = AppPreference.getAppPreference(getActivity()).getPreferenceCategory();
        if (id.startsWith("C")) {
            isChannelNews = true;
        }

        if (id.length() > 0) {
            id = id.substring(1);
        }


        Call<FeedSearchModelResponse> feedSearchModelResponseCall = null;
        if (isChannelNews) {
            ArrayList<ChannelData> channelDatasArrayList = AppPreference.getAppPreference(getActivity()).getChannelData().getData();
            if (channelDatasArrayList != null) {
                for (ChannelData channelData : channelDatasArrayList) {
                    if (id.equals(channelData.getId())) {
                        newsTitle = channelData.getChannel_name();
                        ((NewFeedNewsActivity) getActivity()).setTitle(newsTitle);
                    }
                }
            }

            if (id != null && id.length() > 0) {
                Log.d("id -- ", id);

                feedNewsResponseCall = AroundPostApp.getAroundPostInterface(getActivity()).getFeedChannelStories(String.valueOf(page), id);
            } else {
                feedNewsResponseCall = AroundPostApp.getAroundPostInterface(getActivity()).getFeedChannelStories();
            }
        } else {

            ArrayList<LocationData> locationDataArrayList = AppPreference.getAppPreference(getActivity()).getLocationData().getData();
            if (locationDataArrayList != null) {
                for (LocationData locationData : locationDataArrayList) {

                    if (id.equals(locationData.getId())) {
                        if (whichApi != 1) {
                            newsTitle = locationData.getLocation_name();
                            ((NewFeedNewsActivity) getActivity()).setTitle(newsTitle);


                        } else {
                            //setting preference arraylist by default null page 1 skip 0 and limit 10
                            // the data will come in list to overide this method has used.
                            AppPreference.getAppPreference(getActivity()).setFeedNeews(null, 0, 10, 1);

                            AppPreference.getAppPreference(getActivity()).setFeedNeews(mFeedNewsModelList, skip, limit, page);
                            ((NewFeedNewsActivity) getActivity()).setNearByLocationTitle(AppPreference.getAppPreference(getActivity()).getTypeDistance());

                        }
                    }
                }
            }
            int distanceCovered = AppPreference.getAppPreference(getActivity()).getDistanceCovered();
            if (whichApi == 1) {
                //http://aroundpost.com/api/Stories?page=1&location=nearby&lat=31.1048&long=77.1734&radius=300
                Location location = JsonUtils.fromJson(AppPreference.getAppPreference(getActivity()).getUserLocation(), Location.class);
                Log.d("Api == ", whichApi + "");
                feedNewsResponseCall = AroundPostApp.getAroundPostInterface(getActivity()).getFeedNearByNewsStories(String.valueOf(page), "nearby", String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), distanceCovered);
               // ((NewFeedNewsActivity) getActivity()).setTitle("");
            } else if (whichApi == 2) {
                Location location = JsonUtils.fromJson(AppPreference.getAppPreference(getActivity()).getUserLocation(), Location.class);
                Log.d("Api == ", whichApi + " location " + location);
                feedNewsResponseCall = AroundPostApp.getAroundPostInterface(getActivity()).getFeedUnSeenNewsStories(String.valueOf(page), "nearby", String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), distanceCovered, "unseen");
            } else if (whichApi == 3) {
                mFeedNewsModelList.clear();
                ((NewFeedNewsActivity) getActivity()).setmFeedNewsModelList(mFeedNewsModelList);
                mFeedNewsAdapter.notifyDataSetChanged();
                // String query = ((NewFeedNewsActivity) getActivity()).query;
                newsTitle = ((NewFeedNewsActivity) getActivity()).query;
                ((NewFeedNewsActivity) getActivity()).setTitle(newsTitle);
                feedNewsResponseCall = AroundPostApp.getAroundPostInterface(getActivity()).getNewsSearch(newsTitle);
            } else if (whichApi == 4) {
                newsTitle = "My Stories";
                ((NewFeedNewsActivity) getActivity()).setTitle(newsTitle);
                mFeedNewsModelList.clear();
                ((NewFeedNewsActivity) getActivity()).setmFeedNewsModelList(mFeedNewsModelList);
                mFeedNewsAdapter.notifyDataSetChanged();
                feedNewsResponseCall = AroundPostApp.getAroundPostInterface(getActivity()).getMyStories();
            } else {
                //id = "2";
                if (id != null && id.length() > 0) {
                    Log.d("id -- ", id);
                    feedNewsResponseCall = AroundPostApp.getAroundPostInterface(getActivity()).getFeedNewsStories(String.valueOf(page), id);
                } else {
                    feedNewsResponseCall = AroundPostApp.getAroundPostInterface(getActivity()).getFeedNewsStories();
                }
            }
        }

        feedNewsResponseCall.enqueue(new Callback<FeedNewsResponse>() {
            @Override
            public void onResponse(Call<FeedNewsResponse> call, Response<FeedNewsResponse> response) {
                loading = true;
                pbLoadMore.setVisibility(View.GONE);
                if (mSwipeContainer.isRefreshing()) {
                    mSwipeContainer.setRefreshing(false);
                    mFeedNewsModelList.clear();
                }
                if (response.isSuccessful()) {
                    FeedNewsResponse body = response.body();
                    try {
                        if (body.getData() != null) {
                            ArrayList<SearchResultNewsModel> feedNewsModelArrayList = body.getData().getSearchResult();

                            feedUserStories(body.getData().getImgUrl(), feedNewsModelArrayList);
                        } else {
                            if (body.getStatusCode() == 401) {
                                //User Unauthrozed.
                                unauthorizedUser(body.getMessage());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedNewsResponse> call, Throwable t) {
                t.printStackTrace();
                pbLoadMore.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Log.d("===", "");
                    mTxtNoNews.setVisibility(View.VISIBLE);
                    mTxtNoNews.setText("No Internet connection");
                }
            }
        });
        if (whichApi != 1)
            AppPreference.getAppPreference(getActivity()).setTitlePref(newsTitle);
    }


    private void unauthorizedUser(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        AppPreference.getAppPreference(getActivity()).clearPreference();
        Intent intent = new Intent(getActivity(), FacebookAccountKitActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void feedUserStories(String imgUrl, ArrayList<SearchResultNewsModel> feedNewsModelArrayList) {
        if (feedNewsModelArrayList != null && feedNewsModelArrayList.size() > 0) {
            mFeedNewsModelList.addAll(feedNewsModelArrayList);

            ((NewFeedNewsActivity) getActivity()).setmFeedNewsModelList(mFeedNewsModelList);
            AppPreference.getAppPreference(getActivity()).setImageURL(imgUrl);
            mFeedNewsAdapter.setImageUrl(imgUrl);
            mFeedNewsAdapter.notifyDataSetChanged();
            loadMore = true;
        } else {
            if (mFeedNewsModelList != null && mFeedNewsModelList.size() > 0) {
            } else {
                mTxtNoNews.setVisibility(View.VISIBLE);
            }
            loadMore = false;
        }
        AppPreference.getAppPreference(getActivity()).setFeedNeews(null, 0, 10, 1);
        AppPreference.getAppPreference(getActivity()).setFeedNeews(mFeedNewsModelList, skip, limit, page);
    }

    @Override
    public void onNewsFeedItemClick(String sourceUrl, String imageUrl, List<SearchResultNewsModel> feedNewsModelList, SearchResultNewsModel feedNewsModel, int position) {
        //FeedItem Clicked and open for specific news.
        //Api to have seen story.
        SaveStoryRequest saveStoryRequest = new SaveStoryRequest();
        saveStoryRequest.setStory_id(feedNewsModel.getStory_id());
        Call<GenericResponse> genericResponseCall = AroundPostApp.getAroundPostInterface(getActivity()).setUserStorySeen(saveStoryRequest);

        genericResponseCall.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {

            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {

            }
        });
        ((NewFeedNewsActivity) getActivity()).setmStoryId(feedNewsModelList.get(position).getStory_id());
        Fragment fragment = new InShortLikeFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("listCount", feedNewsModelList.size());
        bundle.putInt("position", position);
        bundle.putString("imageUrl", imageUrl);
        bundle.putString("title", newsTitle);
        bundle.putString("source_url", sourceUrl);
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onNewsFeedItemLongClick(SearchResultNewsModel feedNewsModel, int position) {
        //Display popup to delete the news feed item.
        CharSequence[] itemsDisplayed = {
                "Delete", "Cancel"
        };

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Delete this news.");
        alertDialog.setItems(itemsDisplayed, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if (position == 0) {
                    mFeedNewsModelList.remove(position);
                    dialog.dismiss();
                } else if (position == 1) {
                    dialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }

    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(getActivity()).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, FeedNewsFragment.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, FeedNewsFragment.class.getName());
        mFirebaseAnalytics.logEvent("FeedNewsFragment", bundle);
    }
}
