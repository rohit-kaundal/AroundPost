package com.aroundpost.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aroundpost.R;
import com.aroundpost.activity.NewFeedNewsActivity;
import com.aroundpost.utils.BasicUtils;
import com.aroundpost.utils.Constants;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manish on 11/12/2016.
 */

public class TutorialScreenFragment extends Fragment {

    private static int PAGE_POSITION;
    private TextView mTVPageText;
    private TextView mTvPageHeading2;
    private ImageView mIVPageHeading1;
    private RelativeLayout mRelSpinner,relative_check;
    private FirebaseAnalytics mFirebaseAnalytics;
    public static TutorialScreenFragment getTutorialFragment(int pagePosition) {
        TutorialScreenFragment tutorialScreenFragment = new TutorialScreenFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.TUTORIAL_SCREEN_POSITION, pagePosition);
        tutorialScreenFragment.setArguments(bundle);
        Log.d("Position: ", pagePosition + "");
        return tutorialScreenFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            PAGE_POSITION = getArguments().getInt(Constants.TUTORIAL_SCREEN_POSITION);
            Log.d("Page Position ", PAGE_POSITION + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tutorial_screen, container, false);
        initView(view);
        mTvPageHeading2.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdBook(getActivity()));
        mTVPageText.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdBook(getActivity()));
        firebaseAnalytics();
        if (PAGE_POSITION == 0) {
            relative_check.setVisibility(View.GONE);
            spinnerVisibilityAndText(View.GONE, getActivity().getString(R.string.around_tutorial_1));
        } else if (PAGE_POSITION == 1) {
            relative_check.setVisibility(View.GONE);
            spinnerVisibilityAndText(View.GONE, getActivity().getString(R.string.around_tutorial_2));
        } else if (PAGE_POSITION == 2) {
            relative_check.setVisibility(View.VISIBLE);
            spinnerVisibilityAndText(View.VISIBLE, getActivity().getString(R.string.around_tutorial_3));
        }
        return view;
    }

    private void initView(View view) {
        mTVPageText = (TextView) view.findViewById(R.id.tv_tutorial_1);
        mIVPageHeading1 = (ImageView) view.findViewById(R.id.tv_heading1);
        mTvPageHeading2 = (TextView) view.findViewById(R.id.tv_heading2);
        mRelSpinner = (RelativeLayout) view.findViewById(R.id.rel_destrict_select);
        relative_check = (RelativeLayout) view.findViewById(R.id.relative_check);
    }

    public void spinnerVisibilityAndText(int state, String message) {
        mTVPageText.setText(message);
        mRelSpinner.setVisibility(state);
        if (state == View.VISIBLE) {
            mTVPageText.setVisibility(View.INVISIBLE);
        }
    }

    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(getActivity()).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, TutorialScreenFragment.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, TutorialScreenFragment.class.getName());
        mFirebaseAnalytics.logEvent("TutorialScreenFragment", bundle);
    }
}
