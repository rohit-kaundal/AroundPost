package com.aroundpost.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.R;
import com.aroundpost.activity.FeedNewsActivity;
import com.aroundpost.activity.NewFeedNewsActivity;
import com.aroundpost.response.FeedNewsModel;
import com.aroundpost.response.SearchResultNewsModel;
import com.aroundpost.utils.BasicUtils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Manish on 9/24/2016.
 */
public class FeedNewsDetailFragment extends Fragment {

    private int mPosition;
    private ArrayList<SearchResultNewsModel> mFeedNewsModelList = new ArrayList<>();
    private SearchResultNewsModel mFeedNewsModel;
    private ImageView ivPreviousView, ivNextNews, ivNewsImage;
    private TextView tvNewsTiming, tvNewsPublisher, tvNewsHeader, tvNewsDescription;
    private String imageUrl;
    private RelativeLayout mainview;
    private LinearLayout linearNews;
    private float mLastTouchY;
    private String newTitle;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        mPosition = bundle.getInt("position");
        imageUrl = bundle.getString("imageUrl");
        //newTitle = bundle.getString("title");
        ((NewFeedNewsActivity)getActivity()).mPosition = mPosition;
        mFeedNewsModelList = ((NewFeedNewsActivity) getActivity()).getmFeedNewsModelList();
        mFeedNewsModel = mFeedNewsModelList.get(mPosition);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_news_detail, container, false);
        initView(view);
        firebaseAnalytics();
        //((FeedNewsActivity) getActivity()).setTitle(newTitle);
        ((NewFeedNewsActivity)getActivity()).hideShowLayout(View.GONE, true);
        ((NewFeedNewsActivity) getActivity()).setTopMargin(false);
        setDataFromCurrentModel(mFeedNewsModel);
        return view;
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private void setDataFromCurrentModel(SearchResultNewsModel mFeedNewsModel) {
        Log.d("Image Url == ", imageUrl + mFeedNewsModel.getStory().getStory_image());
        String fullUrl = imageUrl + mFeedNewsModel.getStory().getStory_image();
        if(!imageUrl.startsWith("http://")){
            fullUrl = "http://" + fullUrl;
        }
        Picasso.with(getActivity()).load(fullUrl).into(ivNewsImage);
        tvNewsTiming.setText(BasicUtils.getBasicUtilInstance().timeCalculation(mFeedNewsModel.getStory().getCreated()));
        //YYYY-MM-DDThh:mm:ssTZD
        //yyyy-MM-dd'T'HH:mm:ss.SSSZ
       /* SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        try {
            Date date = format.parse(mFeedNewsModel.getStory().getCreated());
            Log.d("Time ", date.getTime()+"");
            int year = 1900 + date.getYear();
            tvNewsTiming.setText(date.getDate()+"-"+ date.getMonth() + "-" + year +"");
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

       // tvNewsPublisher.setText(mFeedNewsModel.getStory().getUser().getFirst_name());
       if (mFeedNewsModel.getDistance() != null && mFeedNewsModel.getDistance().length() > 0) {
            Log.d("Image Url == ", "************Distance**********" + mFeedNewsModel.getDistance());
            double doubleDistance = Double.parseDouble(mFeedNewsModel.getDistance());
            double roundDistance = round(doubleDistance, 2);// // returns 200.35
            String strDistance = "" + roundDistance + "Kms";
            tvNewsPublisher.setText(strDistance);
        } else {
            tvNewsPublisher.setText(mFeedNewsModel.getStory().getUser().getFirst_name() + " " + mFeedNewsModel.getStory().getUser().getLast_name());
        }


     //   tvNewsPublisher.setText(mFeedNewsModel.getStory().getUser().getFirst_name());
        tvNewsHeader.setText(mFeedNewsModel.getStory().getStory_title());
        tvNewsDescription.setText(mFeedNewsModel.getStory().getStory_content());
        ((NewFeedNewsActivity) getActivity()).setmStoryId(mFeedNewsModel.getStory_id());
    }

    public void initView(View view) {
        ivNewsImage = (ImageView) view.findViewById(R.id.imv_news);
        tvNewsTiming = (TextView) view.findViewById(R.id.tv_timing);
        tvNewsPublisher = (TextView) view.findViewById(R.id.tv_editor);
        tvNewsHeader = (TextView) view.findViewById(R.id.tv_news_heading);
        tvNewsDescription = (TextView) view.findViewById(R.id.tv_news_description);
        mainview = (RelativeLayout) view.findViewById(R.id.mainview);
        linearNews = (LinearLayout) view.findViewById(R.id.linearnews);

        tvNewsHeader.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdBook(getActivity()));
        tvNewsDescription.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdBook(getActivity()));
        final boolean[] value = {true};
        mainview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action){
                    case MotionEvent.ACTION_DOWN:
                        if(value[0]){
                            value[0] = false;
                            ((NewFeedNewsActivity) getActivity()).hideShowLayout(View.VISIBLE, true);
                        }else{
                            value[0] = true;
                            ((NewFeedNewsActivity) getActivity()).hideShowLayout(View.GONE, true);
                        }
                        break;
                }
                return false;
            }
        });
    }

    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(getActivity()).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, FeedNewsDetailFragment.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, FeedNewsDetailFragment.class.getName());
        mFirebaseAnalytics.logEvent("FeedNewsDetailFragment", bundle);
    }
}
