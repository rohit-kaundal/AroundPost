package com.aroundpost.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aroundpost.AppPreference;
import com.aroundpost.R;
import com.aroundpost.activity.FacebookAccountKitActivity;
import com.aroundpost.activity.FeedSelectedListActivity;
import com.aroundpost.activity.NewFeedNewsActivity;
import com.aroundpost.activity.SettingActivity;
import com.aroundpost.adapter.LocationSpinnerAdapter;
import com.aroundpost.interfaces.LocationDataInterface;
import com.aroundpost.response.LocationData;
import com.aroundpost.response.LocationResponse;
import com.aroundpost.utils.BasicUtils;
import com.aroundpost.utils.Constants;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

/**
 * Created by Manish on 12/13/2016.
 */

public class TutorialScreenFragment1 extends Fragment implements LocationDataInterface {

    private static int PAGE_POSITION;
    private TextView mTVPageText;
    private TextView mTvPageHeading2;
    private ImageView mIVPageHeading1;
    private RelativeLayout mRelSpinner, relative_check;
    private Spinner mSpinnerLocation;
    private LocationSpinnerAdapter mLocationSpinnerAdapter;
    private CheckBox checkEnglish, checkHindi;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean select = false;
    ImageView imgRightArrow;

    private FirebaseAnalytics mFirebaseAnalytics;
    ArrayList<LocationData> locationDataArrayList = new ArrayList<LocationData>();

    public static TutorialScreenFragment1 getTutorialFragment1(int pagePosition) {
        TutorialScreenFragment1 tutorialScreenFragment = new TutorialScreenFragment1();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.TUTORIAL_SCREEN_POSITION, pagePosition);
        tutorialScreenFragment.setArguments(bundle);
        Log.d("Position: ", pagePosition + "");
        return tutorialScreenFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            PAGE_POSITION = getArguments().getInt(Constants.TUTORIAL_SCREEN_POSITION);

            Log.e("Page Position ++++++++", PAGE_POSITION + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tutorial_screen, container, false);
        initView(view);
        getPrefs();
        mTvPageHeading2.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdBook(getActivity()));
        mTVPageText.setTypeface(BasicUtils.getBasicUtilInstance().setAvirLTStdBook(getActivity()));
        firebaseAnalytics();
        if (PAGE_POSITION == 0) {
            imgRightArrow.setVisibility(View.VISIBLE);
            relative_check.setVisibility(View.GONE);
            spinnerVisibilityAndText(View.GONE, getActivity().getString(R.string.around_tutorial_1));
        } else if (PAGE_POSITION == 1) {
            imgRightArrow.setVisibility(View.VISIBLE);
            relative_check.setVisibility(View.GONE);
            spinnerVisibilityAndText(View.GONE, getActivity().getString(R.string.around_tutorial_2));
        } else if (PAGE_POSITION == 2) {
            imgRightArrow.setVisibility(View.GONE);
            relative_check.setVisibility(View.VISIBLE);
            spinnerVisibilityAndText(View.VISIBLE, getActivity().getString(R.string.around_tutorial_3));
        }

        if (PAGE_POSITION == 2) {
            LocationResponse locationResponse = AppPreference.getAppPreference(getActivity()).getLocationData();
            LocationData locationData = new LocationData();
            locationData.setId("-1");
            locationData.setLocation_name("Please select your district");
            locationData.setParent_id("-1");
            locationDataArrayList.add(locationData);
            if (locationResponse.getData() != null) {
                locationDataArrayList.addAll(locationResponse.getData());
            }


            if (locationDataArrayList.size() > 0) {
                mLocationSpinnerAdapter = new LocationSpinnerAdapter(getActivity(), locationDataArrayList, this);
                mSpinnerLocation.setAdapter(mLocationSpinnerAdapter);
                //mLocationSpinnerAdapter.addAll(locationDataArrayList);
                mLocationSpinnerAdapter.notifyDataSetChanged();
            }


        }

        mSpinnerLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    if (select==false) {
                        if (preferences.getString(AppPreference.language_type, "").equals("") && preferences.getString(AppPreference.language_type, "").equals("")) {
                            showAlertDialog("Please Select one language at least.");
                            mSpinnerLocation.setSelection(0);

                        }
                    }
                    if (checkEnglish.isChecked() && checkHindi.isChecked()) {
                        editor.putString(AppPreference.language_type, "1,2");
                        editor.commit();
                    }

                    if (select==true) {

                        LocationData locationData = (LocationData) parent.getItemAtPosition(position);
                        AppPreference.getAppPreference(getActivity()).setPreferedCategory("L" + locationData.getId());
                        AppPreference.getAppPreference(getActivity()).setLocationNewsPreference(AppPreference.getAppPreference(getActivity()).getLocationNewsPreference() + "," + String.valueOf(locationData.getId()));
                        Log.e("ID selected ","***********ID*********"+ AppPreference.getAppPreference(getActivity()).getLocationNewsPreference());


                        Intent intent = new Intent(getActivity(), FacebookAccountKitActivity.class);
                        startActivity(intent);
                        getActivity().finish();

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    private void getPrefs() {
        preferences = getActivity().getSharedPreferences(AppPreference.AROUND_POST_PREFERENCE, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    private void initView(View view) {
        imgRightArrow = (ImageView)view.findViewById(R.id.imgRightArrow);
        mTVPageText = (TextView) view.findViewById(R.id.tv_tutorial_1);
        mIVPageHeading1 = (ImageView) view.findViewById(R.id.tv_heading1);
        mTvPageHeading2 = (TextView) view.findViewById(R.id.tv_heading2);
        mRelSpinner = (RelativeLayout) view.findViewById(R.id.rel_spinner);
        mSpinnerLocation = (Spinner) view.findViewById(R.id.spinner_tutorial);
        relative_check = (RelativeLayout) view.findViewById(R.id.relative_check);
        checkEnglish = (CheckBox) view.findViewById(R.id.checkEnglish);
        checkHindi = (CheckBox) view.findViewById(R.id.checkHindi);
        setListeners();
    }

    private void setListeners() {
        //checkbox to select language for news
        checkEnglish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    select = true;
                    editor.putString(AppPreference.language_type, "2");
                    editor.commit();
                } else {
                    select=false;
                    editor.putString(AppPreference.language_type, "");
                    editor.commit();

                }
            }
        });

        checkHindi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    select = true;
                    editor.putString(AppPreference.language_type, "1");
                    editor.commit();

                } else {
                    editor.putString(AppPreference.language_type, "");
                    editor.commit();

                   select = false;
                }
            }
        });


    }

    public void spinnerVisibilityAndText(int state, String message) {
        mTVPageText.setText(message);
        mRelSpinner.setVisibility(state);
        if (state == View.VISIBLE) {
            mTVPageText.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void locationDataListener(LocationData locationData) {

    }

    private void showAlertDialog(String emptyFieldName) {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        // Setting Dialog Message
        alertDialog.setMessage(emptyFieldName);

        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                alertDialog.dismiss();


            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(getActivity()).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, NewFeedNewsActivity.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, NewFeedNewsActivity.class.getName());
        mFirebaseAnalytics.logEvent("NewFeedNewsActivity", bundle);
    }



}