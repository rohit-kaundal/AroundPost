package com.aroundpost.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aroundpost.R;
import com.aroundpost.activity.FeedNewsActivity;
import com.aroundpost.activity.NewFeedNewsActivity;
import com.aroundpost.adapter.ScreenSlidePagerAdapter;
import com.aroundpost.customview.VerticalViewPager;
import com.aroundpost.response.SearchResultNewsModel;

import java.util.List;

/**
 * Created by Manish on 3/28/2017.
 */

public class InShortLikeFragment extends Fragment {
    private String imageUrl,source_url;
    private int listCount, position;
    List<SearchResultNewsModel> feedNewsModelList;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        imageUrl = bundle.getString("imageUrl");
        listCount = bundle.getInt("listCount");
        position = bundle.getInt("position");
        source_url = bundle.getString("source_url");
      /*  if(bundle.getSerializable("list")!=null) {
            feedNewsModelList = (List<SearchResultNewsModel>) bundle.getSerializable("list");
        }*/
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inshort_like_view, container, false);
        ((NewFeedNewsActivity) getActivity()).setTopMargin(false);
        ScreenSlidePagerAdapter screenSlidePagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager(), imageUrl,source_url, listCount, position);
        VerticalViewPager    verticalViewPager  = (VerticalViewPager) view.findViewById(R.id.pager);
        verticalViewPager.setAdapter(screenSlidePagerAdapter);
        verticalViewPager.setCurrentItem(position);
        verticalViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
               // ((NewFeedNewsActivity) getActivity()).setmStoryId(feedNewsModelList.get(position).getStory_id());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        ((NewFeedNewsActivity) getActivity()).currentFragment("shortin");

    }



}
