package com.aroundpost.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.aroundpost.AroundPostApp;
import com.aroundpost.R;
import com.aroundpost.activity.FeedNewsActivity;
import com.aroundpost.activity.NewFeedNewsActivity;
import com.aroundpost.request.ReportUserRequest;
import com.aroundpost.response.GenericResponse;
import com.aroundpost.response.UserInterestResponse;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manish on 11/16/2016.
 */

public class ReportAStoryFragment extends Fragment implements View.OnClickListener {

    private Button mBtnReportSubmit, mBtnReportClose;
    private LinearLayout mThanksLinear, mReportLinear;
    private RadioButton mRadioAppropiate, mRadioAccurate, mRadioIncites, mRadioPropoganda;
    private RelativeLayout mRelInAppropiate, mRelAccurate, mRelIncites, mRelPropanganda;
    private String mType;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_story, container, false);
        ((NewFeedNewsActivity) getActivity()).setTopMargin(false);
        ((NewFeedNewsActivity)getActivity()).hideShowLayoutSliding(View.GONE);
        initView(view);
        firebaseAnalytics();
        clickListener();
        return view;
    }

    private void clickListener() {
        mBtnReportSubmit.setOnClickListener(this);
        mBtnReportClose.setOnClickListener(this);
        mRelInAppropiate.setOnClickListener(this);
        mRelAccurate.setOnClickListener(this);
        mRelIncites.setOnClickListener(this);
        mRelPropanganda.setOnClickListener(this);
    }

    public void initView(View view) {
        mBtnReportSubmit = (Button) view.findViewById(R.id.bt_remote_submit);
        mThanksLinear = (LinearLayout) view.findViewById(R.id.ll_thanks);
        mReportLinear = (LinearLayout) view.findViewById(R.id.ll_report_option);
        mBtnReportClose = (Button) view.findViewById(R.id.bt_remote_close);

        mRadioAppropiate = (RadioButton) view.findViewById(R.id.rd_inappropraite);
        mRadioAccurate = (RadioButton) view.findViewById(R.id.rd_inaccurate);
        mRadioIncites = (RadioButton) view.findViewById(R.id.rd_incites);
        mRadioPropoganda = (RadioButton) view.findViewById(R.id.rd_propaganda);

        mRelInAppropiate = (RelativeLayout) view.findViewById(R.id.rel_inappropiate);
        mRelAccurate = (RelativeLayout) view.findViewById(R.id.rel_inaccurate);
        mRelIncites = (RelativeLayout) view.findViewById(R.id.rel_incite);
        mRelPropanganda = (RelativeLayout) view.findViewById(R.id.rel_propoganda);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_remote_submit:
                if (mType != null && mType.length() > 0) {
                    String mStoryId = ((NewFeedNewsActivity) getActivity()).getmStoryId();
                    ReportUserRequest reportUserRequest = new ReportUserRequest();
                    reportUserRequest.setStory_id(mStoryId);
                    reportUserRequest.setReport_category_id(mType);
                    Call<GenericResponse> reportStoryApi = AroundPostApp.getAroundPostInterface(getActivity()).reportUserStory(reportUserRequest);
                    reportStoryApi.enqueue(new Callback<GenericResponse>() {
                        @Override
                        public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                            if (response.isSuccessful()) {
                                mReportLinear.setVisibility(View.GONE);
                                mThanksLinear.setVisibility(View.VISIBLE);
                            }else {
                                Toast.makeText(getActivity(), "Error Occured", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GenericResponse> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }else{
                    Toast.makeText(getActivity(), "Please select option.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.bt_remote_close:
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                break;
            case R.id.rel_inappropiate:
                mType = "1";
                setRadioButtonChecked(true, false, false, false);
                break;
            case R.id.rel_inaccurate:
                mType = "2";
                setRadioButtonChecked(false, true, false, false);
                break;
            case R.id.rel_incite:
                mType = "3";
                setRadioButtonChecked(false, false, true, false);
                break;
            case R.id.rel_propoganda:
                mType = "4";
                setRadioButtonChecked(false, false, false, true);
                break;
        }
    }

    private void setRadioButtonChecked(boolean inappropiate, boolean inAccurate, boolean incites, boolean propoganda) {
        mRadioAppropiate.setChecked(inappropiate);
        mRadioAccurate.setChecked(inAccurate);
        mRadioIncites.setChecked(incites);
        mRadioPropoganda.setChecked(propoganda);
    }

    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(getActivity()).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ReportAStoryFragment.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ReportAStoryFragment.class.getName());
        mFirebaseAnalytics.logEvent("FeedNewsDetailFragment", bundle);
    }
}
