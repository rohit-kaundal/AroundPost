package com.aroundpost.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.AroundPostApp;
import com.aroundpost.R;
import com.aroundpost.dialogs.TransparentDialog;
import com.aroundpost.request.SignUpRequest;
import com.aroundpost.response.SignUpResponse;
import com.aroundpost.utils.BasicUtils;
import com.aroundpost.utils.Constants;
import com.aroundpost.utils.JsonUtils;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.BuildConfig;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aroundpost.utils.Constants.nextPermissionsRequestCode;

/**
 * Created by Manish on 11/30/2016.
 */

public class FacebookAccountKitActivity extends BaseActivity {

    private Button btnLoginViaSMS;
    private RelativeLayout mainView;

    String both_lang = "";
    String notification = "";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String toastMessage = "";

    public static boolean isRegisterLoginTime = false;
    public interface OnCompleteListener {
        void onComplete();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_kit);
        isRegisterLoginTime = true;
        getPrefs();
        printKeyHash(this);

        //init accountkit
      AccountKit.initialize(getApplicationContext());
        // login via mobile
        //loginViaSMS(LoginType.PHONE);

        //login directly via signup data
        loginUsingFBAccountKit(null, null);

        btnLoginViaSMS = (Button) findViewById(R.id.login_btn);
        mainView = (RelativeLayout) findViewById(R.id.main_view);
    }

    //getting prefs for language by default is english i.e 2 and sound notification setting by default off
    private void getPrefs() {
        preferences = getSharedPreferences(AppPreference.AROUND_POST_PREFERENCE, Context.MODE_PRIVATE);
        editor = preferences.edit();
        both_lang = preferences.getString(AppPreference.language_type, "");
        if (preferences.getString(AppPreference.language_type, "").equals("")) {
            both_lang = "2";
        }
        if (preferences.getString(AppPreference.SOUND_ON_OFF, "").equals("")) {
            editor.putString(AppPreference.SOUND_ON_OFF, "0");
            editor.commit();
        }
    }

    private void loginViaSMS(LoginType loginType) {
        Log.d("Clicked ", "c");
        final Intent intent = new Intent(FacebookAccountKitActivity.this, AccountKitActivity.class);
        Log.d("Clicked ", "intent " + intent);
        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder
                = new AccountKitConfiguration.AccountKitConfigurationBuilder(
                loginType,
                AccountKitActivity.ResponseType.TOKEN);
        Log.d("Clicked ", "configurationBuilder " + configurationBuilder);
        final AccountKitConfiguration configuration = configurationBuilder.build();
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        OnCompleteListener completeListener = new OnCompleteListener() {
            @Override
            public void onComplete() {
                startActivityForResult(intent, Constants.ACTIVITY_FACEBOOK_ACCOUNT_KIT);
            }
        };


        if (configuration.isReceiveSMSEnabled()) {
            final OnCompleteListener receiveSMSCompleteListener = completeListener;
            completeListener = new OnCompleteListener() {
                @Override
                public void onComplete() {
                    requestPermissions(
                            Manifest.permission.RECEIVE_SMS,
                            R.string.permissions_receive_sms_title,
                            R.string.permissions_receive_sms_message,
                            receiveSMSCompleteListener);
                }
            };
        }
        if (configuration.isReadPhoneStateEnabled()) {
            final OnCompleteListener readPhoneStateCompleteListener = completeListener;
            completeListener = new OnCompleteListener() {
                @Override
                public void onComplete() {
                    requestPermissions(
                            Manifest.permission.READ_PHONE_STATE,
                            R.string.permissions_read_phone_state_title,
                            R.string.permissions_read_phone_state_message,
                            readPhoneStateCompleteListener);
                }
            };
        }
        completeListener.onComplete();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != 1) {
            Log.d("Error ", "Message Error");
            return;
        }
        // AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
        final String toastMessage;
        final AccountKitLoginResult loginResult = AccountKit.loginResultWithIntent(data);
        if (loginResult == null || loginResult.wasCancelled()) {
            toastMessage = "Login Cancelled";
            Log.d(toastMessage, toastMessage);
            finish();
        } else if (loginResult.getError() != null) {
            toastMessage = "Error";
            Log.d(toastMessage, toastMessage);
        } else {
            final AccessToken accessToken = loginResult.getAccessToken();

            if (accessToken != null) {
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(Account account) {
                        loginUsingFBAccountKit(account, accessToken);
                    }

                    @Override
                    public void onError(AccountKitError accountKitError) {
                        Log.e("", "accountKitError" + accountKitError.getUserFacingMessage());
                    }
                });
            } else {

            }
        }
    }


    private void loginUsingFBAccountKit(Account account, AccessToken accessToken) {
        PhoneNumber phoneNumber = new PhoneNumber("IN", "+919816498164", "IN");
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setCountry_code(phoneNumber.getCountryCode());
        signUpRequest.setMobile(phoneNumber.getPhoneNumber());
        signUpRequest.setDevice_id(BasicUtils.getBasicUtilInstance().getDeviceID(FacebookAccountKitActivity.this));
        signUpRequest.setCountry("91");
        signUpRequest.setFb_access_token(null);
        signUpRequest.setFacebook_id(null);
        signUpRequest.setDevice_type("Android");
        signUpRequest.setLang_type(both_lang);
        signUpRequest.setOne_signal_id(AppPreference.getAppPreference(FacebookAccountKitActivity.this).getOneSignalID());
        Log.d("signUpRequest ", new Gson().toJson(signUpRequest));
        final TransparentDialog transparentDialog = new TransparentDialog(FacebookAccountKitActivity.this, R.style.transparent_dialog);
        transparentDialog.show();
        Call<SignUpResponse> signUpApi = AroundPostApp.getAroundPostInterface(this).getSignUpApi(signUpRequest);
        signUpApi.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                transparentDialog.dismiss();
                Log.d("Response ", JsonUtils.toJson(response));
                if (response != null && response.body().getStatusCode() == 200) {
                    AppPreference.getAppPreference(FacebookAccountKitActivity.this).setSignUpDetails(response.body());
                    Log.e("********Token********","***************Register Token**************"+response.body().getData());

                    Intent intent = new Intent(FacebookAccountKitActivity.this, NewFeedNewsActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Snackbar.make(mainView, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                t.printStackTrace();
                transparentDialog.dismiss();
            }
        });
    }

    private void requestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final OnCompleteListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        checkRequestPermissions(
                permission,
                rationaleTitleResourceId,
                rationaleMessageResourceId,
                listener);
    }

    @TargetApi(23)
    private void checkRequestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final OnCompleteListener listener) {
        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        final int requestCode = nextPermissionsRequestCode++;
        permissionsListeners.put(requestCode, listener);

        if (shouldShowRequestPermissionRationale(permission)) {
            new AlertDialog.Builder(this)
                    .setTitle(rationaleTitleResourceId)
                    .setMessage(rationaleMessageResourceId)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            requestPermissions(new String[]{permission}, requestCode);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            // ignore and clean up the listener
                            permissionsListeners.remove(requestCode);
                            finish();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            requestPermissions(new String[]{permission}, requestCode);
        }
    }

    @TargetApi(23)
    @SuppressWarnings("unused")
    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           final @NonNull String permissions[],
                                           final @NonNull int[] grantResults) {
        final OnCompleteListener permissionsListener = permissionsListeners.remove(requestCode);
        if (permissionsListener != null
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionsListener.onComplete();
        } else {
            finish();
        }
    }

    private final Map<Integer, OnCompleteListener> permissionsListeners = new HashMap<>();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (android.content.pm.Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash===============", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }
}
