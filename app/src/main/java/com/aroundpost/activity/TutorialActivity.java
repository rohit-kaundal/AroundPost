package com.aroundpost.activity;

import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.AroundPostApp;
import com.aroundpost.R;
import com.aroundpost.adapter.TutorialAdapter;
import com.aroundpost.response.LocationResponse;
import com.aroundpost.utils.FusedLocation;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manish on 9/22/2016.
 */
public class TutorialActivity extends BaseActivity implements View.OnClickListener {

    private ViewPager mViewPager;
    private CircleIndicator mCircleIndicator;
    private TutorialAdapter mTutorialAdapter;
    private Button mBtnSkip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        initView();
        mTutorialAdapter = new TutorialAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(mTutorialAdapter);
        mCircleIndicator.setViewPager(mViewPager);
        mBtnSkip.setOnClickListener(this);


       /* FusedLocation fusedLocation = new FusedLocation(TutorialActivity.this,
                new FusedLocation.Callback(){
                    @Override
                    public void onLocationResult(Location location) {
                        Toast.makeText(TutorialActivity.this,
                                "Latitude " +location.getLatitude() +" Longitude: " + location.getLongitude(),
                                Toast.LENGTH_LONG).show();
                    }
                });

        if (!fusedLocation.isGPSEnabled()){
            fusedLocation.showSettingsAlert();
        }else{
            //use fusedLocation API calls here

        }*/
    }

    private void initView() {
        mViewPager = (ViewPager) findViewById(R.id.pager_introduction);
        mCircleIndicator = (CircleIndicator) findViewById(R.id.indicator);
        mBtnSkip = (Button) findViewById(R.id.btn_skip);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_skip:
                Intent intent = new Intent(TutorialActivity.this, FacebookAccountKitActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }


}
