/*
package com.aroundpost.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.aroundpost.AppPreference;
import com.aroundpost.AroundPostApp;
import com.aroundpost.R;
import com.aroundpost.adapter.CategoryInterestAdapter;
import com.aroundpost.adapter.PlacesInterestAdapter;
import com.aroundpost.dialogs.TransparentDialog;
import com.aroundpost.interfaces.OpenGroupInterface;
import com.aroundpost.response.SignUpResponse;
import com.aroundpost.response.UpdateUserInterestResponse;
import com.aroundpost.response.UserDetails;
import com.aroundpost.response.UserInterestCategory;
import com.aroundpost.response.UserInterestPlaces;
import com.aroundpost.response.UserInterestResponse;
import com.aroundpost.utils.BasicUtils;
import com.aroundpost.utils.JsonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

*/
/**
 * Created by Manish on 12/26/2016.
 *//*



public class FeedMainNewsActivity extends BaseActivity implements OpenGroupInterface, ExpandableListView.OnGroupExpandListener, ExpandableListView.OnGroupCollapseListener, View.OnClickListener {

    private ExpandableListView expPlacesInterest, expCategoryInterest;
    private int previousOpenGroup = -1, categoryPreviousOpenGroup = -1;
    private Button floatingActionButton;
    private HashMap<String, String> mListHashMap = new HashMap<>();
    private HashMap<String, String> mCategoryListHashMap = new HashMap<>();
    ArrayList<String> categoryIdList = new ArrayList<>();
    ArrayList<String> cityIdList = new ArrayList<>();
    private TextView tvNavHeader;
    private View view_;
    private ArrayList<String> cityChildList = new ArrayList<>();
    private ArrayList<String> categoryChildList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_feed_news);
        initUI();
        SignUpResponse signUpResponse = AppPreference.getAppPreference(this).getSignUpDetails();
        final UserDetails userDetails = signUpResponse.getData().getUserDetails();
        final TransparentDialog transparentDialog = new TransparentDialog(FeedMainNewsActivity.this, R.style.transparent_dialog);
        transparentDialog.show();
        Call<UserInterestResponse> userInterestResponseCall = AroundPostApp.getAroundPostInterface(this).getUserInterest();
        userInterestResponseCall.enqueue(new Callback<UserInterestResponse>() {
            @Override
            public void onResponse(Call<UserInterestResponse> call, Response<UserInterestResponse> response) {
                transparentDialog.dismiss();
                Log.d("Success", "success_____ " + JsonUtils.toJson(response));
                if (response != null && response.isSuccessful()) {
                    floatingActionButton.setVisibility(View.VISIBLE);
                    Log.d("Success", "success");
                    tvNavHeader.setVisibility(View.VISIBLE);
                    view_.setVisibility(View.VISIBLE);
                    List<UserInterestPlaces> listTitle = new ArrayList<>();
                    ArrayList<UserInterestPlaces> childTitleList = new ArrayList<>();
                    ArrayList<UserInterestPlaces> places = response.body().getData().getPlaces();

                    for (int i = 0; i < places.size(); i++) {
                        if (places.get(i).getParentId() != null && places.get(i).getParentId().length() > 0) {
                            childTitleList.add(places.get(i));
                            if (userDetails.getCityIds().contains(places.get(i).get_id())) {
                                cityChildList.add(places.get(i).get_id());
                            }
                        } else {
                            listTitle.add(places.get(i));
                            if (userDetails.getCityIds().contains(places.get(i).get_id())) {
                                cityIdList.add(places.get(i).get_id());
                            }
                        }
                    }
                    HashMap<UserInterestPlaces, List<UserInterestPlaces>> hashMapString = new HashMap<>();
                    for (int i = 0; i < listTitle.size(); i++) {
                        ArrayList<UserInterestPlaces> subChildList = new ArrayList<>();
                        for (int j = 0; j < childTitleList.size(); j++) {
                            if (listTitle.get(i).get_id().equalsIgnoreCase(childTitleList.get(j).getParentId())) {
                                subChildList.add(childTitleList.get(j));
                            }
                        }
                        hashMapString.put(listTitle.get(i), subChildList);
                    }
                    List<UserInterestCategory> categoryTitle = new ArrayList<>();
                    ArrayList<UserInterestCategory> childCategoryList = new ArrayList<>();
                    ArrayList<UserInterestCategory> categories = response.body().getData().getCategories();
                    for (int i = 0; i < categories.size(); i++) {
                        if (categories.get(i).getParentId() != null && categories.get(i).getParentId().length() > 0) {
                            childCategoryList.add(categories.get(i));
                            if (userDetails.getCategoryIds().contains(categories.get(i).get_id())) {
                                categoryChildList.add(categories.get(i).get_id());
                            }
                        } else {
                            categoryTitle.add(categories.get(i));
                            if (userDetails.getCategoryIds().contains(categories.get(i).get_id())) {
                                categoryIdList.add(categories.get(i).get_id());
                            }
                        }
                    }

                    HashMap<UserInterestCategory, List<UserInterestCategory>> hashMapStringC = new HashMap<>();
                    for (int i = 0; i < categoryTitle.size(); i++) {
                        ArrayList<UserInterestCategory> subChildList = new ArrayList<>();
                        for (int j = 0; j < childCategoryList.size(); j++) {
                            if (categoryTitle.get(i).get_id().equalsIgnoreCase(childCategoryList.get(j).getParentId())) {
                                subChildList.add(childCategoryList.get(j));
                            }
                        }
                        hashMapStringC.put(categoryTitle.get(i), subChildList);
                    }

                    PlacesInterestAdapter feedNewsTopicAdapter = new PlacesInterestAdapter(FeedMainNewsActivity.this, listTitle, hashMapString, expPlacesInterest, cityIdList, cityChildList, FeedMainNewsActivity.this);
                    expPlacesInterest.setAdapter(feedNewsTopicAdapter);
                    CategoryInterestAdapter categoryInterestAdapter = new CategoryInterestAdapter(FeedMainNewsActivity.this, categoryTitle, hashMapStringC, expCategoryInterest, categoryIdList, categoryChildList, FeedMainNewsActivity.this);
                    expCategoryInterest.setAdapter(categoryInterestAdapter);
                    getDynamicHeight();
                } else {
                    Log.d("Error, ", "error");
                    AppPreference.getAppPreference(FeedMainNewsActivity.this).setSignUpDetails(null);
                    Intent intent = new Intent(FeedMainNewsActivity.this, FacebookAccountKitActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<UserInterestResponse> call, Throwable t) {
                Log.d("Error ", "error");
                transparentDialog.dismiss();
                t.printStackTrace();
            }
        });

        expPlacesInterest.setOnGroupExpandListener(this);
        expPlacesInterest.setOnGroupCollapseListener(this);
        expCategoryInterest.setOnGroupExpandListener(this);
        expCategoryInterest.setOnGroupCollapseListener(this);
    }

    private void initUI() {
        expPlacesInterest = (ExpandableListView) findViewById(R.id.exp_places_intrest);
        expCategoryInterest = (ExpandableListView) findViewById(R.id.exp_category_intrest);
        floatingActionButton = (Button) findViewById(R.id.fab_button);
        tvNavHeader = (TextView) findViewById(R.id.nav_header);
        view_ = (View) findViewById(R.id.view_);

        floatingActionButton.setOnClickListener(this);
        Typeface tf = BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(this);
        tvNavHeader.setTypeface(tf);
        floatingActionButton.setTypeface(tf);
    }

    private void getDynamicHeight() {
        BasicUtils.getBasicUtilInstance().setListViewHeightBasedOnChildren(expPlacesInterest);
        BasicUtils.getBasicUtilInstance().setListViewHeightBasedOnChildren(expCategoryInterest);
    }

    @Override
    public void openInterfaceListener(int position, ExpandableListView expandableListView, HashMap<String, String> listHashMap, int type, boolean status) {
        Log.d("expPlacesInterest", expPlacesInterest + "");
        if (status) {
            if (type == 1) {
                if (previousOpenGroup != -1 && previousOpenGroup != position) {
                    expandableListView.collapseGroup(position);
                }
                boolean groupExpanded = expandableListView.isGroupExpanded(position);
                if (groupExpanded) {
                    expandableListView.collapseGroup(position);
                } else {
                    expandableListView.expandGroup(position);
                }
                previousOpenGroup = position;
                Log.d("list map ", listHashMap + "");
                mListHashMap.clear();
                mListHashMap.putAll(listHashMap);
            } else {
                if (categoryPreviousOpenGroup != -1) {
                    expandableListView.collapseGroup(position);
                }
                expandableListView.expandGroup(position);
                categoryPreviousOpenGroup = position;
                Log.d("list map ", listHashMap + "");
                mCategoryListHashMap.clear();
                mCategoryListHashMap.putAll(listHashMap);
            }
        } else {
            if (type == 1) {
                mListHashMap.clear();
                mListHashMap.putAll(listHashMap);
            } else {
                mCategoryListHashMap.clear();
                mCategoryListHashMap.putAll(listHashMap);
            }
        }
    }

    @Override
    public void onGroupCollapse(int groupPosition) {
        getDynamicHeight();
    }

    @Override
    public void onGroupExpand(int groupPosition) {
        getDynamicHeight();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_button:
                Iterator itr = mListHashMap.entrySet().iterator();
                HashMap<String, String> finalPlaceHashMap = new HashMap<>();
                int count = 0;
                while (itr.hasNext()) {
                    Map.Entry mapEntry = (Map.Entry) itr.next();
                    String value = (String) mapEntry.getValue();
                    finalPlaceHashMap.put(count + "", value);
                    count++;
                }

                Iterator itr1 = mCategoryListHashMap.entrySet().iterator();
                HashMap<String, String> finalCategoryHashMap = new HashMap<>();
                int count1 = 0;
                while (itr1.hasNext()) {
                    Map.Entry mapEntry = (Map.Entry) itr1.next();
                    String value = (String) mapEntry.getValue();
                    finalCategoryHashMap.put(count1 + "", value);
                    count1++;
                }
                String cityIds = JsonUtils.toJson(finalPlaceHashMap);
                String categoryIds = JsonUtils.toJson(finalCategoryHashMap);
                Log.d("Category Id ", categoryIds);
                Call<UpdateUserInterestResponse> updateInterestApi = AroundPostApp.getAroundPostInterface(this).updateUserInterest(cityIds, categoryIds);
                updateInterestApi.enqueue(new Callback<UpdateUserInterestResponse>() {
                    @Override
                    public void onResponse(Call<UpdateUserInterestResponse> call, Response<UpdateUserInterestResponse> response) {
                        Log.d("Response ", JsonUtils.toJson(response.body()));
                        AppPreference appPreference = AppPreference.getAppPreference(FeedMainNewsActivity.this);
                        SignUpResponse signUpResponse = appPreference.getSignUpDetails();
                        signUpResponse.getData().getUserDetails().setRegistered(true);
                        signUpResponse.getData().getUserDetails().setCategoryIds(response.body().getData().getCategoryIds());
                        signUpResponse.getData().getUserDetails().setCityIds(response.body().getData().getCityIds());
                        appPreference.setSignUpDetails(signUpResponse);
                        Intent intent = new Intent(FeedMainNewsActivity.this, FeedNewsActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<UpdateUserInterestResponse> call, Throwable t) {
                        Log.d("Error ", "");
                        t.printStackTrace();
                    }
                });
                break;
        }
    }
}
*/
