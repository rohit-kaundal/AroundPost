package com.aroundpost.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.aroundpost.R;

import java.util.Date;

public class SampleScreenShot extends AppCompatActivity {
   Button screenShot;
    private ImageView ivPreviousView, ivNextNews, ivNewsImage;
    private TextView tvNewsTiming, tvNewsPublisher, tvNewsHeader, tvNewsDescription;
    private RelativeLayout mainview;

    LinearLayout rel_news;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_screen_shot);
      //  ivNewsImage = (ImageView) findViewById(R.id.imv_news);
       // tvNewsTiming = (TextView)findViewById(R.id.tv_timing);
       // tvNewsPublisher = (TextView) findViewById(R.id.tv_editor);
        tvNewsHeader = (TextView) findViewById(R.id.tv_news_heading);
        /*
        tvNewsDescription = (TextView) findViewById(R.id.tv_news_description);*/
     /*   ivPreviousView = (ImageView) view.findViewById(R.id.iv_previous_news);
        ivNextNews = (ImageView) view.findViewById(R.id.iv_next_news);*/


        rel_news = (LinearLayout) findViewById(R.id.linearnews);
        //mainview = (RelativeLayout) findViewById(R.id.mainview);
     /*   RelativeLayout activity_sample_screen_shot = (RelativeLayout)findViewById(R.id.rel);
        screenShot = (Button) findViewById(R.id.screenShot);
        screenShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeScreenShot();
            }
        });*/
        tvNewsHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeScreenShot();
            }
        });


    }

    private void takeScreenShot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
//            View v1 = getActivity().getWindow().getDecorView().getRootView();
            View v1 = findViewById(R.id.linearnews);

            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);
            String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "title", null);
            Uri bmpUri = Uri.parse(pathofBmp);
            final Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
            emailIntent1.setType("image/png");
            startActivity(emailIntent1);

        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }
}
