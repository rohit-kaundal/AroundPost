package com.aroundpost.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.R;
import com.aroundpost.net.GetApisResponses;
import com.aroundpost.net.IHttpExceptionListener;
import com.aroundpost.net.IHttpResponseListener;
import com.aroundpost.response.OpenNotificationDetailsModel;
import com.aroundpost.utils.BasicUtils;
import com.aroundpost.utils.Constants;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class OpenNotificationStatusBar extends BaseActivity {
    private static final String TAG = "OpenNotificatStatusBar";
    Toolbar homeToolbar;
    String authToken = "";
    String story_id = "";
    String strResponse = "";
    String strErrorMessage = "";
    ArrayList<OpenNotificationDetailsModel> modelArrayList = new ArrayList<OpenNotificationDetailsModel>();
    private Context context = this;
    private ProgressBar progressBar;
    private ImageView ivPreviousView, ivNextNews, ivNewsImage;
    private TextView tvNewsTiming, tvNewsPublisher, tvNewsHeader, tvNewsDescription;
    SharedPreferences _prefs;
    SharedPreferences.Editor _editor;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 1: {
                    try {
                        JSONObject jsonObject = new JSONObject(strResponse);
                        String strmessage = jsonObject.getString("message");
                        String strstatusCode = jsonObject.getString("statusCode");

                        if (strstatusCode.equals("200")) {
                            OpenNotificationDetailsModel dataModel = new OpenNotificationDetailsModel();
                            if (!jsonObject.isNull("data")) {
                                JSONObject dataObject = jsonObject.getJSONObject("data");
                                dataModel.setImgUrl(dataObject.getString("imgUrl"));
                                dataModel.setRecordCount(dataObject.getString("recordCount"));
                                if (!dataObject.isNull("searchResult")) {
                                    JSONArray searchResultArray = dataObject.getJSONArray("searchResult");
                                    for (int i = 0; i < searchResultArray.length(); i++) {
                                        JSONObject searchResultObject = searchResultArray.getJSONObject(i);
                                        dataModel.setStory_id(searchResultObject.getString("story_id"));
                                        if (!searchResultObject.isNull("story")) {
                                            JSONObject storyObject = searchResultObject.getJSONObject("story");
                                            dataModel.setId(storyObject.getString("id"));
                                            dataModel.setStory_title(storyObject.getString("story_title"));
                                            dataModel.setStory_content(storyObject.getString("story_content"));
                                            dataModel.setStory_image(storyObject.getString("story_image"));
                                            dataModel.setThumb_img(storyObject.getString("thumb_img"));
                                            dataModel.setIcon_img(storyObject.getString("icon_img"));
                                            dataModel.setLongitude(storyObject.getString("longitude"));
                                            dataModel.setLatitude(storyObject.getString("latitude"));
                                            dataModel.setLanguage_id(storyObject.getString("language_id"));
                                            dataModel.setUser_id(storyObject.getString("user_id"));
                                            dataModel.setCreated(storyObject.getString("created"));

                                            if (!storyObject.isNull("user")) {
                                                JSONObject userObject = storyObject.getJSONObject("user");
                                                dataModel.setFirst_name(userObject.getString("first_name"));
                                                dataModel.setLast_name(userObject.getString("last_name"));
                                            }
                                            if (!storyObject.isNull("language")) {
                                                JSONObject languageObject = storyObject.getJSONObject("language");
                                                dataModel.setLanguage_name(languageObject.getString("language_name"));
                                            }
                                        }
                                    }
                                }

                                modelArrayList.add(dataModel);
                                Log.e(TAG, "****ArrayListSize***" + modelArrayList.size());

                            }


                        }

                        /*****
                         Setting Up the API Data on Widgets
                         ******/
                        setUpDataOnWidgets();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                }
                case 2: {
                    Toast.makeText(OpenNotificationStatusBar.this, "" + strErrorMessage, Toast.LENGTH_SHORT).show();
                    break;
                }

            }
        }

        private void setUpDataOnWidgets() {
            OpenNotificationDetailsModel model = modelArrayList.get(0);

            String strStoryImageUrl = model.getImgUrl() + model.getStory_image();
            /***StoryImage***/
            if (!strStoryImageUrl.startsWith("http://")) {
                strStoryImageUrl = "http://" + strStoryImageUrl;
            }
            Picasso.with(context).load(strStoryImageUrl)
                    .fit()
                    .into(ivNewsImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            Log.e(TAG, "*****ERROR****");
                        }
                    });
            /**Description**/
            tvNewsDescription.setText(model.getStory_content());
            /**TIMe***/
            tvNewsTiming.setText(BasicUtils.timeCalculation(model.getCreated()));
            /**Name***/
//            String strName = model.getFirst_name() + " " + model.getLast_name();
//            tvNewsPublisher.setText(strName);

            double doubleDistance = distance(Double.parseDouble(_prefs.getString(AppPreference.LATITUDE,"0")),
                    Double.parseDouble(_prefs.getString(AppPreference.LONGITUDE,"0")),
                    Double.parseDouble(model.getLatitude()),Double.parseDouble(model.getLongitude()));

            double roundDistance = round(doubleDistance,2);// // returns 200.35
            String strDistance = ""+roundDistance;
            tvNewsPublisher.setText(""+strDistance + " " + "km away");


            /**News Header**/
            tvNewsHeader.setText(model.getStory_title());

        }
    };
    IHttpResponseListener responseForJsonService = new IHttpResponseListener() {
        @Override
        public void handleResponse(String response) {
            Log.e(TAG, "******" + response);
            strResponse = response.toString();
            handler.sendEmptyMessage(1);
        }
    };
    IHttpExceptionListener exceptionForJsonService = new IHttpExceptionListener() {
        @Override
        public void handleException(String exception) {
            Log.e(TAG, "******" + exception);
            strErrorMessage = exception;
            handler.sendEmptyMessage(2);
        }
    };
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_notification_status_bar);
        _prefs = BasicUtils.getSharedPreferences(OpenNotificationStatusBar.this);
        initializedViews();
        firebaseAnalytics();
        getIntentData();
        /**Execute API**/
        executeApi();

    }

    private void getIntentData() {
        Intent intent = getIntent();
        authToken = intent.getStringExtra("auth_token");
        story_id = intent.getStringExtra("story_id");
    }

    private void initializedViews() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        homeToolbar = (Toolbar) findViewById(R.id.homeToolbar);
        ivNewsImage = (ImageView) findViewById(R.id.imv_news);
        tvNewsTiming = (TextView) findViewById(R.id.tv_timing);
        tvNewsPublisher = (TextView) findViewById(R.id.tv_editor);
        tvNewsHeader = (TextView) findViewById(R.id.tv_news_heading);
        tvNewsDescription = (TextView) findViewById(R.id.tv_news_description);
        ivPreviousView = (ImageView) findViewById(R.id.iv_previous_news);
        ivNextNews = (ImageView) findViewById(R.id.iv_next_news);

        setToolBar();
    }

    private void setToolBar() {
        setSupportActionBar(homeToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        homeToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void executeApi() {
        String apiUrl = Constants.GET_STORY + story_id;
        // String apiUrl = "http://aroundpost.com/api/Stories?story_id=17";
        //authToken = "737566647173357a6d6966676e7777";
        GetApisResponses gettigDataFromApi = new GetApisResponses(OpenNotificationStatusBar.this,
                responseForJsonService,
                "GET",
                apiUrl,
                exceptionForJsonService,
                authToken);
        gettigDataFromApi.execute(getJsonAPIsParam());

    }

    // Define @Param for the Get + Post APIs
    private JSONObject getJsonAPIsParam() {
        JSONObject jsonObject = new JSONObject();
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(this).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, OpenNotificationStatusBar.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, OpenNotificationStatusBar.class.getName());
        mFirebaseAnalytics.logEvent("OpenNotificationStatusBar", bundle);
    }


    public double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
