package com.aroundpost.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.AroundPostApp;
import com.aroundpost.R;
import com.aroundpost.fragment.FeedNewsDetailFragment;
import com.aroundpost.fragment.FeedNewsFragment;
import com.aroundpost.fragment.InShortLikeFragment;
import com.aroundpost.fragment.ReportAStoryFragment;
import com.aroundpost.fragment.SlidingFragment;
import com.aroundpost.request.SaveStoryRequest;
import com.aroundpost.response.GenericResponse;
import com.aroundpost.response.SearchResultNewsModel;
import com.aroundpost.services.FetchLocationService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manish on 9/22/2016.
 */
public class FeedNewsActivity extends BaseActivity implements  View.OnClickListener, SearchView.OnQueryTextListener {
    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private static final int EXTERNAL_PERMISSION_REQUEST = 2;
    private int mContainerID;
    private ArrayList<SearchResultNewsModel> mFeedNewsModelList = new ArrayList<>();
    private Toolbar mToolbar;
    private ImageView ivPreviousView, ivNextNews, ivShareNews;
    //private ActionBarDrawerToggle actionBarDrawerToggle;
    public String mStoryId;
    private BroadcastReceiver broadcastReceiver;
    private LinearLayout mBottomRelView;
    private ImageView ivSaveStory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_news);
        Log.d("current Activity", this.getClass().getName());
        initView();

        Intent fetchLocationService = new Intent(FeedNewsActivity.this, FetchLocationService.class);
        startService(fetchLocationService);

        mContainerID = R.id.container;
        Fragment fragment = new FeedNewsFragment();
        changeFragment(mContainerID, fragment, false, 0);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String type = intent.getStringExtra("type");
                if(type.equals("1")){
                    getLocationPermission();
                }else if(type.equals("2")){
                    Fragment fragment = new FeedNewsFragment();
                    changeFragment(mContainerID, fragment, true, 1);
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("location-broadcast"));
    }

    private void getLocationPermission() {
        if (ActivityCompat.checkSelfPermission(FeedNewsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(FeedNewsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(FeedNewsActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_PERMISSION_REQUEST);
        }
    }

    private void getExternalStoragePermission(){
        if (ActivityCompat.checkSelfPermission(FeedNewsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(FeedNewsActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    EXTERNAL_PERMISSION_REQUEST);
        }else{
            takeScreenShotAndShare();
        }
    }

    public String getmStoryId() {
        return mStoryId;
    }

    public void setmStoryId(String mStoryId) {
        this.mStoryId = mStoryId;
    }

    @SuppressLint("NewApi")
    private void initView() {
        mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            getSupportActionBar().setHomeAsUpIndicator(getDrawable(R.drawable.hamburger_icon));
        } else {
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.hamburger_icon));
        }

        ivSaveStory = (ImageView) findViewById(R.id.iv_save_news);
        ivPreviousView = (ImageView) findViewById(R.id.iv_previous_news);
        ivNextNews = (ImageView) findViewById(R.id.iv_next_news);
        ivShareNews = (ImageView) findViewById(R.id.iv_share_news);
        mBottomRelView = (LinearLayout) findViewById(R.id.re_navigation_bottom);

        /*ivPreviousView.setOnClickListener(this);
        ivNextNews.setOnClickListener(this);
        ivShareNews.setOnClickListener(this);
        ivSaveStory.setOnClickListener(this);*/

        mToolbar.setTitle("");
    }

    public void setTopMargin(boolean hide){
        FrameLayout fm = (FrameLayout) findViewById(R.id.container);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        if(hide){
            params.setMargins(0, (int) (48 * Resources.getSystem().getDisplayMetrics().density), 0, 0);
        }else{
            params.setMargins(0, 0, 0, 0);
        }
        fm.setLayoutParams(params);
    }

    public void setTitle(String message) {
        //Log.d("Message ", message);
        mToolbar.setTitle(message);
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
    }

    public ArrayList<SearchResultNewsModel> getmFeedNewsModelList() {
        return mFeedNewsModelList;
    }

    public void setmFeedNewsModelList(ArrayList<SearchResultNewsModel> mFeedNewsModelList) {
        this.mFeedNewsModelList = mFeedNewsModelList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null && fragment instanceof FeedNewsFragment) {
            getMenuInflater().inflate(R.menu.menu_home, menu);

            SearchManager searchManager = (SearchManager)
                    getSystemService(Context.SEARCH_SERVICE);
            MenuItem item = menu.findItem(R.id.action_search);
            Log.d("item ", item +"");
            SearchView searchView = (SearchView) item.getActionView();
            Log.d("Search ", searchView +"");

            EditText searchBox=((EditText) searchView.findViewById (android.support.v7.appcompat.R.id.search_src_text));
            searchBox.setTextColor(Color.parseColor("#ffffff"));

            ImageView searchButton = (ImageView) searchView.findViewById (android.support.v7.appcompat.R.id.search_button);
            searchButton.setColorFilter (Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
            ImageView searchClose = (ImageView) searchView.findViewById (android.support.v7.appcompat.R.id.search_close_btn);
            searchClose.setColorFilter (Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
            searchClose.setVisibility(View.GONE);
            searchView.setSearchableInfo(searchManager.
                    getSearchableInfo(getComponentName()));
            searchView.setSubmitButtonEnabled(true);
            searchView.setOnQueryTextListener(this);

        } else if (fragment != null && fragment instanceof InShortLikeFragment) {
            getMenuInflater().inflate(R.menu.menu_splash, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_report_story) {
            Fragment fragment = new ReportAStoryFragment();
            changeFragment(mContainerID, fragment, false, 0);
        } else if (id == android.R.id.home) {
            Intent intent = new Intent(FeedNewsActivity.this, FeedSelectedListActivity.class);
            startActivity(intent);
        }else if(id == R.id.action_nearby_stories){
            //check if location permission is given.
            getHeaderStoriesApi(1);
        }else if(id == R.id.action_unseen_stories){
            getHeaderStoriesApi(2);
        }else if(id == R.id.action_latest_stories){
            Fragment fragment = new FeedNewsFragment();
            changeFragment(mContainerID, fragment, false, 0);
        } else if(id == R.id.action_save_story){
            savedStorriesApi();
        }else if(id == R.id.action_my_stories){
            viewMyStories();
        }
        return super.onOptionsItemSelected(item);
    }

    private void viewMyStories() {
        Fragment fragment = new FeedNewsFragment();
        changeFragment(mContainerID, fragment, false, 4);
    }

    private void savedStorriesApi() {
        SaveStoryRequest saveStoryRequest = new SaveStoryRequest();
        saveStoryRequest.setStory_id(getmStoryId());
        Call<GenericResponse> savedStoriesCallback = AroundPostApp.getAroundPostInterface(this).getSavedStories(saveStoryRequest);
        savedStoriesCallback.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(FeedNewsActivity.this, "Story saved successfully", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {

            }
        });
    }

    private void getHeaderStoriesApi(int type) {
        String userLocation = AppPreference.getAppPreference(FeedNewsActivity.this).getUserLocation();
        if(userLocation != null && userLocation.length() > 0) {
            Fragment fragment = new FeedNewsFragment();
            changeFragment(mContainerID, fragment, true, type);
        }else{
            //check if location is enabled.
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if(isGpsEnabled){
                getLocationPermission();
            }else{
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof FeedNewsFragment) {
            moveTaskToBack(true);
            finish();
        } else if(fragment instanceof InShortLikeFragment){
            Fragment fragment1 = new FeedNewsFragment();
            changeFragment(mContainerID, fragment1, false, AppPreference.getAppPreference(this).getWhichApi());
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == LOCATION_PERMISSION_REQUEST){
            for(int i = 0; i < grantResults.length; i++){
                if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                    return;
                }
            }
            Intent fetchLocationService = new Intent(FeedNewsActivity.this, FetchLocationService.class);
            fetchLocationService.putExtra("doBroadcast", true);
            startService(fetchLocationService);
        }else if(requestCode == EXTERNAL_PERMISSION_REQUEST){
            for(int i = 0; i < grantResults.length; i++){
                if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                    return;
                }
            }
            takeScreenShotAndShare();
        }
    }

   /* @Override
    public SearchResultNewsModel getData(int position) {
        this.mPosition = position;
        return mFeedNewsModelList.get(position);
    }*/

    public int mPosition;

    public void hideShowLayout(int state, boolean type){
        mToolbar.setVisibility(state);
        mBottomRelView.setVisibility(state);
    }

    public void feedHideLayout(){
        mToolbar.setVisibility(View.VISIBLE);
        mBottomRelView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        Log.d("Mposition ", mPosition+"");
        switch (v.getId()) {
            case R.id.iv_previous_news:
                if (mPosition != 0) {
                    mPosition = mPosition - 1;
                    navigateToFeedNewsFragment();
                }
                break;
            case R.id.iv_next_news:
                if (mPosition != mFeedNewsModelList.size() - 1) {
                    mPosition = mPosition + 1;
                    navigateToFeedNewsFragment();
                }
                break;
            case R.id.iv_share_news:
                Toast.makeText(getApplicationContext(),"Clicked on share button...", Toast.LENGTH_LONG).show();
                //takeScreenShotAndShare();
               // getExternalStoragePermission();
                break;
            case R.id.iv_save_news:
                savedStorriesApi();
                break;
        }
    }

    private void takeScreenShotAndShare() {
//        Date now = new Date();
//        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
//
//        try {
//            // create bitmap screen capture
//            //View v1 = getWindow().getDecorView().getRootView();
//            View v1 = getWindow().getDecorView().getRootView();
//            v1.setDrawingCacheEnabled(true);
//            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
//            v1.setDrawingCacheEnabled(false);
//            String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap,"title", null);
//            Uri bmpUri = Uri.parse(pathofBmp);
//            final Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
//            emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
//            emailIntent1.setType("image/png");
//            emailIntent1.setType("text/plain");
//            emailIntent1.putExtra(android.content.Intent.EXTRA_SUBJECT, "Shared via Around Post");
//
//            emailIntent1.putExtra(android.content.Intent.EXTRA_TEXT, "http://aroundpost.com/dl");
//            emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
//
//            startActivity(emailIntent1);
//
//        } catch (Throwable e) {
//            // Several error may come out with file handling or OOM
//            e.printStackTrace();
//        }
    }

    private void navigateToFeedNewsFragment(){
      //  SearchResultNewsModel searchResultNewsModel = getData(mPosition);
        String imageURL = AppPreference.getAppPreference(this).getImageURL();
        Fragment fragment = new InShortLikeFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("listCount", mFeedNewsModelList.size());
        bundle.putInt("position", mPosition);
        bundle.putString("imageUrl", imageURL);
        //bundle.putString("title", mToolbar.getTitle());
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        this.query = query;
        Fragment fragment = new FeedNewsFragment();
        changeFragment(mContainerID, fragment, true, 3);
        Log.d("Query == ", query);
        return false;
    }

    public String query;

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}