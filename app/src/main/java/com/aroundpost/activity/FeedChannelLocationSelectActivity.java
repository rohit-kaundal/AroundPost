package com.aroundpost.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.R;
import com.aroundpost.adapter.ChannelNewsAdapter;
import com.aroundpost.adapter.LocationNewsAdapter;
import com.aroundpost.interfaces.OpenGroupInterface;
import com.aroundpost.net.GetApisResponses;
import com.aroundpost.net.IHttpExceptionListener;
import com.aroundpost.net.IHttpResponseListener;
import com.aroundpost.response.ChannelData;
import com.aroundpost.response.ChannelResponse;
import com.aroundpost.response.LocationData;
import com.aroundpost.response.LocationResponse;
import com.aroundpost.utils.BasicUtils;
import com.aroundpost.utils.Constants;
import com.facebook.accountkit.internal.Utility;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Manish on 3/25/2017.
 */

public class FeedChannelLocationSelectActivity extends BaseActivity implements ExpandableListView.OnGroupExpandListener, ExpandableListView.OnGroupCollapseListener, OpenGroupInterface, View.OnClickListener {

    public static final String TAG = "FeedChannelLocation";
    private ExpandableListView expPlacesInterest, expCategoryInterest;
    private Button floatingActionButton;
    private HashMap<String, String> mListHashMap = new HashMap<>();
    private HashMap<String, String> mCategoryListHashMap = new HashMap<>();
    ArrayList<String> categoryIdList = new ArrayList<>();
    ArrayList<String> cityIdList = new ArrayList<>();
    private TextView tvNavHeader;
    private View view_;
    private ArrayList<String> cityChildList = new ArrayList<>();
    private ArrayList<String> categoryChildList = new ArrayList<>();
    private FirebaseAnalytics mFirebaseAnalytics;
    SharedPreferences _prefs;
    SharedPreferences.Editor _editor;
    public String ok = null;
    public String channel = null;
    public String strResponseLocation, strExceptionLocation;
    public String strResponseChannels, strExceptionChannels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_feed_news);
        _prefs = BasicUtils.getSharedPreferences(FeedChannelLocationSelectActivity.this);
        _editor = _prefs.edit();
        initUI();
        firebaseAnalytics();
        LocationResponse locationResponse = AppPreference.getAppPreference(this).getLocationData();
        ChannelResponse channelResponse = AppPreference.getAppPreference(this).getChannelData();
        ArrayList<LocationData> places = null;

        places = locationResponse.getData();


        HashMap<LocationData, List<LocationData>> hashMapString = new HashMap<>();
        String id = AppPreference.getAppPreference(this).getLocationNewsPreference();
        if (id.contains(",")) {
            String[] splitId = id.split(",");
            for (int i = 0; i < splitId.length; i++) {
                cityIdList.add(splitId[i]);
            }

        } else {
            cityIdList.add(id);
        }
        try {
            for (int i = 0; i < places.size(); i++) {
                Log.d("Location Name ", places.get(i).getLocation_name());
                //cityIdList.add(places.get(i).getId());
                ArrayList<LocationData> subChildList = new ArrayList<>();
                hashMapString.put(places.get(i), subChildList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<ChannelData> channels = channelResponse.getData();
        HashMap<ChannelData, List<ChannelData>> hashMapChannelString = new HashMap<>();

        String channelId = AppPreference.getAppPreference(this).getChannelNewsPreference();
        if (channelId.contains(",")) {
            String[] splitId = channelId.split(",");
            for (int i = 0; i < splitId.length; i++) {
                categoryIdList.add(splitId[i]);
            }

        } else {
            categoryIdList.add(channelId);
        }

        for (int i = 0; i < channels.size(); i++) {
            Log.d("Channel Name ", channels.get(i).getChannel_name());

            //categoryIdList.add(channels.get(i).getId());
            ArrayList<ChannelData> subChildList = new ArrayList<>();
            hashMapChannelString.put(channels.get(i), subChildList);
        }

        LocationNewsAdapter locationNewsAdapter = new LocationNewsAdapter(FeedChannelLocationSelectActivity.this, places, hashMapString, expPlacesInterest, cityIdList, cityChildList, FeedChannelLocationSelectActivity.this);
        expPlacesInterest.setAdapter(locationNewsAdapter);
        ChannelNewsAdapter feedNewsTopicAdapter = new ChannelNewsAdapter(FeedChannelLocationSelectActivity.this, channels, hashMapChannelString, expCategoryInterest, categoryIdList, categoryChildList, FeedChannelLocationSelectActivity.this);
        expCategoryInterest.setAdapter(feedNewsTopicAdapter);
        getDynamicHeight();

        expPlacesInterest.setOnGroupExpandListener(this);
        expPlacesInterest.setOnGroupCollapseListener(this);
        expCategoryInterest.setOnGroupExpandListener(this);
        expCategoryInterest.setOnGroupCollapseListener(this);
    }

    private void getDynamicHeight() {
        BasicUtils.getBasicUtilInstance().setListViewHeightBasedOnChildren(expPlacesInterest);
        BasicUtils.getBasicUtilInstance().setListViewHeightBasedOnChildren(expCategoryInterest);
    }

    private void initUI() {
        expPlacesInterest = (ExpandableListView) findViewById(R.id.exp_places_intrest);
        expCategoryInterest = (ExpandableListView) findViewById(R.id.exp_category_intrest);
        floatingActionButton = (Button) findViewById(R.id.fab_button);
        tvNavHeader = (TextView) findViewById(R.id.nav_header);
        view_ = (View) findViewById(R.id.view_);

        floatingActionButton.setOnClickListener(this);
        Typeface tf = BasicUtils.getBasicUtilInstance().setAvirLTStdLignt(this);
        tvNavHeader.setTypeface(tf);
        floatingActionButton.setTypeface(tf);
    }

    @Override
    public void onGroupCollapse(int groupPosition) {
        Log.e("group position", "groupPosition" + groupPosition);
        getDynamicHeight();
    }

    @Override
    public void onGroupExpand(int groupPosition) {
        getDynamicHeight();
    }

    @Override
    public void openInterfaceListener(int position, ExpandableListView expandableListView, HashMap<String, String> listHashMap, int type, boolean status) {
        if (status) {
            if (type == 1) {
                Log.d("list map ", listHashMap + "");
                mListHashMap.clear();
                mListHashMap.putAll(listHashMap);
            } else {
                Log.d("list map ", listHashMap + "");
                mCategoryListHashMap.clear();
                mCategoryListHashMap.putAll(listHashMap);
            }
        } else {
            if (type == 1) {
                mListHashMap.clear();
                mListHashMap.putAll(listHashMap);
            } else {
                mCategoryListHashMap.clear();
                mCategoryListHashMap.putAll(listHashMap);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_button:
                Iterator itr = mListHashMap.entrySet().iterator();
                Log.d("MhashMap ", mListHashMap.toString() + "");

                while (itr.hasNext()) {
                    Map.Entry mapEntry = (Map.Entry) itr.next();
                    String value = (String) mapEntry.getValue();
                    if (ok == null) {
                        ok = value;
                    } else {
                        ok = ok + "," + value;
                    }
                    Log.d("Ok -- ", ok);
                }

                AppPreference.getAppPreference(FeedChannelLocationSelectActivity.this).setLocationNewsPreference(ok);

                getExecuteLocationAPI();

                Iterator itr1 = mCategoryListHashMap.entrySet().iterator();
                HashMap<String, String> finalCategoryHashMap = new HashMap<>();
                int count1 = 0;
                while (itr1.hasNext()) {
                    Map.Entry mapEntry = (Map.Entry) itr1.next();
                    String value = (String) mapEntry.getValue();

                    finalCategoryHashMap.put(count1 + "", value);
                    if (channel == null) {
                        channel = value;
                    } else {
                        channel = channel + "," + value;
                    }
                }
                getExecuteChannelsAPI();

                AppPreference.getAppPreference(FeedChannelLocationSelectActivity.this).setChannelNewsPreference(channel);
                Intent intent = new Intent(FeedChannelLocationSelectActivity.this, NewFeedNewsActivity.class);
                startActivity(intent);
                finish();
        }
    }

    private void getExecuteLocationAPI() {

        String strToken = _prefs.getString(AppPreference.USER_TOKEN_KEY, "");

        // Call the Api:
        GetApisResponses apiResponseAsync = new GetApisResponses(FeedChannelLocationSelectActivity.this,
                iHttpResponseListenerLocation,
                "POST",
                Constants.LOCATION_UPDATES,
                iHttpExceptionListenerLocation,
                strToken);
        apiResponseAsync.execute(getLocationRequestParams());
    }

    // Define @Param for the Get + Post APIs
    private JSONObject getLocationRequestParams() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("locations", ok);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    IHttpResponseListener iHttpResponseListenerLocation = new IHttpResponseListener() {
        @Override
        public void handleResponse(String response) {
            Log.e(TAG, "*************Reponse************" + response);
            strResponseLocation = response;
            handler.sendEmptyMessage(1);
        }
    };
    IHttpExceptionListener iHttpExceptionListenerLocation = new IHttpExceptionListener() {
        @Override
        public void handleException(String exception) {
            Log.e(TAG, "*************exception************" + exception);
            strExceptionLocation = exception;
            handler.sendEmptyMessage(2);
        }
    };


    private void getExecuteChannelsAPI() {

        String strToken = _prefs.getString(AppPreference.USER_TOKEN_KEY, "");

        // Call the Api:
        GetApisResponses apiResponseAsync = new GetApisResponses(FeedChannelLocationSelectActivity.this,
                iHttpResponseListenerChannels,
                "POST",
                Constants.CHANNELS_UPDATES,
                iHttpExceptionListenerChannels,
                strToken);
        apiResponseAsync.execute(getChannelsRequestParams());
    }

    // Define @Param for the Get + Post APIs
    private JSONObject getChannelsRequestParams() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("channels", channel);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    IHttpResponseListener iHttpResponseListenerChannels = new IHttpResponseListener() {
        @Override
        public void handleResponse(String response) {
            Log.e(TAG, "*************Reponse************" + response);
            strResponseChannels = response;
            handler.sendEmptyMessage(3);
        }
    };
    IHttpExceptionListener iHttpExceptionListenerChannels = new IHttpExceptionListener() {
        @Override
        public void handleException(String exception) {
            Log.e(TAG, "*************exception************" + exception);
            strExceptionChannels = exception;
            handler.sendEmptyMessage(4);
        }
    };


    // Define the Handler Thread....to fetch the Response:::
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1: {
                    // Show the Data in widgets those data can get from the Apis
                    try {
                        JSONObject jsonObject = new JSONObject(strResponseLocation);
                        String message = jsonObject.getString("message");
                        String statusCode = jsonObject.getString("statusCode");
                        if (message.equals("Success") && statusCode.equals("200")) {
                            Log.e(TAG,"*********Location Update Successful*********");
                            Toast.makeText(FeedChannelLocationSelectActivity.this, "Locations updated DONE!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 2: {
                    Toast.makeText(FeedChannelLocationSelectActivity.this, "Server not respond.", Toast.LENGTH_SHORT).show();
                    break;
                }
                case 3: {
                    try {
                        JSONObject jsonObject = new JSONObject(strResponseChannels);
                        String message = jsonObject.getString("message");
                        String statusCode = jsonObject.getString("statusCode");
                        if (message.equals("Success") && statusCode.equals("200")) {
                            Log.e(TAG,"*************Channels Update Sucessfull***************");
                            Toast.makeText(FeedChannelLocationSelectActivity.this, "Channels updated DONE!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }

                case 4: {
                    Toast.makeText(FeedChannelLocationSelectActivity.this, "Server not respond.", Toast.LENGTH_SHORT).show();
                    break;
                }

            }
        }
    };


    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(this).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, FeedChannelLocationSelectActivity.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, FeedChannelLocationSelectActivity.class.getName());
        mFirebaseAnalytics.logEvent("SettingActivity", bundle);
    }
}
