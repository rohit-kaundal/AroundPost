package com.aroundpost.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.aroundpost.R;
import com.aroundpost.dialogs.NoInternetConnectionDialog;
import com.aroundpost.interfaces.NoInternetInterface;
import com.aroundpost.utils.BasicUtils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SourceLinkOpen extends BaseActivity implements NoInternetInterface {
    WebView sourceWeb;
    Toolbar toolbar_source;
    private String mCM;
    private ValueCallback<Uri> mUM;
    private ValueCallback<Uri[]> mUMA;
    private final static int FCR = 1;
    boolean isConnected;
    private String source_url="";
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_source_link_open);
        isConnected = BasicUtils.getBasicUtilInstance().checkInternetOn(this);
        firebaseAnalytics();
        if (isConnected) {
            getIntentData();
            initializedViews();
        } else {
            isConnected = false;
            showDialogBox(isConnected);
        }


    }

    private void getIntentData() {
        Intent intent = getIntent();
        source_url = intent.getStringExtra("source_url");
    }

    private void initializedViews() {
        sourceWeb = (WebView) findViewById(R.id.sourceWeb);
        toolbar_source = (Toolbar) findViewById(R.id.toolbar_source);
        setToolBar();
        showDataInWebview();
    }


    private void setToolBar() {
        setSupportActionBar(toolbar_source);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar_source.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void showDataInWebview() {
        assert sourceWeb != null;
        WebSettings webSettings = sourceWeb.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSettings.setBuiltInZoomControls(true);

        methodInvoke(webSettings, "setPluginsEnabled", new Class[]{boolean.class}, new Object[]{true});
        methodInvoke(webSettings, "setPluginState", new Class[]{WebSettings.PluginState.class}, new Object[]{WebSettings.PluginState.ON});
        methodInvoke(webSettings, "setPluginsEnabled", new Class[]{boolean.class}, new Object[]{true});
        methodInvoke(webSettings, "setAllowUniversalAccessFromFileURLs", new Class[]{boolean.class}, new Object[]{true});
        methodInvoke(webSettings, "setAllowFileAccessFromFileURLs", new Class[]{boolean.class}, new Object[]{true});

        sourceWeb.clearHistory();
        sourceWeb.clearFormData();
        sourceWeb.clearCache(true);


        if (Build.VERSION.SDK_INT >= 21) {
            webSettings.setMixedContentMode(0);
            sourceWeb.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else if (Build.VERSION.SDK_INT >= 19) {
            sourceWeb.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT < 19) {
            sourceWeb.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        //if link starts with http and contains  .pdf in end and starts with http only
        if(source_url.contains("http://") && source_url.contains(".pdf")) {

            sourceWeb.setWebViewClient(new Callback());

            sourceWeb.loadUrl( "http://drive.google.com/viewerng/viewer?embedded=true&url="+source_url);
        }
        else
        {
            sourceWeb.setWebViewClient(new Callback());

            sourceWeb.loadUrl(source_url);
        }

        {

        }
        // url contains https
      if(source_url.contains("https://"))
        {
            sourceWeb.setWebViewClient(new SSLTolerentWebViewClient());
            sourceWeb.loadUrl(source_url);
        }
       //sourceWeb.loadUrl("https://joomcar.net/k2-filter-search-module-demo-1-standard-view");


        sourceWeb.setWebChromeClient(new WebChromeClient() {
            //For Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUM = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                SourceLinkOpen.this.startActivityForResult(Intent.createChooser(i, "File Chooser"), FCR);
            }

            // For Android 3.0+, above method not supported in some android 3+ versions, in such case we use this
            public void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                mUM = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                SourceLinkOpen.this.startActivityForResult(
                        Intent.createChooser(i, "File Browser"),
                        FCR);
            }

            //For Android 4.1+
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUM = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                SourceLinkOpen.this.startActivityForResult(Intent.createChooser(i, "File Chooser"), SourceLinkOpen.FCR);
            }

            //For Android 5.0+
            public boolean onShowFileChooser(
                    WebView webView, ValueCallback<Uri[]> filePathCallback,
                    WebChromeClient.FileChooserParams fileChooserParams) {
                if (mUMA != null) {
                    mUMA.onReceiveValue(null);
                }
                mUMA = filePathCallback;
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(SourceLinkOpen.this.getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        takePictureIntent.putExtra("PhotoPath", mCM);
                    } catch (IOException ex) {
                        //Log.e(TAG, "Image file creation failed", ex);
                    }
                    if (photoFile != null) {
                        mCM = "file:" + photoFile.getAbsolutePath();
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    } else {
                        takePictureIntent = null;
                    }
                }
                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                contentSelectionIntent.setType("image/*");
                Intent[] intentArray;
                if (takePictureIntent != null) {
                    intentArray = new Intent[]{takePictureIntent};
                } else {
                    intentArray = new Intent[0];
                }

                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooserIntent, FCR);
                return true;
            }
        });

    }

    @Override
    public void noInternetConnectionListner() {
        finish();

    }

    private class SSLTolerentWebViewClient extends WebViewClient {
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {

            handler.proceed(); // Ignore SSL certificate errors

            AlertDialog.Builder builder = new AlertDialog.Builder(SourceLinkOpen.this);
            AlertDialog alertDialog = builder.create();
            String message = "SSL Certificate error.";
            switch (error.getPrimaryError()) {
                case SslError.SSL_UNTRUSTED:
                    message = "The certificate authority is not trusted.";
                    break;
                case SslError.SSL_EXPIRED:
                    message = "The certificate has expired.";
                    break;
                case SslError.SSL_IDMISMATCH:
                    message = "The certificate Hostname mismatch.";
                    break;
                case SslError.SSL_NOTYETVALID:
                    message = "The certificate is not yet valid.";
                    break;
            }

            message += " Do you want to continue anyway?";
            alertDialog.setTitle("SSL Certificate Error");
            alertDialog.setMessage(message);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Ignore SSL certificate errors
                    handler.proceed();
                }
            });

            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    handler.cancel();
                }
            });
            alertDialog.show();
        }
    }

    // Create an image file
    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "img_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (sourceWeb.canGoBack()) {
                        sourceWeb.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private final static Object methodInvoke(Object obj, String method, Class<?>[] parameterTypes, Object[] args) {
        try {
            Method m = obj.getClass().getMethod(method, new Class[]{boolean.class});
            m.invoke(obj, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    private void showDialogBox(boolean isConnected) {
        if (!isConnected) {
           /* AlertDialog alertDialog = new AlertDialog.Builder(
                    SourceLinkOpen.this).create();

            // Setting Dialog Title
            alertDialog.setTitle("Alert");

            // Setting Dialog Message
            alertDialog.setMessage("Check Your Internet Connection!");

            // Setting OK Button
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to execute after dialog closed
                    Intent startMain = new Intent(Intent.ACTION_MAIN);
                    startMain.addCategory(Intent.CATEGORY_HOME);
                    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startMain);
                }
            });

            // Showing Alert Message
            alertDialog.show();*/
            NoInternetConnectionDialog dialog = new NoInternetConnectionDialog(this, this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        }

    }
       public class Callback extends WebViewClient {
        ProgressDialog progressDialog;

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            //Toast.makeText(getApplicationContext(), "Failed loading app!", Toast.LENGTH_SHORT).show();
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            /*progressDialog = new ProgressDialog(SourceLinkOpen.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();*/
            return true;
        }

          /* @Override
           public void onPageStarted(WebView view, String url, Bitmap favicon) {
               super.onPageStarted(view, url, favicon);
               progressDialog = new ProgressDialog(SourceLinkOpen.this);
               progressDialog.setMessage("Loading...");
               progressDialog.show();
           }*/

           //Show loader on url load
        public void onLoadResource(WebView view, String url) {
            if (progressDialog == null) {
                // in standard case YourActivity.this
                progressDialog = new ProgressDialog(SourceLinkOpen.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();
            }

        }

        public void onPageFinished(WebView view, String url) {
            try {
               // progressDialog.dismiss();
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                   progressDialog = null;
                }
               /* if(progressDialog!=null){
                    progressDialog.dismiss();
                }*/
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }
    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(this).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, SourceLinkOpen.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SourceLinkOpen.class.getName());
        mFirebaseAnalytics.logEvent("SourceLinkOpen", bundle);
    }
}
