package com.aroundpost.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.aroundpost.AppPreference;
import com.aroundpost.R;
import com.aroundpost.dialogs.TransparentDialog;
import com.aroundpost.interfaces.LocationUserInterface;
import com.aroundpost.interfaces.NoInternetInterface;
import com.aroundpost.response.SignUpResponse;
import com.aroundpost.services.AroundPostWrapper;
import com.aroundpost.utils.BasicUtils;

public class SplashActivity extends BaseActivity implements NoInternetInterface, LocationUserInterface {

    private TransparentDialog mTransparentDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mTransparentDialog = new TransparentDialog(this, R.style.transparent_dialog);
        //api to get locations for user.
        boolean checkInternetOn = BasicUtils.getBasicUtilInstance().checkInternetOn(this);
        if (!checkInternetOn) {
            Intent intent = new Intent(SplashActivity.this, NewFeedNewsActivity.class);
            startActivity(intent);
        } else {
            mTransparentDialog.show();
            AroundPostWrapper.getAroundPostWrapper(this).getAroundPostServices().getUserLocation(this, this);
            AroundPostWrapper.getAroundPostWrapper(this).getAroundPostServices().getUserChannel(this);
        }
    }

    @Override
    public void noInternetConnectionListner() {
        finish();
    }

    @Override
    public void userLocationSuccessListener() {
        mTransparentDialog.dismiss();
        SignUpResponse signUpResponse = AppPreference.getAppPreference(SplashActivity.this).getSignUpDetails();
        if (signUpResponse != null) {
            String accessToken = signUpResponse.getData().getApi_key();
            Log.e("Acess Token+++++", "#######+" + accessToken);
            if (accessToken != null && accessToken.length() > 0) {

                Intent intent = new Intent(SplashActivity.this, NewFeedNewsActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(SplashActivity.this, FacebookAccountKitActivity.class);
                startActivity(intent);
                finish();
            }
        } else {
            boolean isAppStartedFirst = AppPreference.getAppPreference(SplashActivity.this).isAppStartedFirstTime();
            Log.d("isAppStartedFirst", "" + isAppStartedFirst);
            if (isAppStartedFirst) {
                AppPreference.getAppPreference(SplashActivity.this).setLocationNewsPreference("1");
                AppPreference.getAppPreference(SplashActivity.this).setPreferedCategory("L" + 1);
                AppPreference.getAppPreference(SplashActivity.this).setIsAppStartedAgain(false);
                Intent intent = new Intent(SplashActivity.this, TutorialActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(SplashActivity.this, FacebookAccountKitActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void userLocationFaliourListener() {
        mTransparentDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
