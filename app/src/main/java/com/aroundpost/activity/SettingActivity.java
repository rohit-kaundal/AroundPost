package com.aroundpost.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.AroundPostApp;
import com.aroundpost.R;
import com.aroundpost.net.GetApisResponses;
import com.aroundpost.net.IHttpExceptionListener;
import com.aroundpost.net.IHttpResponseListener;
import com.aroundpost.response.SignUpResponse;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.Locale;

public class SettingActivity extends AppCompatActivity {
    private static final int EXTERNAL_PERMISSION_REQUEST = 2;
    CheckBox checkEnglish, checkHindi;
    Button btnSave;
    TextView tv_rate;
    int pos;
    int pos1;
    Locale myLocale;
    Toolbar toolbar;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    EditText editFirstName, editLastName, editEmail;
    String strResponse = "";
    String strErrorMessage = "";
    String strResponseLastUpdateInfo = "";
    String strErrorMessageLastUpdateInfo = "";
    ImageView imgRate, imgInvite;
    private Context context = this;

    private Switch switchPushNotifications, switchSound;
    private LinearLayout layRate, invitelay;
    private String first_name = "";
    private String last_name = "";
    private String email = "";
    private String notification = "";
    private String strSound = "";
    private String strLang = "";
    private String authToken = "";
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        setWidgetIDs();
        getPrefs();
        firebaseAnalytics();
        setAlreadySelctedNotiMethod();
        setAlreadySelectedSound();
        setAlreadySelctedLanguage();



        showData();

        setChangeListner();

        gettingLastUpdatedUserProfileData();

    }

    //method to show the data of user from prefernces.
    private void showData() {

        gettingLastUpdatedUserProfileData();
        /*if (!preferences.getString(AppPreference.setting_User_FirstName, "").equals("")) {
            first_name = preferences.getString(AppPreference.setting_User_FirstName, "");
            editFirstName.setText(first_name);
        }
        if (!preferences.getString(AppPreference.setting_User_Email, "").equals("")) {
            email = preferences.getString(AppPreference.setting_User_Email, "");
            editEmail.setText(email);
        }

        if (!preferences.getString(AppPreference.setting_User_LastName, "").equals("")) {
            last_name = preferences.getString(AppPreference.setting_User_LastName, "");
            editLastName.setText(last_name);
        }*/
    }


    // to select sound
    private void setAlreadySelectedSound() {
        if (!preferences.getString(AppPreference.SOUND_ON_OFF, "").equals("")) {
            strSound = preferences.getString(AppPreference.SOUND_ON_OFF, "");
            if (strSound.equals("1")) {
                switchSound.setChecked(true);
            } else if (strSound.equals("0")) {
                switchSound.setChecked(false);
            }
        } else {
            switchSound.setChecked(true);
        }
    }

    // method to select particular lang from checkbox
    private void setAlreadySelctedLanguage() {
        if (!preferences.getString(AppPreference.language_type, "").equals("")) {
            strLang = preferences.getString(AppPreference.language_type, "");
            if (strLang.equals("2")) {
                checkEnglish.setChecked(true);
            } else if (strLang.equals("1")) {
                checkHindi.setChecked(true);
            } else {
                checkEnglish.setChecked(true);
                checkHindi.setChecked(true);
            }
        } else {
            checkEnglish.setChecked(true);
        }
    }

    // method to turn on off notifications.
    private void setAlreadySelctedNotiMethod() {
        if (!preferences.getString(AppPreference.NOTIFICATION_ON_OFF, "").equals("")) {
            notification = preferences.getString(AppPreference.NOTIFICATION_ON_OFF, "");
            if (notification.equals("1")) {
                switchPushNotifications.setChecked(true);
            } else if (notification.equals("0")) {
                switchPushNotifications.setChecked(false);
            }
        } else {
            switchPushNotifications.setChecked(true);
        }


    }

    private void getPrefs() {
        try {
            // this to get auth token to run api
            SignUpResponse signUpDetails = AppPreference.getAppPreference(AroundPostApp.getContext()).getSignUpDetails();
            if (signUpDetails != null) {
                authToken = signUpDetails.getData().getApi_key();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        preferences = getSharedPreferences(AppPreference.AROUND_POST_PREFERENCE, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    private void setWidgetIDs() {
        switchPushNotifications = (Switch) findViewById(R.id.switchPushNotifications);
        switchSound = (Switch) findViewById(R.id.switchSound);
        switchPushNotifications.setChecked(true);
       /* tv_rate = (TextView) findViewById(R.id.textrate);

        tv_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.aroundpost.com/rate")));
            }
        });*/
        checkEnglish = (CheckBox) findViewById(R.id.checkEnglish);
        checkHindi = (CheckBox) findViewById(R.id.checkHindi);

        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editEmail = (EditText) findViewById(R.id.editEmail);

        btnSave = (Button) findViewById(R.id.btnSave);

        toolbar = (Toolbar) findViewById(R.id.toolbar_set);
        setToolBar();

        imgInvite = (ImageView) findViewById(R.id.imgInvite);
        imgRate = (ImageView) findViewById(R.id.imgRate);

        layRate = (LinearLayout) findViewById(R.id.layRate);
        invitelay = (LinearLayout) findViewById(R.id.invitelay);

    }

    private void setToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setChangeListner() {

        switchPushNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    editor.putString(AppPreference.NOTIFICATION_ON_OFF, "1");
                    editor.commit();
                    OneSignal.setSubscription(true);
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.notion),
                            Toast.LENGTH_SHORT).show();
                } else {
                    editor.putString(AppPreference.NOTIFICATION_ON_OFF, "0");
                    editor.commit();
                    OneSignal.setSubscription(false);
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.notioff),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        switchSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    // OneSignal.enableSound(true);

                    editor.putString(AppPreference.SOUND_ON_OFF, "1");
                    editor.commit();
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.soundon),
                            Toast.LENGTH_SHORT).show();

                } else {
                    //  OneSignal.enableSound(false);
                    editor.putString(AppPreference.SOUND_ON_OFF, "0");
                    editor.commit();
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.soundoff),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


// checkboxes to select particular language
        checkEnglish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strLang = "2";
                }
            }
        });

        checkHindi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strLang = "1";
                } else {

                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkEnglish.isChecked() && !checkHindi.isChecked()) {
                    showAlertDialog("Please Select one language at least.");
                } else {
                    executeSaveUserInfoAPI();
                }
            }
        });

// to rate app on playstore
        imgRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.aroundpost.com/rate")));
            }
        });
// to invite friend by sharing link
        imgInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getExternalStoragePermission();
            }
        });

        invitelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getExternalStoragePermission();
            }
        });

        layRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }*/
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.aroundpost.com/rate")));

            }
        });
    }

    private void getExternalStoragePermission() {
        if (ActivityCompat.checkSelfPermission(SettingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SettingActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    EXTERNAL_PERMISSION_REQUEST);
        } else {
            inviteFriends();
        }
    }

    private void inviteFriends() {
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        //  shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Try (your game) for Android!");
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, download AroundPost for all the latest news in Himachal! Click here: aroundpost.com/dl");

        Intent chooserIntent = Intent.createChooser(shareIntent, "Share with Your Friends");
        chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(chooserIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    // getting data for posting in api.
    private void getData() {
        first_name = editFirstName.getText().toString().trim();
        editor.putString(AppPreference.setting_User_FirstName, first_name);
        editor.commit();

        last_name = editLastName.getText().toString().trim();
        editor.putString(AppPreference.setting_User_LastName, last_name);
        editor.commit();

        email = editEmail.getText().toString().trim();
        editor.putString(AppPreference.setting_User_Email, email);
        editor.commit();

        if (checkEnglish.isChecked()) {
            editor.putString(AppPreference.language_type, "2");
            editor.commit();
        }
        if (checkHindi.isChecked()) {
            strLang = "1";
            editor.putString(AppPreference.language_type, "1");
            editor.commit();
        }
        if (checkEnglish.isChecked() && checkHindi.isChecked()) {
            editor.putString(AppPreference.language_type, "1,2");
            editor.commit();
            strLang = "1,2";
        }
        if (switchPushNotifications.isChecked()) {
            notification = "1";

        } else {
            notification = "0";
        }

        if (switchSound.isChecked()) {
            strSound = "1";
        } else {
            strSound = "0";
        }


    }

    private void showAlertDialog(String emptyFieldName) {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                SettingActivity.this).create();
        // Setting Dialog Message
        alertDialog.setMessage(emptyFieldName);

        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                alertDialog.dismiss();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == EXTERNAL_PERMISSION_REQUEST) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            inviteFriends();
        }
    }

    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(this).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, SettingActivity.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SettingActivity.class.getName());
        mFirebaseAnalytics.logEvent("SettingActivity", bundle);
    }






    /*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/

    /******
     * Execute Save API
     ********/
    public void executeSaveUserInfoAPI() {
        String strApiUrl = "http://aroundpost.com/api/Users/profile";

        GetApisResponses getApisResponses = new GetApisResponses(SettingActivity.this,
                iHttpResponseListener,
                "POST",
                strApiUrl,
                iHttpExceptionListener,
                authToken);
        getApisResponses.execute(getJsonAPIsParam());
    }

    // Define @Param for the Get + Post APIs
    private JSONObject getJsonAPIsParam() {
        JSONObject jsonObject = new JSONObject();
        try {
            getData();

            jsonObject.put("first_name", first_name);
            jsonObject.put("last_name", last_name);
            jsonObject.put("email", email);
            jsonObject.put("notification", notification);
            jsonObject.put("sound", strSound);
            jsonObject.put("languages", strLang);
            Log.e("one_signal_id + setting", AppPreference.getAppPreference(SettingActivity.this).getOneSignalID());
            jsonObject.put("one_signal_id", AppPreference.getAppPreference(SettingActivity.this).getOneSignalID());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1: {
                    try {
                        JSONObject jsonObject = new JSONObject(strResponse);
                        Toast.makeText(context, "" + "Changes saved", Toast.LENGTH_SHORT).show();
                        gettingLastUpdatedUserProfileData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 2: {
                    Toast.makeText(context, "" + strErrorMessage, Toast.LENGTH_SHORT).show();
                    break;
                } case 3: {
                    try{
                    JSONObject jsonObject = new JSONObject(strResponseLastUpdateInfo);
                        String message = jsonObject.getString("message");
                        String statusCode = jsonObject.getString("statusCode");
                        if (message.equals("Data Found") && statusCode.equals("200")){
                            String data = jsonObject.getString("data");
                            JSONObject dataObject = new JSONObject(data);
                            String first_name  = dataObject.getString("first_name");
                            String last_name  = dataObject.getString("last_name");
                            String email  = dataObject.getString("email");
                            String device_id  = dataObject.getString("device_id");
                            String notification  = dataObject.getString("notification");
                            String sound  = dataObject.getString("sound");
                            String languages  = dataObject.getString("languages");

                            editEmail.setText(email);
                            editFirstName.setText(first_name);
                            editLastName.setText(last_name);
                            if (notification.equals("1"))
                                switchPushNotifications.setChecked(true);
                            else
                                switchPushNotifications.setChecked(false);
                            if (sound.equals("1"))
                                switchSound.setChecked(true);
                            else
                                switchSound.setChecked(false);
                            //language
                            if (languages.contains(",")){
                                String arrayLang[] = languages.split(",");
                                String str0  = arrayLang[0];
                                String str1  = arrayLang[1];
                                if (str0.equals("2")){
                                    checkEnglish.setChecked(true);
                                }else if (str0.equals("1")){
                                    checkHindi.setChecked(true);
                                }else {

                                }
                                if (str1.equals("2")){
                                    checkEnglish.setChecked(true);
                                }else if (str1.equals("1")){
                                    checkHindi.setChecked(true);
                                }else{

                                }
                            }else{
                                if (languages.equals("2")){
                                    checkEnglish.setChecked(true);
                                }else if (languages.equals("1")){
                                    checkHindi.setChecked(true);
                                }else{

                                }
                            }
                        }else{
                            Toast.makeText(context, "Server Not Found", Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                } case 4: {
                    Toast.makeText(context, "" + strErrorMessageLastUpdateInfo, Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }
    };
    private String TAG = "SettingActivity";
    IHttpResponseListener iHttpResponseListener = new IHttpResponseListener() {
        @Override
        public void handleResponse(String response) {
            Log.e(TAG, "****SETTING API****" + response);
            strResponse = response;
            handler.sendEmptyMessage(1);
        }
    };
    IHttpExceptionListener iHttpExceptionListener = new IHttpExceptionListener() {
        @Override
        public void handleException(String exception) {
            Log.e(TAG, "****SETTING API****" + exception);
            strErrorMessage = exception;
            handler.sendEmptyMessage(2);
        }
    };



    private void gettingLastUpdatedUserProfileData() {
        String strApiUrl = "http://aroundpost.com/api/Users/profile";

        GetApisResponses getApisResponsesLastUpdateInfo = new GetApisResponses(SettingActivity.this,
                iHttpResponseListenerLastUpdateInfo,
                "GET",
                strApiUrl,
                iHttpExceptionListenerLastUpdateInfo,
                authToken);
        getApisResponsesLastUpdateInfo.execute(getJsonAPIsParamGetInfo());
    }

    IHttpResponseListener iHttpResponseListenerLastUpdateInfo = new IHttpResponseListener() {
        @Override
        public void handleResponse(String response) {
            Log.e(TAG, "****SETTING API****" + response);
            strResponseLastUpdateInfo = response;
            handler.sendEmptyMessage(3);
        }
    };
    IHttpExceptionListener iHttpExceptionListenerLastUpdateInfo = new IHttpExceptionListener() {
        @Override
        public void handleException(String exception) {
            Log.e(TAG, "****SETTING API****" + exception);
            strErrorMessageLastUpdateInfo = exception;
            handler.sendEmptyMessage(4);
        }
    };

    // Define @Param for the Get + Post APIs
    private JSONObject getJsonAPIsParamGetInfo() {
        JSONObject jsonObject = new JSONObject();
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
