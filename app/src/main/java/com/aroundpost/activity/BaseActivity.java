package com.aroundpost.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.aroundpost.AppPreference;
import com.aroundpost.R;


import java.util.Locale;


/**
 * Created by Manish on 9/22/2016.
 */
public class BaseActivity extends AppCompatActivity {
    SharedPreferences preferences;
//    Singleton singleton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        singleton = Singleton.getInstance();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            // Do something for lollipop and above versions
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.background_purple));
        } else {
            // do something for phones running an SDK before lollipop
        }
        //getAppLang();
    }

  /*  @Override
    protected void onResume() {
        super.onResume();
        getAppLang();
    }*/

    public void changeFragment(int containerId, Fragment fragment, boolean isBundleToSend, int type) {

//        if(singleton.getmFeedNewsModelList().size()>0){
//            singleton.getmFeedNewsModelList().clear();
//        }

        AppPreference.getAppPreference(this).setWhichApi(type);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putBoolean("hasdata", isBundleToSend);
        fragment.setArguments(bundle);
        fragmentTransaction.add(containerId, fragment);
        fragmentTransaction.commit();
    }

    private void getAppLang() {
        preferences = getSharedPreferences(AppPreference.AROUND_POST_PREFERENCE, Context.MODE_PRIVATE);
        String lang = preferences.getString(AppPreference.LANG, "");
        Configuration config = getBaseContext().getResources().getConfiguration();

        if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);

            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
    }
}
