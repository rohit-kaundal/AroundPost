package com.aroundpost.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.AroundPostApp;
import com.aroundpost.R;
import com.aroundpost.adapter.FeedSelectedChannelListAdapter;
import com.aroundpost.adapter.FeedSelectedListAdapter;
import com.aroundpost.fragment.FeedNewsFragment;
import com.aroundpost.fragment.InShortLikeFragment;
import com.aroundpost.fragment.ReportAStoryFragment;
import com.aroundpost.fragment.SlidingFragment;
import com.aroundpost.net.GetApisResponses;
import com.aroundpost.net.IHttpExceptionListener;
import com.aroundpost.net.IHttpResponseListener;
import com.aroundpost.request.SaveStoryRequest;
import com.aroundpost.response.ChannelData;
import com.aroundpost.response.GenericResponse;
import com.aroundpost.response.LocationData;
import com.aroundpost.response.SearchResultNewsModel;
import com.aroundpost.response.SignUpResponse;
import com.aroundpost.services.FetchLocationService;
import com.aroundpost.services.GPSTracker;
import com.aroundpost.utils.BasicUtils;
import com.aroundpost.utils.Constants;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aroundpost.activity.FacebookAccountKitActivity.isRegisterLoginTime;

/**
 * Created by Manish on 4/1/2017.
 */

public class NewFeedNewsActivity extends BaseActivity implements SlidingFragment.AppData, View.OnClickListener, SearchView.OnQueryTextListener {
    private static final String TAG = "NewFeedNewsActivity";
    private static final int INFINITE_LOCATION_PERMISSION_REQUEST = 999;
    private static final int PERMISSIONS_MULTIPLE_REQUEST_LOACTION = 111;
    private static final int PERMISSIONS_MULTIPLE_REQUEST_STORAGE = 222;
    public static ImageView ivSaveStory, ivShareNews;
    public static int selection;
    public static int selectNear;
    public static TextView textNews, text_location;
    public static double doubleLatitude, doubleLogitude;
    public Toolbar mToolbar;
    public int mPosition;
    public String mStoryId;
    public ArrayList<SearchResultNewsModel> mFeedNewsModelList = new ArrayList<>();
    public String query;

    // GPSTracker class
    GPSTracker gps;
    Menu menu;
    int featuredId = 0;
    String currentFragment = "";
    String currentVersion = "";
    AlertDialog.Builder builder;
    SharedPreferences _prefs;
    SharedPreferences.Editor _editor;
    private DrawerLayout mDrawerLayout;
    private ListView mListView, mChannelListView;
    private FeedSelectedListAdapter mFeedSelectedListAdapter;
    private FeedSelectedChannelListAdapter mFeedSelectedChannelListAdapter;
    public ArrayList<LocationData> mLocationDataArrayList;
    public ArrayList<ChannelData> mChannelDataArrayList;
    private String locationId, channelID;
    private int mContainerID;
    private ActionBarDrawerToggle mDrawerToggle;
    private LinearLayout mBottomRelView;
    private BroadcastReceiver broadcastReceiver;
    private ImageView ivPreviousView, ivNextNews;
    private FloatingActionButton floatingActionButton;
    private Dialog dialog;
    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean isDrawerOpen = false;
    private boolean isMenuOpen = false;
    private String message;

    private String strResponseLocation, strExceptionLocation, strResponseChannels, strExceptionChannels;
    // Define the Handler Thread....to fetch the Response:::
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1: {
                    // Show the Data in widgets those data can get from the Apis
                    try {
                        JSONObject jsonObject = new JSONObject(strResponseLocation);
                        String message = jsonObject.getString("message");
                        String statusCode = jsonObject.getString("statusCode");
                        if (message.equals("Success") && statusCode.equals("200")) {
                            Log.e(TAG, "*********Location Update Successful*********");
                            Toast.makeText(NewFeedNewsActivity.this, "Locations Updated!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 2: {
                    Toast.makeText(NewFeedNewsActivity.this, "Server not respond.", Toast.LENGTH_SHORT).show();
                    break;
                }
                case 3: {
                    try {
                        JSONObject jsonObject = new JSONObject(strResponseChannels);
                        String message = jsonObject.getString("message");
                        String statusCode = jsonObject.getString("statusCode");
                        if (message.equals("Success") && statusCode.equals("200")) {
                            Log.e(TAG, "*************Channels Update Sucessfull***************");
                            Toast.makeText(NewFeedNewsActivity.this, "Channels Update!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }

                case 4: {
                    Toast.makeText(NewFeedNewsActivity.this, "Server not respond.", Toast.LENGTH_SHORT).show();
                    break;
                }

            }
        }
    };
    IHttpResponseListener iHttpResponseListenerLocation = new IHttpResponseListener() {
        @Override
        public void handleResponse(String response) {
            Log.e(TAG, "*************Reponse************" + response);
            strResponseLocation = response;
            handler.sendEmptyMessage(1);
        }
    };
    IHttpExceptionListener iHttpExceptionListenerLocation = new IHttpExceptionListener() {
        @Override
        public void handleException(String exception) {
            Log.e(TAG, "*************exception************" + exception);
            strExceptionLocation = exception;
            handler.sendEmptyMessage(2);
        }
    };
    IHttpResponseListener iHttpResponseListenerChannels = new IHttpResponseListener() {
        @Override
        public void handleResponse(String response) {
            Log.e(TAG, "*************Reponse************" + response);
            strResponseChannels = response;
            handler.sendEmptyMessage(3);
        }
    };
    IHttpExceptionListener iHttpExceptionListenerChannels = new IHttpExceptionListener() {
        @Override
        public void handleException(String exception) {
            Log.e(TAG, "*************exception************" + exception);
            strExceptionLocation = exception;
            handler.sendEmptyMessage(4);
        }
    };

    public void hideShowLayoutSliding(int state) {
        mToolbar.setVisibility(state);
        mBottomRelView.setVisibility(state);
    }

    // method to show and hide the top and bottom toolbar with animations
    public void hideShowLayout(int state, boolean type) {

        if (type) {
            //GONE
            ShowGone(state);
        } else {
            ShowVisible(state);
            //VISIBLE
        }

//        mToolbar.setVisibility(state);
//        mBottomRelView.setVisibility(state);

    }

    public void ShowVisible(int state) {
       /* Animation topdown = AnimationUtils.loadAnimation(NewFeedNewsActivity.this,
                R.anim.toolbar_down);

        mToolbar.startAnimation(topdown);*/
        mToolbar.setVisibility(state);
/*
        Animation bottomup = AnimationUtils.loadAnimation(NewFeedNewsActivity.this,
                R.anim.bottom_up);

        mBottomRelView.startAnimation(bottomup);*/
        mBottomRelView.setVisibility(state);
    }

    public void ShowGone(final int state) {

        mToolbar.setVisibility(state);
        mBottomRelView.setVisibility(state);
/*



        Animation toolup = AnimationUtils.loadAnimation(NewFeedNewsActivity.this,
                R.anim.toolbar_up);

        mToolbar.startAnimation(toolup);

        toolup.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        Animation bottomDown = AnimationUtils.loadAnimation(NewFeedNewsActivity.this,
                R.anim.bottom_down);

        mBottomRelView.startAnimation(bottomDown);


        bottomDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
*/

    }

    public String getmStoryId() {
        return mStoryId;
    }

    public void setmStoryId(String mStoryId) {
        this.mStoryId = mStoryId;
    }

    public ArrayList<SearchResultNewsModel> getmFeedNewsModelList() {
        return mFeedNewsModelList;
    }

    public void setmFeedNewsModelList(ArrayList<SearchResultNewsModel> mFeedNewsModelList) {
        this.mFeedNewsModelList = mFeedNewsModelList;
    }

    public void setTopMargin(boolean hide) {
        FrameLayout fm = (FrameLayout) findViewById(R.id.container);
        DrawerLayout.LayoutParams params = new DrawerLayout.LayoutParams(
                DrawerLayout.LayoutParams.WRAP_CONTENT,
                DrawerLayout.LayoutParams.WRAP_CONTENT
        );
        if (hide) {
            params.setMargins(0, (int) (48 * Resources.getSystem().getDisplayMetrics().density), 0, 0);
        } else {
            params.setMargins(0, 0, 0, 0);
        }
        fm.setLayoutParams(params);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_new_activity);
        _prefs = BasicUtils.getSharedPreferences(NewFeedNewsActivity.this);
        _editor = _prefs.edit();
        SignUpResponse signUpResponse = AppPreference.getAppPreference(NewFeedNewsActivity.this).getSignUpDetails();
        if (signUpResponse != null) {
            String accessToken = signUpResponse.getData().getApi_key();
            Log.e("Acess Token+++++", "#######+" + accessToken);
            _editor.putString(AppPreference.USER_TOKEN_KEY, accessToken);
            _editor.commit();
        }
        mLocationDataArrayList = new ArrayList<LocationData>();
        mChannelDataArrayList = new ArrayList<ChannelData>();
        /*Getting Current Location*/
        gettingCurrentLocation();


        initView();

        //to check whether the app update is available on play store or not.
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        new GetVersionCode().execute();

        Intent fetchLocationService = new Intent(NewFeedNewsActivity.this, FetchLocationService.class);
        startService(fetchLocationService);


        firebaseAnalytics();
        mFeedSelectedListAdapter = new FeedSelectedListAdapter(this, R.layout.spinner_layout);
        mListView.setAdapter(mFeedSelectedListAdapter);

        mFeedSelectedChannelListAdapter = new FeedSelectedChannelListAdapter(this, R.layout.spinner_layout);
        mChannelListView.setAdapter(mFeedSelectedChannelListAdapter);


        if (AppPreference.getAppPreference(this).getLocationData()!= null)
            mLocationDataArrayList.addAll(AppPreference.getAppPreference(this).getLocationData().getData());

        if (AppPreference.getAppPreference(this).getChannelData().getData() != null)
            mChannelDataArrayList.addAll(AppPreference.getAppPreference(this).getChannelData().getData());


        locationId = AppPreference.getAppPreference(this).getLocationNewsPreference().trim();

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                mToolbar,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                if (currentFragment.equals("slide")) {
                    mBottomRelView.setVisibility(View.VISIBLE);
                }

                super.onDrawerClosed(view);
                isDrawerOpen = false;
                //getSupportActionBar().setTitle(message);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {

                if (isMenuOpen) {
                    onPanelClosed(featuredId, menu);
                    isMenuOpen = false;
                }

                if (currentFragment.equals("slide")) {
                    mBottomRelView.setVisibility(View.GONE);
                }
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle(message);
                isDrawerOpen = true;
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);


        if (isRegisterLoginTime) {
            /******Update Location*******/
            getExecuteLocationAPI();
            /********Update Channels********/
            getExecuteChannelsAPI();
        } else {
            Log.e(TAG, "***********No Need UpDate*********");
        }

        Log.e("locationId bere ", locationId);
        boolean isHimachalThere = false;
        if (locationId.contains(",")) {
            String[] splitIds = locationId.split(",");
            for (int i = 0; i < splitIds.length; i++) {
                for (LocationData locationData : mLocationDataArrayList) {
                    Log.d("Id ", locationData.getId() + " - " + locationData.getLocation_name() + " - " + locationData.getParent_id());
                    if (locationData.getId().equals(splitIds[i])) {
                        mFeedSelectedListAdapter.add(locationData);
                        mFeedSelectedListAdapter.notifyDataSetChanged();
                    }
                }
            }

        } else {
            for (LocationData locationData : mLocationDataArrayList) {
                if (locationData.getId().equals(locationId)) {
                    mFeedSelectedListAdapter.add(locationData);
                    mFeedSelectedListAdapter.notifyDataSetChanged();
                }
            }
        }

        channelID = AppPreference.getAppPreference(this).getChannelNewsPreference().trim();
        Log.d("channel bere ", channelID);
        if (channelID.contains(",")) {
            String[] splitIds = channelID.split(",");
            for (int i = 0; i < splitIds.length; i++) {
                for (ChannelData channelData : mChannelDataArrayList) {
                    if (channelData.getId().equals(splitIds[i])) {
                        mFeedSelectedChannelListAdapter.add(channelData);
                        mFeedSelectedChannelListAdapter.notifyDataSetChanged();
                    }
                }
            }
        } else {
            for (ChannelData channelData : mChannelDataArrayList) {
                if (channelData.getId().equals(channelID)) {
                    mFeedSelectedChannelListAdapter.add(channelData);
                    mFeedSelectedChannelListAdapter.notifyDataSetChanged();
                }
            }
        }

        if (mFeedSelectedChannelListAdapter.getCount() > 0) {
            findViewById(R.id.view).setVisibility(View.VISIBLE);
        }

        BasicUtils.getBasicUtilInstance().setListViewHeightBasedOnChildren(mListView);
        BasicUtils.getBasicUtilInstance().setListViewHeightBasedOnChildren(mChannelListView);

        mContainerID = R.id.container;
        Fragment fragment = new FeedNewsFragment();
        changeFragment(mContainerID, fragment, true, AppPreference.getAppPreference(this).getWhichApi());

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                LocationData locationData = (LocationData) parent.getItemAtPosition(position);
                String locationId = locationData.getId();
                AppPreference.getAppPreference(NewFeedNewsActivity.this).setPreferedCategory("L" + locationId);
                Fragment fragment = new FeedNewsFragment();
                changeFragment(mContainerID, fragment, false, 0);

                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        mChannelListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChannelData locationData = (ChannelData) parent.getItemAtPosition(position);
                String locationId = locationData.getId();
                AppPreference.getAppPreference(NewFeedNewsActivity.this).setPreferedCategory("C" + locationId);
                Fragment fragment = new FeedNewsFragment();
                changeFragment(mContainerID, fragment, false, 0);

                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        });


        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String type = intent.getStringExtra("type");
                if (type.equals("1")) {
//                    getLocationPermission();
                    checkLocationPermissions();
                } else if (type.equals("2")) {
//                    Fragment fragment = new FeedNewsFragment();
//                    changeFragment(mContainerID, fragment, false, 1);
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("location-broadcast"));

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewFeedNewsActivity.this, FeedChannelLocationSelectActivity.class);
                startActivity(intent);
            }
        });

//        getExternalStoragePermission();
        /*External Storage Permissions*/
        checkExternalStoragePermissions();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGpsEnabled) {
//            getLocationPermission();
            checkLocationPermissions();
        } else {
            displayLocationSettingsRequest(this);
//            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//            startActivity(intent);
        }
    }

    private void gettingCurrentLocation() {

        SharedPreferences prefs;
        SharedPreferences.Editor editor;
        prefs = getSharedPreferences(AppPreference.AROUND_POST_PREFERENCE, MODE_PRIVATE);
        editor = prefs.edit();
        // create class object
        gps = new GPSTracker(NewFeedNewsActivity.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();


            doubleLatitude = latitude;
            doubleLogitude = longitude;
            editor.putString(AppPreference.LATITUDE, "" + doubleLatitude);
            editor.putString(AppPreference.LONGITUDE, "" + doubleLogitude);
            editor.commit();
            Log.e("******Latitude****", "************" + prefs.getString(AppPreference.LATITUDE, ""));
            Log.e("*****Longitude***", "************" + prefs.getString(AppPreference.LONGITUDE, ""));
            // \n is for new line
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    public void feedHideLayout() {
        mToolbar.setVisibility(View.VISIBLE);
        mBottomRelView.setVisibility(View.GONE);
    }

    public void currentFragment(String current) {
        this.currentFragment = current;
    }

  /*  private void getExternalStoragePermission() {
        if (ActivityCompat.checkSelfPermission(NewFeedNewsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(NewFeedNewsActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    EXTERNAL_PERMISSION_REQUEST);
        } else {
            takeScreenShotAndShare();
        }
    }*/

    /* private void getLocationPermission() {
         if (ActivityCompat.checkSelfPermission(NewFeedNewsActivity.this,
                 Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                 && ActivityCompat.checkSelfPermission(NewFeedNewsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
             ActivityCompat.requestPermissions(NewFeedNewsActivity.this,
                     new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                             Manifest.permission.ACCESS_COARSE_LOCATION},
                     LOCATION_PERMISSION_REQUEST);
         } else {
             showNearByStoriesDialog();
         }
     }
 */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initView() {
        mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("");
        textNews = (TextView) findViewById(R.id.textNews);
//        text_location = (TextView) findViewById(R.id.text_location);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            getSupportActionBar().setHomeAsUpIndicator(getDrawable(R.drawable.hamburger_icon));
        } else {
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.hamburger_icon));
        }

        ivSaveStory = (ImageView) findViewById(R.id.iv_save_news);
        ivPreviousView = (ImageView) findViewById(R.id.iv_previous_news);
        ivNextNews = (ImageView) findViewById(R.id.iv_next_news);
        ivShareNews = (ImageView) findViewById(R.id.iv_share_news);
        mBottomRelView = (LinearLayout) findViewById(R.id.re_navigation_bottom);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mListView = (ListView) findViewById(R.id.listview);
        mChannelListView = (ListView) findViewById(R.id.channellistview);

        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingbutton);


        ivPreviousView.setOnClickListener(this);
        ivNextNews.setOnClickListener(this);
//        text_location.setOnClickListener(this);
        ivShareNews.setOnClickListener(this);
        ivSaveStory.setOnClickListener(this);


    }

    @Override
    public SearchResultNewsModel getData(int position) {
        this.mPosition = position;
        return mFeedNewsModelList.get(position);

    }

    public void setTitle(String message) {
        this.message = message;
//        Log.d("Message ", message);
        /*mToolbar.setTitle(message);
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));*/
        textNews.setText(message);
        textNews.setTextColor(getResources().getColor(android.R.color.white));
        AppPreference.getAppPreference(NewFeedNewsActivity.this).setTitlePref(message);

    }

    @Override
    public void onClick(View v) {
        Log.d("Mposition ", mPosition + "");
        switch (v.getId()) {
           case R.id.iv_share_news:
               Toast.makeText(getApplicationContext(),"Clicked on share news",Toast.LENGTH_LONG).show();
               break;

           /* case R.id.iv_save_news:
               // savedStorriesApi();
                Intent intent = new Intent(NewFeedNewsActivity.this,SourceLinkOpen.class);
                startActivity(intent);
                break;*/
        }
    }

    private void takeScreenShotAndShare() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // create bitmap screen capture
            //View v1 = getWindow().getDecorView().getRootView();
/*
            View v1 = getWindow().getDecorView().findViewById(R.id.linearnews);
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);
            String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap,"title", null);
            Uri bmpUri = Uri.parse(pathofBmp);
            final Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
//            emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
//            emailIntent1.setType("image/png");
            emailIntent1.setType("image*//*");
           // emailIntent1.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
            emailIntent1.putExtra(Intent.EXTRA_TEXT, "Shared via AroundPost");

            emailIntent1.putExtra(Intent.EXTRA_HTML_TEXT, "Shared via <a href=\"http://aroundpost.com/dl\"> AroundPost</a>");
            emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);

            startActivity(Intent.createChooser(emailIntent1, "Share News"));*/

            Bitmap bm = screenShot(getWindow().getDecorView().findViewById(R.id.linearnews));
            File file = saveBitmap(bm, "aroundpost_screenshot.png");
            Log.i("chase", "filepath: "+file.getAbsolutePath());
            Uri uri = Uri.fromFile(new File(file.getAbsolutePath()));
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Shared Via AroundPost.");
            shareIntent.putExtra(Intent.EXTRA_HTML_TEXT, "<html><body><h1>Shared Via AroundPost</h1></body><html>");

            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            shareIntent.setType("image/*");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(shareIntent, "share via"));

        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }

    private Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private static File saveBitmap(Bitmap bm, String fileName){
        final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(path);
        if(!dir.exists())
            dir.mkdirs();
        File file = new File(dir, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 90, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
    private void savedStorriesApi() {
        SaveStoryRequest saveStoryRequest = new SaveStoryRequest();
        SearchResultNewsModel searchResultNewsModel = new SearchResultNewsModel();
        saveStoryRequest.setStory_id(getmStoryId());
        Call<GenericResponse> savedStoriesCallback = AroundPostApp.getAroundPostInterface(this).getSavedStories(saveStoryRequest);
        savedStoriesCallback.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(NewFeedNewsActivity.this, "Story saved successfully", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
//
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(NewFeedNewsActivity.this, 1);
                        } catch (IntentSender.SendIntentException e) {
//                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    /* @Override
     public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
         if (requestCode == LOCATION_PERMISSION_REQUEST) {
             for (int i = 0; i < grantResults.length; i++) {
                 if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                     return;
                 }
             }
             Intent fetchLocationService = new Intent(NewFeedNewsActivity.this, FetchLocationService.class);
             fetchLocationService.putExtra("doBroadcast", true);
             startService(fetchLocationService);
         } else if (requestCode == EXTERNAL_PERMISSION_REQUEST) {
             for (int i = 0; i < grantResults.length; i++) {
                 if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                     return;
                 }
             }
             //takeScreenShotAndShare();
         }
     }
 */
    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (isDrawerOpen) {
            this.menu = menu;
            this.featuredId = featureId;
            mDrawerLayout.closeDrawer(GravityCompat.START);
            isDrawerOpen = false;
        }
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    public void onPanelClosed(int featureId, Menu menu) {
        super.onPanelClosed(featureId, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);

        if (fragment != null && fragment instanceof FeedNewsFragment) {
            getMenuInflater().inflate(R.menu.menu_home, menu);

            SearchManager searchManager = (SearchManager)
                    getSystemService(Context.SEARCH_SERVICE);
            MenuItem item = menu.findItem(R.id.action_search);
            Log.d("item ", item + "");
            SearchView searchView = (SearchView) item.getActionView();
            Log.d("Search ", searchView + "");

            EditText searchBox = ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
            searchBox.setTextColor(Color.parseColor("#ffffff"));

            ImageView searchButton = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_button);
            searchButton.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
            ImageView searchClose = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
            searchClose.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
            searchClose.setVisibility(View.GONE);
            searchView.setSearchableInfo(searchManager.
                    getSearchableInfo(getComponentName()));
            searchView.setSubmitButtonEnabled(true);
            searchView.setOnQueryTextListener(this);

        } else if (fragment != null && fragment instanceof InShortLikeFragment) {
            getMenuInflater().inflate(R.menu.menu_splash, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_report_story) {
         //takeScreenShotAndShare();

           Fragment fragment = new ReportAStoryFragment();
           changeFragment(mContainerID, fragment, false, 0);
        } else if (id == android.R.id.home) {
            Intent intent = new Intent(NewFeedNewsActivity.this, FeedSelectedListActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_nearby_stories) {
            //check if location permission is given.
//            checkLocationPermissionsForNearByStories();
            /*Setting Up Infinite Check for Location*/
            settingUpInfinitCheckForLocationPermission();
        } else if (id == R.id.action_unseen_stories) {
            getHeaderStoriesApi(2);
        } else if (id == R.id.action_latest_stories) {
            Fragment fragment = new FeedNewsFragment();
            changeFragment(mContainerID, fragment, false, 0);
        } else if (id == R.id.action_save_story) {
            savedStorriesApi();
        } else if (id == R.id.action_my_stories) {
            viewMyStories();
        } else if (id == R.id.action_settings) {
            Intent intent = new Intent(NewFeedNewsActivity.this, SettingActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void showNearByStoriesDialog() {
        dialog = new Dialog(NewFeedNewsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_distance_cover);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button tv10Km = (Button) dialog.findViewById(R.id.km10);
        Button tv50Km = (Button) dialog.findViewById(R.id.km50);
        Button tv100Km = (Button) dialog.findViewById(R.id.km100);
        Button tv200Km = (Button) dialog.findViewById(R.id.km200);
            /*setting dialog box to show nearby stories according to radius*/
        onDialogClickEvent(tv10Km, 10);
        onDialogClickEvent(tv50Km, 50);
        onDialogClickEvent(tv100Km, 100);
        onDialogClickEvent(tv200Km, 200);
        dialog.show();
    }

    private void onDialogClickEvent(TextView tvGeneral, final int i) {
        tvGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppPreference.getAppPreference(NewFeedNewsActivity.this).setDistanceCovered(i);

                selection = i;
                AppPreference.getAppPreference(NewFeedNewsActivity.this).setTypeDistance(i);
                getHeaderStoriesApi(1);

                dialog.dismiss();
            }
        });
    }

    private void getHeaderStoriesApi(int type) {
        String userLocation = AppPreference.getAppPreference(NewFeedNewsActivity.this).getUserLocation();
        if (userLocation != null && userLocation.length() > 0) {
            // setting title of near by stories
            setNearByLocationTitle(selection);
            Fragment fragment = new FeedNewsFragment();
            changeFragment(mContainerID, fragment, false, type);
        } else {
            //check if location is enabled.
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGpsEnabled) {
                checkLocationPermissions();
            } else {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        }
    }

    public void setNearByLocationTitle(int type) {

        if (type == 10) {
            setTitle("Local posts around 10kms");
        }
        if (type == 50) {
            setTitle("Local posts around 50 Kms");
        }
        if (type == 100) {
            setTitle("Local posts around 100 Kms");
        }
        if (type == 200) {
            setTitle("Local posts around 200 Kms");
        }
    }

    private void viewMyStories() {
        Fragment fragment = new FeedNewsFragment();
        changeFragment(mContainerID, fragment, false, 4);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        this.query = query;
        Fragment fragment = new FeedNewsFragment();
        changeFragment(mContainerID, fragment, false, 3);
        Log.d("Query == ", query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }



    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof FeedNewsFragment) {
            if (isDrawerOpen) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                isDrawerOpen = false;
            } else {
                moveTaskToBack(true);
                finish();
            }
        } else if (fragment instanceof InShortLikeFragment) {
            Fragment fragment1 = new FeedNewsFragment();
            changeFragment(mContainerID, fragment1, true, AppPreference.getAppPreference(this).getWhichApi());

        } else {
            super.onBackPressed();
            moveTaskToBack(true);
            finish();
        }
    }

    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(this).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, NewFeedNewsActivity.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, NewFeedNewsActivity.class.getName());
        mFirebaseAnalytics.logEvent("NewFeedNewsActivity", bundle);
    }

    private void showAlertDialog() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(NewFeedNewsActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(NewFeedNewsActivity.this, R.style.MyDialogTheme);
        }
        builder.setTitle("Around Post");
        builder.setMessage("Update is available");


        String positiveText = "Update App";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                });

        String negativeText = "Later";
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    private void checkLocationPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissionLocation();
        } else {
            // write your logic here
        }

    }


    private void checkExternalStoragePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissionStorage();
        } else {
            // write your logic here
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermissionStorage() {
        if (ContextCompat.checkSelfPermission(NewFeedNewsActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (NewFeedNewsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Toast.makeText(NewFeedNewsActivity.this, "Need enable the External Storage permission.", Toast.LENGTH_SHORT).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_MULTIPLE_REQUEST_STORAGE);
            }
        } else {
            // write your logic code if permission already granted
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermissionLocation() {
        if (ContextCompat.checkSelfPermission(NewFeedNewsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) + ContextCompat
                .checkSelfPermission(NewFeedNewsActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (NewFeedNewsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (NewFeedNewsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                Toast.makeText(NewFeedNewsActivity.this, "Need enable the Location permissions.", Toast.LENGTH_SHORT).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSIONS_MULTIPLE_REQUEST_LOACTION);
            }
        } else {
            // write your logic code if permission already granted
        }
    }


    private void getExecuteLocationAPI() {

        String strToken = _prefs.getString(AppPreference.USER_TOKEN_KEY, "");

        // Call the Api:
        GetApisResponses apiResponseAsync = new GetApisResponses(NewFeedNewsActivity.this,
                iHttpResponseListenerLocation,
                "POST",
                Constants.LOCATION_UPDATES,
                iHttpExceptionListenerLocation,
                strToken);
        apiResponseAsync.execute(getLocationRequestParams());
    }

    // Define @Param for the Get + Post APIs
    private JSONObject getLocationRequestParams() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("locations", locationId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    private void getExecuteChannelsAPI() {

        String strToken = _prefs.getString(AppPreference.USER_TOKEN_KEY, "");

        // Call the Api:
        GetApisResponses apiResponseAsync = new GetApisResponses(NewFeedNewsActivity.this,
                iHttpResponseListenerChannels,
                "POST",
                Constants.CHANNELS_UPDATES,
                iHttpExceptionListenerChannels,
                strToken);
        apiResponseAsync.execute(getChannelsRequestParams());
    }

    // Define @Param for the Get + Post APIs
    private JSONObject getChannelsRequestParams() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("channels", channelID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + NewFeedNewsActivity.this.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    showAlertDialog();

                } else {

                }
            }
            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void turnGPSOn(){
        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

    }

    /************************OnPermissions Grant or denied**********************************/
    /*******************************************************/
    /*********
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) WRITE_EXTERNAL_STORAGE PERMISSION
     * 2) RECORD_AUDIO
     **********/

    private void settingUpInfinitCheckForLocationPermission() {

       // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkAndRequestPermissions()) {
                askForPermissions();
            }
            //turnGPSOn();
            showNearByStoriesDialog();
        //}
    }

    private void askForPermissions() {
        ArrayList<String> listpermissionsNeeded = new ArrayList<String>();
        listpermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        listpermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        ActivityCompat.requestPermissions(this, listpermissionsNeeded.toArray(new String[listpermissionsNeeded.size()]), INFINITE_LOCATION_PERMISSION_REQUEST);

    }

    ArrayList<String> listpermissionsNeeded = new ArrayList<String>();

    @TargetApi(Build.VERSION_CODES.M)
    public boolean checkAndRequestPermissions() {

        listpermissionsNeeded = new ArrayList<String>();

        int permissionACCESS_FINE_LOCATION = ContextCompat.checkSelfPermission(NewFeedNewsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionACCESS_COARSE_LOCATION = ContextCompat.checkSelfPermission(NewFeedNewsActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permissionACCESS_FINE_LOCATION != PackageManager.PERMISSION_GRANTED) {
            listpermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionACCESS_COARSE_LOCATION != PackageManager.PERMISSION_GRANTED) {
            listpermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listpermissionsNeeded.isEmpty()) {

            return false;
        }

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSIONS_MULTIPLE_REQUEST_LOACTION:
                if (grantResults.length > 0) {
                    boolean accessFineLocation = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean accessCoarseLocation = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (accessFineLocation && accessCoarseLocation) {
                        // write your logic here

                    } else {

                    }
                }
                break;

            case PERMISSIONS_MULTIPLE_REQUEST_STORAGE:
                if (grantResults.length > 0) {
                    boolean externalstorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (externalstorage) {
                        // write your logic here
                    } else {
                    }
                }
                break;

            case INFINITE_LOCATION_PERMISSION_REQUEST: {
                if (requestCode == INFINITE_LOCATION_PERMISSION_REQUEST) {
                    boolean isallpermissioongranted = true;
                    for (int i = 0, len = permissions.length; i < len; i++) {
                        String permission = permissions[i];
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            boolean showRationale = shouldShowRequestPermissionRationale(permission);
                            if (!showRationale) {
                                isallpermissioongranted = false;
                                break;
                            }
                        }
                    }
                    if (!isallpermissioongranted) {
                        // popup
                        showPermissionsDialog();
                    } else {
                        if (checkAndRequestPermissions()) {
                            showNearByStoriesDialog();
                        }
                    }


//                    Map<String, Integer> perms = new HashMap<>();
//                    // Initialize the map with both permissions
//                    perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
//                    perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
//
//                    // Fill with actual results from user
//                    if (grantResults.length > 0) {
//                        for (int i = 0; i < permissions.length; i++)
//                            perms.put(permissions[i], grantResults[i]);
//
//                        // Check for both permissions
//                        if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
//                                && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                            Log.d(TAG, "External Storage & Audio Record permission granted");
//                            /******** process the normal flow
//                             *else any one or both the permissions are not granted
//                             *Perform Tasks That you Want******************/
//                            showNearByStoriesDialog();
//
//                        } else {
//                            Log.d(TAG, "Some permissions are not granted ask again ");
//                            //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                            // shouldShowRequestPermissionRationale will return true
//                            //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
//                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) ||
//                                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
//                                showNearByStoriesDialog();
//                            }
//                            //permission is denied (and never ask again is  checked)
//                            //shouldShowRequestPermissionRationale will return false
//                            else {
//                                checkAndRequestPermissions();
//                            }
//                        }
//                    }
                }


            }
        }
    }


    public void showPermissionsDialog() {
        final Dialog mDialog = new Dialog(NewFeedNewsActivity.this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_dialog);

        Button btnCancel = (Button) mDialog.findViewById(R.id.btnCancel);
        Button btnOk = (Button) mDialog.findViewById(R.id.btnOk);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, 100);
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (data != null) {
                if (checkAndRequestPermissions()) {
                    showNearByStoriesDialog();
                }
            }
        }
    }
}

