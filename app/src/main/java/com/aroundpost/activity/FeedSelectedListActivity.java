package com.aroundpost.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.aroundpost.AppPreference;
import com.aroundpost.R;
import com.aroundpost.adapter.FeedSelectedChannelListAdapter;
import com.aroundpost.adapter.FeedSelectedListAdapter;
import com.aroundpost.response.ChannelData;
import com.aroundpost.response.LocationData;
import com.aroundpost.utils.BasicUtils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

/**
 * Created by Manish on 3/24/2017.
 */

public class FeedSelectedListActivity extends BaseActivity {
    private ListView mListView, mChannelListView;
    private FeedSelectedListAdapter mFeedSelectedListAdapter;
    private FeedSelectedChannelListAdapter mFeedSelectedChannelListAdapter;
    private ArrayList<LocationData> mLocationDataArrayList = new ArrayList<>();
    private ArrayList<ChannelData> mChannelDataArrayList = new ArrayList<>();
    private String locationId;
    private FloatingActionButton floatingActionButton;
    private String channelID;
    private Toolbar mToolbar;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_selected_list);

        mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Subscribed List");

        mListView = (ListView) findViewById(R.id.listview);
        mChannelListView = (ListView) findViewById(R.id.channellistview);
        firebaseAnalytics();
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingbutton);

        mFeedSelectedListAdapter = new FeedSelectedListAdapter(this, R.layout.spinner_layout);
        mListView.setAdapter(mFeedSelectedListAdapter);

        mFeedSelectedChannelListAdapter = new FeedSelectedChannelListAdapter(this, R.layout.spinner_layout);
        mChannelListView.setAdapter(mFeedSelectedChannelListAdapter);

        mLocationDataArrayList.addAll(AppPreference.getAppPreference(this).getLocationData().getData());
        mChannelDataArrayList.addAll(AppPreference.getAppPreference(this).getChannelData().getData());

        locationId = AppPreference.getAppPreference(this).getLocationNewsPreference().trim();
        Log.d("locationId bere ", locationId);
        boolean isHimachalThere = false;
        if(locationId.contains(",")){
            String[] splitIds = locationId.split(",");
            for(int i = 0; i < splitIds.length; i++){
                for (LocationData locationData : mLocationDataArrayList) {
                    Log.d("Id ", locationData.getId() + " - " + locationData.getLocation_name() + " - " + locationData.getParent_id());
                    if(locationData.getId().equals(splitIds[i])){
                        mFeedSelectedListAdapter.add(locationData);
                        mFeedSelectedListAdapter.notifyDataSetChanged();
                    }
                }
            }

        }else{
            for (LocationData locationData : mLocationDataArrayList) {
                if(locationData.getId().equals(locationId)){
                    mFeedSelectedListAdapter.add(locationData);
                    mFeedSelectedListAdapter.notifyDataSetChanged();
                }
            }
        }

        channelID = AppPreference.getAppPreference(this).getChannelNewsPreference().trim();
        Log.d("channel bere ", channelID);
        if(channelID.contains(",")){
            String[] splitIds = channelID.split(",");
            for(int i = 0; i < splitIds.length; i++){
                for (ChannelData channelData : mChannelDataArrayList) {
                    if(channelData.getId().equals(splitIds[i])){
                        mFeedSelectedChannelListAdapter.add(channelData);
                        mFeedSelectedChannelListAdapter.notifyDataSetChanged();
                    }
                }
            }
        }else{
            for (ChannelData channelData : mChannelDataArrayList) {
                if(channelData.getId().equals(channelID)){
                    mFeedSelectedChannelListAdapter.add(channelData);
                    mFeedSelectedChannelListAdapter.notifyDataSetChanged();
                }
            }
        }

        if(mFeedSelectedChannelListAdapter.getCount() > 0){
            findViewById(R.id.view).setVisibility(View.VISIBLE);
        }

        BasicUtils.getBasicUtilInstance().setListViewHeightBasedOnChildren(mListView);
        BasicUtils.getBasicUtilInstance().setListViewHeightBasedOnChildren(mChannelListView);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LocationData locationData = (LocationData) parent.getItemAtPosition(position);
                String locationId = locationData.getId();
                AppPreference.getAppPreference(FeedSelectedListActivity.this).setPreferedCategory("L"+locationId);
                Intent intent = new Intent(FeedSelectedListActivity.this, FeedNewsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mChannelListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChannelData locationData = (ChannelData) parent.getItemAtPosition(position);
                String locationId = locationData.getId();
                AppPreference.getAppPreference(FeedSelectedListActivity.this).setPreferedCategory("C"+locationId);
                Intent intent = new Intent(FeedSelectedListActivity.this, FeedNewsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FeedSelectedListActivity.this, FeedChannelLocationSelectActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return true;
    }

    /*todo firebase analytics*/
    public void firebaseAnalytics() {
        if (!FirebaseApp.getApps(this).isEmpty()) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(5000);
        mFirebaseAnalytics.setSessionTimeoutDuration(1000000);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, FeedSelectedListActivity.class.getName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, FeedSelectedListActivity.class.getName());
        mFirebaseAnalytics.logEvent("FeedSelectedListActivity", bundle);
    }
}
