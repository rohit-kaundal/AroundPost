package com.aroundpost.utils;

/**
 * Created by Manish on 9/22/2016.
 */
public class Constants {
    public static final int SPLASH_INTERVAL = 2000;
    public static final String TUTORIAL_SCREEN_POSITION = "pagePosition";
    public static final int TUTORIAL_PAGES_SIZE = 3;
    public static final int ACTIVITY_FACEBOOK_ACCOUNT_KIT = 1;
    //public static final String BASE_URL = " http://35.156.174.103:3001";
    public static final String BASE_URL = " http://aroundpost.com";
    public static final String GET_STORY= "http://aroundpost.com/api/Stories?story_id=";
    public static int nextPermissionsRequestCode = 4000;


    public static final String CHANNELS_UPDATES = "http://aroundpost.com/api/Channels";//"channels": "7,8"
    public static final String LOCATION_UPDATES = "http://aroundpost.com/api/Locations";//"locations": "7,8"
}
