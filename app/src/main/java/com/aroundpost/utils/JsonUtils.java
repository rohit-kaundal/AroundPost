package com.aroundpost.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Manish on 12/12/2016.
 */

public class JsonUtils {
    public static String toJson(Object object) {

        Gson gson = new GsonBuilder().create();
        return gson.toJson(object);
    }

    public static <T> T fromJson(String jsonString, Class<T> classType) {

        Gson gson = new GsonBuilder().create();
        return gson.fromJson(jsonString, classType);
    }
}
