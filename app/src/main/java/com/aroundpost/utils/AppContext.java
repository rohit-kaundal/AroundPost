package com.aroundpost.utils;

import android.content.Context;

/**
 * Created by Manish on 12/25/2016.
 */

public class AppContext {
    private Context appContext;

    private AppContext() {
    }

    public Context init(Context context) {
        if (appContext == null) {
            appContext = context;
        }
        return appContext;
    }

    private Context getContext() {
        return appContext;
    }

    public static Context getAroundPostContext() {
        return getInstance().getContext();
    }

    private static AppContext instance;

    public static AppContext getInstance() {
        return instance == null ?
                (instance = new AppContext()) :
                instance;
    }
}
