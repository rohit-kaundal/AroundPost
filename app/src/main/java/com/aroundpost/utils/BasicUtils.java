package com.aroundpost.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.aroundpost.AppPreference;
import com.aroundpost.R;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;
import static com.aroundpost.R.id.view;

/**
 * Created by Manish on 11/16/2016.
 */

public class BasicUtils {

    private static BasicUtils basicUtils;
    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sssZ");

    private BasicUtils(){

    }

    public static SharedPreferences getSharedPreferences(Context _context){
        return _context.getSharedPreferences(AppPreference.AROUND_POST_PREFERENCE,Context.MODE_PRIVATE);
    }


    public static BasicUtils getBasicUtilInstance(){
        if(basicUtils == null){
            basicUtils = new BasicUtils();
        }
        return basicUtils;
    }

    public boolean checkInternetOn(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()){
            return true;
        }
        return false;
    }

    public void slideUp(View view, Context context){
        view.setAnimation(AnimationUtils.loadAnimation(context, R.anim.slid_down));
    }

    public void SlideDown(View view,Context context){
        view.startAnimation(AnimationUtils.loadAnimation(context,
                R.anim.slid_up));
    }

    public Typeface setAdobeDevangari(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Adobe_Devanagari.ttf");
        return typeface;
    }

    public Typeface setAvirLTStdBook(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/AvenirLTStd-Book.otf");
        return typeface;
    }

    public Typeface setAvirLTStdLignt(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/AvenirLTStd-Light.otf");
        return typeface;
    }

    public Typeface setAvirLTStdRoman(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/AvenirLTStd-Roman.otf");
        return typeface;
    }

    public String getDeviceID(Context context){
        String deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceID;
    }

    private Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent();
        int width = (int) (paint.measureText(text) + 0.0f);
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    public void setListViewHeightBasedOnChildren(ExpandableListView listView) {
        ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        Log.d("setListViewHeightBaen", "");
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View listItem = listAdapter.getGroupView(i, true, null, listView);
            Log.d("Lisy Item == ", listItem + " " + desiredWidth);
            listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            listItem.measure(desiredWidth, 120);
            totalHeight += listItem.getMeasuredHeight();
            if (listView.isGroupExpanded(i)) {
                int childrenCount = listAdapter.getChildrenCount(i);
                for (int j = 0; j < childrenCount; j++) {
                    View childItem = listAdapter.getChildView(i, j, false, null, listView);
                    childItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += childItem.getMeasuredHeight();
                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        Log.d("params.height ", params.height + "");
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static String timeCalculation(String timeDate){
        try {
            Date date = format.parse(timeDate);
            Date nowDate = new Date();

            long difference = nowDate.getTime() - date.getTime();
            long days = TimeUnit.MILLISECONDS.toDays(difference);
            if(days > 0){
                if(days == 1){
                    return days + " day ago";
                }else{
                    return days + " days ago";
                }
            }else{
                long minute = TimeUnit.MILLISECONDS.toMinutes(difference);
                if(minute > 60){
                    return TimeUnit.MILLISECONDS.toHours(difference)+" hours ago";
                }else{
                    return minute + " minutes ago";
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
