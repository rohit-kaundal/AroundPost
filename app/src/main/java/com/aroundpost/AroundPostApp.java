package com.aroundpost;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;

import com.aroundpost.interfaces.AroundPostInterface;
import com.aroundpost.interfaces.SignalNotificationOpenHandler;
import com.aroundpost.interfaces.SignalNotificationReceiver;
import com.aroundpost.services.RetrofitInstance;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalDbHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import retrofit2.Retrofit;

/**
 * Created by Manish on 9/22/2016.
 */
public class AroundPostApp extends MultiDexApplication {
    private static AroundPostApp _instance;
    private static Context mContext;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
        @Override
    public void onCreate() {
        super.onCreate();
//            getKeyHashes();
            mContext = getApplicationContext();
            //initializing one signal
            OneSignal.startInit(this).setNotificationOpenedHandler(new SignalNotificationOpenHandler()).setNotificationReceivedHandler(new SignalNotificationReceiver()).init();
            //getting one signal player id
            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                @Override
                public void idsAvailable(String userId, String registrationId) {
                    AppPreference.getAppPreference(mContext).setOneSignalID(userId);
                    Log.e("Player id ++++",""+userId);
                }
            });

    }



    public static AroundPostInterface getAroundPostInterface(Context context) {
        Retrofit retrofit = RetrofitInstance.getRetrofitInstance().retrofitObject(context);
        AroundPostInterface aroundPostInterface = retrofit.create(AroundPostInterface.class);
        return aroundPostInterface;
    }

    public static AroundPostApp getInstance() {
        return _instance;
    }
    public static Context getContext() {
        return mContext;
    }


}
