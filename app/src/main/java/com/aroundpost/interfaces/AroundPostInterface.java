package com.aroundpost.interfaces;

import com.aroundpost.request.ReportUserRequest;
import com.aroundpost.request.SaveStoryRequest;
import com.aroundpost.request.SignUpRequest;
import com.aroundpost.response.ChannelResponse;
import com.aroundpost.response.FeedNewsResponse;
import com.aroundpost.response.FeedSearchModelResponse;
import com.aroundpost.response.GenericResponse;
import com.aroundpost.response.LocationResponse;
import com.aroundpost.response.SignUpResponse;
import com.aroundpost.response.UpdateUserInterestResponse;
import com.aroundpost.response.UserInterestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Manish on 12/2/2016.
 */

public interface AroundPostInterface {
    @GET("/RetrofitExample/books.json")
    Call<Object> getMockApi();

    @POST("/api/Users")
    Call<SignUpResponse> getSignUpApi(@Body SignUpRequest signUpRequest);

    @GET("/api/user/interest")
    Call<UserInterestResponse> getUserInterest();

    @FormUrlEncoded
    @PUT("/api/user/updateInterest")
    Call<UpdateUserInterestResponse> updateUserInterest(@Field("cityIds") String cityIds, @Field("categoryIds") String categoryIds);

    @GET("/api/Stories")
    Call<FeedNewsResponse> getFeedNewsStories();

    @GET("/api/Stories")//page=1&location=nearby&lat=31.1048&long=77.1734&radius=300
    Call<FeedNewsResponse> getFeedNearByNewsStories(@Query("page") String page, @Query("location") String location, @Query("lat") String lat, @Query("long") String lng, @Query("radius") int radius);

    @GET("/api/Stories")
    Call<FeedNewsResponse> getFeedNewsStories(@Query("page") String page, @Query("location") String location);

    @GET("/api/Stories")
    Call<FeedNewsResponse> getFeedChannelStories();

    @GET("/api/Stories")
    Call<FeedNewsResponse> getFeedChannelStories(@Query("page") String page, @Query("Channels") String channels);

    @POST("/api/Report")
    Call<GenericResponse> reportUserStory(@Body ReportUserRequest reportUserRequest);

    @GET("/api/Locations")
    Call<LocationResponse> getUserLocation();

    @GET("/api/Channels")
    Call<ChannelResponse> getNewsChannel();

    @GET("/api/Stories/search")
    Call<FeedNewsResponse> getNewsSearch(@Query("keywords") String keywords);

    @GET("/api/Stories/myStories")
    Call<FeedNewsResponse> getMyStories();

    @GET("/api/Stories")
    Call<FeedNewsResponse> getFeedUnSeenNewsStories(@Query("page") String page, @Query("location") String location, @Query("lat") String lat, @Query("long") String lng, @Query("radius") int radius, @Query("type") String type);

    @POST("/api/Stories/save")
    Call<GenericResponse> getSavedStories(@Body SaveStoryRequest saveStoryRequest);

    @POST("/api/Stories/seen")
    Call<GenericResponse> setUserStorySeen(@Body SaveStoryRequest saveStoryRequest);
}