package com.aroundpost.interfaces;

import com.aroundpost.response.FeedNewsModel;
import com.aroundpost.response.SearchResultNewsModel;

import java.util.List;

/**
 * Created by Manish on 9/24/2016.
 */
public interface OnNewsFeedClickInterface {
    void onNewsFeedItemClick(String sourceUrl,String imageUrl, List<SearchResultNewsModel> feedNewsModelList, SearchResultNewsModel feedNewsModel, int position);
    void onNewsFeedItemLongClick(SearchResultNewsModel feedNewsModel, int position);
}
