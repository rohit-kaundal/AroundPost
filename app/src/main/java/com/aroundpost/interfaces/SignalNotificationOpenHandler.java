package com.aroundpost.interfaces;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.aroundpost.AppPreference;
import com.aroundpost.AroundPostApp;
import com.aroundpost.activity.OpenNotificationStatusBar;
import com.aroundpost.activity.SplashActivity;
import com.aroundpost.response.SignUpResponse;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by patas tech on 4/13/2017.
 */

public class SignalNotificationOpenHandler implements OneSignal.NotificationOpenedHandler {
    Context context;


    String authToken = "";
    String activityToBeOpened = "";

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {

        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;
        try {
            SignUpResponse signUpDetails = AppPreference.getAppPreference(AroundPostApp.getContext()).getSignUpDetails();
            if (signUpDetails != null) {
                authToken = signUpDetails.getData().getApi_key();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //While sending a Push notification from OneSignal dashboard
        // you can send an addtional data named "activityToBeOpened" and retrieve the value of it and do necessary operation
        //If key is "activityToBeOpened" and value is "AnotherActivity", then when a user clicks
        //on the notification, AnotherActivity will be opened.
        //Else, if we have not set any additional data splash activity  is opened.
        if (data != null) {
            activityToBeOpened = data.optString("story_id", null);
            if (activityToBeOpened != null && authToken != null) {
                Log.i("OneSignalExample", "customkey set with value: " + activityToBeOpened);
                Intent intent = new Intent(AroundPostApp.getContext(), OpenNotificationStatusBar.class);
                intent.putExtra("story_id", activityToBeOpened);
                intent.putExtra("auth_token", authToken);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                AroundPostApp.getContext().startActivity(intent);
            } else {
                Intent i = new Intent(AroundPostApp.getContext(), SplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                AroundPostApp.getContext().startActivity(i);
            }
        } else {
            Intent i = new Intent(AroundPostApp.getContext(), SplashActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            AroundPostApp.getContext().startActivity(i);
        }
        //If we send notification with action buttons we need to specidy the button id's and retrieve it to
        //do the necessary operation.
        if (actionType == OSNotificationAction.ActionType.ActionTaken) {
            Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);
            if (result.action.actionID.equals("ActionOne")) {
                Toast.makeText(AroundPostApp.getContext(), "ActionOne Button was pressed", Toast.LENGTH_LONG).show();
            } else if (result.action.actionID.equals("ActionTwo")) {
                Toast.makeText(AroundPostApp.getContext(), "ActionTwo Button was pressed", Toast.LENGTH_LONG).show();
            }
        }
    }
}
