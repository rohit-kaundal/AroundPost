package com.aroundpost.interfaces;

import android.widget.ExpandableListView;

import java.util.HashMap;

/**
 * Created by Manish on 1/5/2017.
 */

public interface OpenGroupInterface {
    void openInterfaceListener(int position, ExpandableListView expandableListView, HashMap<String, String> listHashMap, int type, boolean status);
}
