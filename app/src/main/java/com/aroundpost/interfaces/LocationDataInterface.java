package com.aroundpost.interfaces;

import com.aroundpost.response.LocationData;

/**
 * Created by Manish on 3/24/2017.
 */

public interface LocationDataInterface {
    public void locationDataListener(LocationData locationData);
}
