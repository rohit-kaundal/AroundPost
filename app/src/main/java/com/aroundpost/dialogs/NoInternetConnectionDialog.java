package com.aroundpost.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.aroundpost.R;
import com.aroundpost.interfaces.NoInternetInterface;

/**
 * Created by Manish on 3/26/2017.
 */

public class NoInternetConnectionDialog extends Dialog{
    private NoInternetInterface noInternetInterface;

    public NoInternetConnectionDialog(@NonNull Context context, NoInternetInterface noInternetInterface) {
        super(context);
        this.noInternetInterface = noInternetInterface;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);
        Button button = (Button) findViewById(R.id.btn_internet);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noInternetInterface.noInternetConnectionListner();
            }
        });
    }
}
