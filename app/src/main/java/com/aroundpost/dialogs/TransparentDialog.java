package com.aroundpost.dialogs;

import android.app.Dialog;
import android.content.Context;

import com.aroundpost.R;

/**
 * Created by Manish on 12/7/2016.
 */

public class TransparentDialog extends Dialog {
    public TransparentDialog(Context context) {
        super(context);
        initView();
    }

    public TransparentDialog(Context context, int themeResId) {
        super(context, themeResId);
        initView();
    }

    protected TransparentDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        initView();
    }

    public void initView() {
        setContentView(R.layout.dialog_transparent);
    }
}
