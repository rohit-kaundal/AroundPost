package com.aroundpost.net;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Window;


import com.aroundpost.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 * Created by dhiman on 11/27/2016.
 */

public class GetApisResponses extends AsyncTask<JSONObject,Void,String> {
    public static final String TAG = "GetApisResponses";
    /*Define Activity*/
    Activity activity;
    /*Response Interface*/
    IHttpResponseListener iHttpResponseListener ;
    /*Method : POST / GET*/
    String Method = "";
    /*API Url*/
    String Url = "";
    /*Exception Interface*/
    IHttpExceptionListener iHttpExceptionListener;
    /*Progress Dialog*/
    Dialog progressDialog;
    /*String Getting Response*/
    String Data = "";
    /*String Token*/
    String Token="";
    /*Constructor For Class*/
    public GetApisResponses(Activity activity, IHttpResponseListener iHttpResponseListener, String method, String url, IHttpExceptionListener iHttpExceptionListener,String Token) {
        this.activity = activity;
        this.iHttpResponseListener = iHttpResponseListener;
        Method = method;
        Url = url;
        this.iHttpExceptionListener = iHttpExceptionListener;
        this.Token = Token;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        progressDialog = new Dialog(activity);
//        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        progressDialog.setContentView(R.layout.dialog_transparent);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
    }

    @Override
    protected String doInBackground(JSONObject... jsonObjects) {
        if (Method.equals("POST")){
            try {
                /*DefaultHttpClient*/
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                /*HttpPost*/
                HttpPost httpPost = new HttpPost(Url);
                /*SetHeader*/
                httpPost.setHeader("Content-type", "application/json");
                /*Set Key*/
                httpPost.setHeader("X-API-KEY",Token);
                /*StringEntity*/
                StringEntity se = new StringEntity(jsonObjects[0].toString());
                httpPost.setEntity(se);
                /* HttpResponse : Here we'll receive the response*/
                HttpResponse httpResponse = defaultHttpClient.execute(httpPost);
                /*Response Code*/
                int code = httpResponse.getStatusLine().getStatusCode();
                Log.e(TAG, "API CODE : " + code);
                HttpEntity entity = httpResponse.getEntity();
                Data = EntityUtils.toString(entity);
                Log.e(TAG, "API DATA : " + Data);
            } catch (Exception e) {
                e.printStackTrace();
                if (e.getClass().getName().equals("java.net.UnknownHostException")) {
                    iHttpExceptionListener.handleException("The Internet connection appears to be offline.");
                } else if (e.getClass().getName().equals("java.net.SocketTimeoutException")) {
                    iHttpExceptionListener.handleException("The request timed out.");
                } else if(e.getClass().getName().equals("android.os.DeadObjectException")) {
                    iHttpExceptionListener.handleException("The request is no longer.");
                }else{
                    iHttpExceptionListener.handleException("Server not found.");
                }
            }
        }
        if (Method.equals("GET")){
            try {
                /*DefaultHttpClient*/
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                /*HttpGet*/
                HttpGet httpGet = new HttpGet(Url);
                /*SetHeader*/
                httpGet.setHeader("Content-type", "application/json");
                /*Set Key*/
                httpGet.setHeader("X-API-KEY", Token);
                Log.e(TAG, "TOKEN APIIIII ++++ : " + Token);
//                /*StringEntity*/
//                StringEntity se = new StringEntity(jsonObjects[0].toString());
//                httpGet.setEntity(se);
                /* HttpResponse : Here we'll receive the response*/
                HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
                /*Response Code*/
                int code = httpResponse.getStatusLine().getStatusCode();
                Log.e(TAG, "API CODE : " + code);
                HttpEntity entity = httpResponse.getEntity();
                Data = EntityUtils.toString(entity);
                Log.e(TAG, "API DATA : " + Data);
            } catch (Exception e) {
                e.printStackTrace();
                if (e.getClass().getName().equals("java.net.UnknownHostException")) {
                    iHttpExceptionListener.handleException("The Internet connection appears to be offline.");
                } else if (e.getClass().getName().equals("java.net.SocketTimeoutException")) {
                    iHttpExceptionListener.handleException("The request timed out.");
                } else {
                    iHttpExceptionListener.handleException("Server not found.");
                }
            }
        }
        return Data;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
//        progressDialog.dismiss();
        try {
            if (iHttpResponseListener != null && response != null) {
                iHttpResponseListener.handleResponse(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
