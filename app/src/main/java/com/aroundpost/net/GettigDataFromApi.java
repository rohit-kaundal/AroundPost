package com.aroundpost.net;


import android.content.Context;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class GettigDataFromApi extends Thread {

    ResponseForJsonService responseForJsonService;
    ExceptionForJsonService exceptionForJsonService;
    Context context;
    JSONObject jsonObject;
    String methodType;
    String url;
    String data;

    public GettigDataFromApi(Context context, String url, ResponseForJsonService responseForJsonService, ExceptionForJsonService exceptionForJsonService) {
        this.context = context;
        this.responseForJsonService = responseForJsonService;
        this.exceptionForJsonService = exceptionForJsonService;
        this.url = url;
    }

    @Override
    public void run() {

        try {

            HttpURLConnection urlConnection = null;
            // create connection
            URL urlToRequest = new URL(url);
            urlConnection = (HttpURLConnection)
                    urlToRequest.openConnection();

            // handle issues
            int statusCode = urlConnection.getResponseCode();

            InputStream in = new BufferedInputStream(
                    urlConnection.getInputStream());
            responseForJsonService.GetResponse(getResponseText(in));


        } catch (Exception e) {
            e.printStackTrace();
            if (exceptionForJsonService != null) {
                exceptionForJsonService.GetException("Error While Running Service");
            }
        }

    }

    private static String getResponseText(InputStream inStream) {
        // very nice trick from
        // http://weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html
        return new Scanner(inStream).useDelimiter("\\A").next();
    }

}
