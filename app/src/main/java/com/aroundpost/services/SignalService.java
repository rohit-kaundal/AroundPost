package com.aroundpost.services;

import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.aroundpost.AppPreference;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationReceivedResult;


public class SignalService extends NotificationExtenderService {
    long pattern[] = {0, 100, 200, 300, 400};
    SharedPreferences preferences;

    @Override
    public boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        NotificationExtenderService.OverrideSettings overrideSettings = new NotificationExtenderService.OverrideSettings();
        overrideSettings.extender = new NotificationCompat.Extender() {
            @Override
            public NotificationCompat.Builder extend(NotificationCompat.Builder builder) {
                Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                preferences = getSharedPreferences(AppPreference.AROUND_POST_PREFERENCE, Context.MODE_PRIVATE);
                // Sets the background notification color to Green on Android 5.0+ devices.
//                return builder.setColor(new BigInteger("FF00FF00", 16).intValue());
//                return builder.setSound(sound);
                // return builder.setVibrate(pattern);
                if (!preferences.getString(AppPreference.SOUND_ON_OFF, "").equals("")) {
                    String strSound = preferences.getString(AppPreference.SOUND_ON_OFF, "");
                    if (strSound.equals("1")) {
//                        return builder.setDefaults(Notification.DEFAULT_SOUND);
                        return builder.setSound(sound);
                    } else if (strSound.equals("0")) {
                        builder.setPriority(Notification.PRIORITY_MAX);
                        return builder.setDefaults(Notification.DEFAULT_LIGHTS);
                    }

                }
                else
                {
                    return builder.setSound(sound);
                }
                return builder;
                // if (Build.VERSION.SDK_INT >= 21)


                // return   builder.setDefaults(Notification.DEFAULT_LIGHTS);
            }
        };
        OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);
        Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);

        return true;
    }





}