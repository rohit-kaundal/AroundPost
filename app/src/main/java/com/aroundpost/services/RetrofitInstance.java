package com.aroundpost.services;

import android.content.Context;
import android.util.Log;

import com.aroundpost.AppPreference;
import com.aroundpost.response.SignUpResponse;
import com.aroundpost.utils.Constants;

import java.io.IOException;
import java.sql.Time;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Manish on 12/2/2016.
 */

public class RetrofitInstance {
    private static RetrofitInstance mRetrofitInstance;
    private Retrofit mRetrofit;

    private RetrofitInstance() {

    }

    public static RetrofitInstance getRetrofitInstance() {
        if (mRetrofitInstance == null) {
            mRetrofitInstance = new RetrofitInstance();
        }
        return mRetrofitInstance;
    }

    public Retrofit retrofitObject(Context context) {
        SignUpResponse signUpDetails = AppPreference.getAppPreference(context).getSignUpDetails();
        String authToken = "";
        if(signUpDetails != null){
            authToken = signUpDetails.getData().getApi_key();
        }

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(httpLoggingInterceptor);
        final String finalAuthToken = authToken;
        Log.d("finalAuthToken ", finalAuthToken);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                Request request;
                if(finalAuthToken != null && finalAuthToken.length()>0){
                    request  = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .header("X-API-KEY", finalAuthToken)

                            .build();
                }else{
                    request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .build();
                }

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();
        mRetrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build();
        return mRetrofit;
    }
}
