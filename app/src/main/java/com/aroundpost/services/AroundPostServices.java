package com.aroundpost.services;

import android.content.Context;
import android.util.Log;

import com.aroundpost.AppPreference;
import com.aroundpost.AroundPostApp;
import com.aroundpost.interfaces.LocationUserInterface;
import com.aroundpost.response.ChannelResponse;
import com.aroundpost.response.LocationResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AroundPostServices {
    private AroundPostWrapper aroundPostWrapper;

    public void getUserLocation(final Context context, final LocationUserInterface locationUserInterface) {
        aroundPostWrapper = AroundPostWrapper.getAroundPostWrapper(context);
        Call<LocationResponse> locationResponseCall = AroundPostApp.getAroundPostInterface(context).getUserLocation();
        locationResponseCall.enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                if (response != null && response.isSuccessful()) {
                    Log.d("Location api", "");
                    AppPreference.getAppPreference(context).setLocationData(response.body());
                    if (locationUserInterface != null) {
                        locationUserInterface.userLocationSuccessListener();
                    }
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {

            }
        });
    }

    public void getUserChannel(final Context context) {
        aroundPostWrapper = AroundPostWrapper.getAroundPostWrapper(context);
        Call<ChannelResponse> channelResponseCall = AroundPostApp.getAroundPostInterface(context).getNewsChannel();
        channelResponseCall.enqueue(new Callback<ChannelResponse>() {
            @Override
            public void onResponse(Call<ChannelResponse> call, Response<ChannelResponse> response) {
                if (response != null && response.isSuccessful()) {
                    Log.d("Channel api", "");
                    AppPreference.getAppPreference(context).setChannelData(response.body());
                }
            }

            @Override
            public void onFailure(Call<ChannelResponse> call, Throwable t) {

            }
        });
    }
}
