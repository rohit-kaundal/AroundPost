package com.aroundpost.services;

import android.content.Context;

/**
 * Created by Manish on 3/24/2017.
 */

public class AroundPostWrapper {
    public static AroundPostWrapper aroundPostWrapper;
    private AroundPostServices aroundPostServices;
    private Context context;

    private AroundPostWrapper(Context context){
        this.context = context;
    }

    public static AroundPostWrapper getAroundPostWrapper(Context context){
        if(aroundPostWrapper == null){
            aroundPostWrapper = new AroundPostWrapper(context);
        }
        return aroundPostWrapper;
    }

    public AroundPostServices getAroundPostServices(){
        if(aroundPostServices == null){
            aroundPostServices = new AroundPostServices();
        }
        return aroundPostServices;
    }
}
