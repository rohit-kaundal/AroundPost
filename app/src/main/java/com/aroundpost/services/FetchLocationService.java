package com.aroundpost.services;

import android.Manifest;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.aroundpost.AppPreference;
import com.aroundpost.utils.JsonUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by Manish on 3/25/2017.
 */

public class FetchLocationService extends IntentService implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationRequest mLocationRequest;
    private boolean doBroadcast;

    public FetchLocationService() {
        super("FetchLocationService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        mGoogleApiClient = new GoogleApiClient.Builder(FetchLocationService.this).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();
        if(intent.hasExtra("doBroadcast")){
            doBroadcast = intent.getBooleanExtra("doBroadcast", false);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Intent intent = new Intent("location-broadcast");
            intent.putExtra("type", "1");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        Log.d("location ", mLocation+"");
        if(mLocation!= null){
            String location = JsonUtils.toJson(mLocation);
            AppPreference.getAppPreference(FetchLocationService.this).setUserLocation(location);
            if(doBroadcast){
                Intent intent = new Intent("location-broadcast");
                intent.putExtra("type", "2");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            }
        }else{
            mLocationRequest = new LocationRequest();
            mLocationRequest.setFastestInterval(1000);
            mLocationRequest.setInterval(1000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("updated loca ", location+"");
        String userLocation = JsonUtils.toJson(location);
        Log.d("Updated ", userLocation);
        AppPreference.getAppPreference(FetchLocationService.this).setUserLocation(userLocation);
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        if(doBroadcast){
            Intent intent = new Intent("location-broadcast");
            intent.putExtra("type", "2");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }
}
